package com.grocery60.inc.grocery.ui.activities.payment;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.databinding.RecyclerPaymentShippingMethodBinding;
import com.grocery60.inc.grocery.interfaces.clickinterfaces.SetOnClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PaymentShippingMethodAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private int lastSelectedPosition = -1;
    private static final String TAG = PaymentShippingMethodAdapter.class.getSimpleName();

    private SetOnClickListener.setCardClick setRadioBtnClick;
    private JSONArray jsonArray;

    public PaymentShippingMethodAdapter(Context context) {
        this.context = context;
        jsonArray = new JSONArray();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerPaymentShippingMethodBinding shippingMethodBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_payment_shipping_method, parent, false);

        return new MyViewHolder(shippingMethodBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            if (holder instanceof MyViewHolder) {
                MyViewHolder myViewHolder = (MyViewHolder) holder;
                JSONObject jsonObject = jsonArray.getJSONObject(position);
                String shippingMthdType = jsonObject.getString("name");
                if (shippingMthdType.equalsIgnoreCase("Store Pickup")){
                    myViewHolder.shippingMethodBinding.methodRadioBtn.setText(shippingMthdType);
                }else {
                    String text = "<font color=#000000>Home Delivery</font>" +
                            "<font color=#B71C1C>(You receive free home delivery within 10 miles if your order includes at least $50)</font>";
                    myViewHolder.shippingMethodBinding.methodRadioBtn.setText(Html.fromHtml(text));
                }
                myViewHolder.shippingMethodBinding.methodRadioBtn.setChecked(lastSelectedPosition == position);
                /*myViewHolder.shippingMethodBinding.setShippingMethodSelect(() -> {
                    setRadioBtnClick.onClick(position, jsonObject);
                });*/

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public void setData(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        public RecyclerPaymentShippingMethodBinding shippingMethodBinding;

        public MyViewHolder(@NonNull RecyclerPaymentShippingMethodBinding shippingMethodBinding) {
            super(shippingMethodBinding.getRoot());
            this.shippingMethodBinding = shippingMethodBinding;
            shippingMethodBinding.methodRadioBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                    Log.e(TAG, "onClick: lastSelectedPosition: "+lastSelectedPosition );
                    try {
                        setRadioBtnClick.onClick(lastSelectedPosition, jsonArray.getJSONObject(lastSelectedPosition));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public void radioBtnClick(SetOnClickListener.setCardClick setOnClickListener) {
        this.setRadioBtnClick = setOnClickListener;
    }

}
