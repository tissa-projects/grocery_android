package com.grocery60.inc.grocery.ServiceCall;


import android.content.Context;
import android.util.Log;


import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;


public class RestAPIClient {


    public static class APICLient {

        private static final String TAG = RestAPIClient.class.getSimpleName();
        private static Context context;

        public static String getRemoteCall(RestAPIClientHelper helper, Context ctx) {
            Log.e(TAG, "getRemoteCall: helper: " + helper.toString());
            context = ctx;
            HttpURLConnection connection = null;
            byte[] postData = null;
            String url = null;
            String cookie = null;
            int postDataLength = 0, lastResponseCode = 0;
            String result = null;
            JSONObject jsonObject = new JSONObject();
            if (helper != null) {
                try {


                    //url = ctx.getResources().getString(R.string.main_url) + helper.getRequestUrl();
                    url = helper.getRequestUrl();
                    String authorizationToken = SharePreferenceUtil.getSPstringValue(ctx, SharePreferenceUtil.AUTHORIZATION_TOKEN);
                    connection = (HttpURLConnection) new URL(url).openConnection();

                    connection.setRequestMethod(helper.getMethodType());
                    connection.setRequestProperty("Content-Type", helper.getContentType());
                    connection.setRequestProperty("charset", "utf-8");
                    connection.setRequestProperty("user_id", UserSession.getUserDetails(ctx).get(UserSession.KEY_USER_ID));
                    connection.setRequestProperty("cart_id", SharePreferenceUtil.getSPstringValue(ctx, SharePreferenceUtil.CART_ID));
                    connection.setRequestProperty("action",helper.getAction());
                    connection.setRequestProperty("order_id", helper.getOrderId());
                    //  connection.setRequestProperty("CSRFToken", helper.getApiToken());
                    //  connection.setRequestProperty("Authorization", helper.getAuthorization());
                  /*  connection.setInstanceFollowRedirects(false);
                    connection.setUseCaches(true);
                    connection.setDoInput(true);*/
                    if (!helper.getRequestUrl().equals(ctx.getResources().getString(R.string.main_url) + ctx.getResources().getString(R.string.login))
                            ||!helper.getRequestUrl().equals(ctx.getResources().getString(R.string.main_url) + ctx.getResources().getString(R.string.forgot_password_api))) {
                        if (authorizationToken.length() >0) {
                            Log.e(TAG, "request_url before : " + url + " authorizationToken : " + authorizationToken);
                            connection.setRequestProperty("Authorization", "Token " + authorizationToken);
                        }else {
                            Log.e(TAG, "request_url before : " + url + " helper.getAuthorization() : " + helper.getAuthorization());
                            connection.setRequestProperty("Authorization", "Token " + helper.getAuthorization());
                        }
                    }
                    if (helper.getMethodType().equals(context.getResources().getString(R.string.POST)) || helper.getMethodType().equals(context.getResources().getString(R.string.PUT))) {
                        try {
                            Log.e(TAG, "getRemoteCall: inside" );
                            postData = helper.getUrlParameter().getBytes(StandardCharsets.UTF_8);
                            postDataLength = postData.length;
                            connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                            try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                                wr.write(postData);
                            } catch (Exception e) {
                                Log.e(TAG, " catch : DataOutputStream: " + e.getMessage());
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                    connection.setConnectTimeout(10000);
                    connection.setReadTimeout(10000);

                    lastResponseCode = connection.getResponseCode();
                    Log.e(TAG, "getRemoteCall: lastResponseCode: " + lastResponseCode);
                   /* if (lastResponseCode == 200){
                    if (helper.getRequestUrl().equals(ctx.getResources().getString(R.string.main_url)+ctx.getResources().getString(R.string.login))) {
                        cookie = connection.getHeaderField("Set-Cookie");
                        Map<String, List<String>> maps = connection.getHeaderFields();
                        List<String> coolist = maps.get("Set-Cookie");
                        SharePreferenceUtil.setSPstring(ctx, SharePreferenceUtil.COOKIES, coolist.toString());
                        SharePreferenceUtil.setSPstring(ctx, SharePreferenceUtil.CSRF_TOKEN, coolist.get(1).split(";")[0]);
                        Log.e(TAG, "getRemoteCall: getHeaderFields : " + cookie);
                    }}*/

                   result = lastResponseCode == 200 || lastResponseCode == 201? readFully(connection.getInputStream()):readFully(connection.getErrorStream());
                    Log.e(TAG, "getRemoteCall: " + result);
                    jsonObject.put("response_code", lastResponseCode);
                    if ((helper.getRequestUrl().equals(ctx.getResources().getString(R.string.main_url) + ctx.getResources().getString(R.string.get_order_api) + UserSession.getUserDetails(context).get("user_id"))
                            || helper.getRequestUrl().equals(ctx.getResources().getString(R.string.main_url) + ctx.getResources().getString(R.string.set_order_item_api)))
                            || helper.getRequestUrl().equals(ctx.getResources().getString(R.string.main_url) + ctx.getResources().getString(R.string.get_country_api))) {
                        jsonObject.put("data",new JSONArray(result));
                    }else if(lastResponseCode == 204){
                        jsonObject.put("data","");
                    }/*else if(lastResponseCode == 401){
                        throw new MyException(ctx);
                    }*/else {
                        /*if (helper.getAccept().equalsIgnoreCase("xml")){
                            jsonObject.put("data", new JSONObject(Utils.xmlToJsonConverter(result)));}else {*/
                    jsonObject.put("data", new JSONObject(result));}

                } catch (Exception ex) {
                    ex.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(ctx);
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }
            }
            return jsonObject.toString();
        }
    }


    public static String readFully(InputStream inputStream) throws IOException {

        if (inputStream == null) {
            return "";
        }

        BufferedInputStream bufferedInputStream = null;
        ByteArrayOutputStream byteArrayOutputStream = null;

        try {
            bufferedInputStream = new BufferedInputStream(inputStream);
            byteArrayOutputStream = new ByteArrayOutputStream();

            final byte[] buffer = new byte[1024];
            int available = 0;

            while ((available = bufferedInputStream.read(buffer)) >= 0) {
                byteArrayOutputStream.write(buffer, 0, available);
            }

            return byteArrayOutputStream.toString();

        } finally {
            if (bufferedInputStream != null) {
                bufferedInputStream.close();
            }
        }
    }
}
