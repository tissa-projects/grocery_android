package com.grocery60.inc.grocery.ServiceCall;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.grocery60.inc.grocery.interfaces.AsyncResponse;


public class RestAsyncTask extends AsyncTask<String, String, String> {

    private Context context;
    private RestAPIClientHelper helper;
    private String response;
    private String TAG;
    public AsyncResponse asyncResponse;

    public RestAsyncTask(Context context, RestAPIClientHelper helper, String TAG, AsyncResponse asyncResponse) {
        this.context = context;
        this.helper = helper;
        this.TAG = TAG;
        this.asyncResponse = asyncResponse;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
      /*  ((Activity) context).getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);*/
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            response = RestAPIClient.APICLient.getRemoteCall(helper, context);
        } catch (Exception e) {
            Log.e(TAG, "doInBackground: catch : " + e.getMessage());

            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        asyncResponse.processResponse(response);
    }
}