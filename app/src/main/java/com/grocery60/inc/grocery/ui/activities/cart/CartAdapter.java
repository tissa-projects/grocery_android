package com.grocery60.inc.grocery.ui.activities.cart;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.databinding.RecyclerCartActivityBinding;
import com.grocery60.inc.grocery.interfaces.clickinterfaces.SetOnClickListener;
import com.grocery60.inc.grocery.models.CartModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private static final String TAG = CartAdapter.class.getSimpleName();
    private String currencyType;
    private SetOnClickListener.setCardClick setOnClickListenerForCardView, setOnClickListenerForCart;
    private JSONArray jsonArray;


    public CartAdapter(Context context) {
        this.context = context;
        jsonArray = new JSONArray();
        currencyType = SharePreferenceUtil.getSPstringValue(context,SharePreferenceUtil.CURRENCY_TYPE);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerCartActivityBinding recyclerCatalogActivityBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_cart_activity, parent, false);

        return new MyViewHolder(recyclerCatalogActivityBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        try {
            if (holder instanceof MyViewHolder) {
                MyViewHolder myViewHolder = (MyViewHolder) holder;
                JSONObject jsonObject = jsonArray.getJSONObject(position);
                CartModel cartModel = new CartModel();
                cartModel.setProductName(jsonObject.getString("product_name"));
                cartModel.setProductPrice(currencyType+Utils.convertToUSDFormat(jsonObject.getString("line_total")));
                cartModel.setProductQty(jsonObject.getString("quantity"));
                Utils.displayImageOriginalString(context,myViewHolder.recyclerCartActivityBinding.iconDash2,jsonObject.getString("product_url"));
                myViewHolder.bind(cartModel);
                Log.e(TAG, "onBindViewHolder: " + jsonObject);
                myViewHolder.recyclerCartActivityBinding.setDeleteClick(() -> {
                    setOnClickListenerForCardView.onClick(position, jsonObject);
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        //  return 10;
        return jsonArray.length();

    }

    public void setData(JSONArray jsonArray) throws JSONException {
        this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        public RecyclerCartActivityBinding recyclerCartActivityBinding;

        public MyViewHolder(@NonNull RecyclerCartActivityBinding recyclerCartActivityBinding) {
            super(recyclerCartActivityBinding.getRoot());
            this.recyclerCartActivityBinding = recyclerCartActivityBinding;
        }

        public void bind(Object obj) {
            recyclerCartActivityBinding.setCartModel((CartModel) obj);
            recyclerCartActivityBinding.executePendingBindings();
        }
    }

    public void deleteClick(SetOnClickListener.setCardClick setOnClickListener) {
        this.setOnClickListenerForCardView = setOnClickListener;
    }

}