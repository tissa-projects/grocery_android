package com.grocery60.inc.grocery.ui.activities.Address;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.ActivityShippingAddressBinding;
import com.grocery60.inc.grocery.ui.activities.payment.INDPaymentActivity;
import com.grocery60.inc.grocery.ui.activities.payment.PaymentShippingMethodAdapter;
import com.grocery60.inc.grocery.ui.activities.payment.USPaymentActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ShippingAddressActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private ActivityShippingAddressBinding addressBinding;
    private AddressViewModel viewModel;
    private PaymentShippingMethodAdapter paymentShippingMethodAdapter;
    private Context context;
    private Dialog dialogShipping, dialogBilling, dialogCountry, dialogstate, dialogGetItem;
    private NavController navController;
    private JSONObject customerObj = null;
    private JSONObject bundle = null, jsonObjectAddress;
    private String shippingName, shippingCompanyName, shippingAddress, shippingHouseNo, shippingZip,
            shippingCity, shippingState, shippingCountry, shippingId = "0", shippingMethodName;
    private boolean isShipAddrAvail = false, isBillAddrAvail = false;
    private String strBillingAddressId = null, strShippingAddressId = null, cartDataArray = null,
            strCounntryShortCode = null, noTaxTotal = "0", subTotal = "0";
    private int lastSelectedPosition = -1;
    private ArrayList<String> countryNameList = null, stateNameList = null, counntryShortCodeList = null;
    public static final String TAG = ShippingAddressActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addressBinding = DataBindingUtil.setContentView(this, R.layout.activity_shipping_address);
        viewModel = ViewModelProviders.of(this).get(AddressViewModel.class);
        context = ShippingAddressActivity.this;
        getIntentData();
        setRecyclerForShippingMethod();
        if (VU.isConnectingToInternet(context)) {

            getShippingMethods();
        }
    }

    private void getIntentData() {
        try {
            Bundle bundleData = getIntent().getExtras();
            if (bundleData != null) {
                //  String strCustomer = bundleData.getString("customerObj");
                cartDataArray = bundleData.getString("cartDataArray");
                noTaxTotal = bundleData.getString("noTaxTotal");
                subTotal = bundleData.getString("subTotal");
              /*  customerObj = new JSONObject(strCustomer);
                Log.e(TAG, "onCreate: " + customerObj.toString());
                Log.e(TAG, "cartDataArray: " + cartDataArray.toString());*/
                Log.e(TAG, "cartDataArray: " + cartDataArray.toString());
                Log.e(TAG, "noTaxTotal: " + noTaxTotal.toString());
                Log.e(TAG, "subTotal: " + subTotal.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setRecyclerForShippingMethod() {
        paymentShippingMethodAdapter = new PaymentShippingMethodAdapter(context);
        paymentShippingMethodAdapter.radioBtnClick((position, jsonObject) -> {   // recycler click

            try {
                shippingId = jsonObject.getString("id");
                shippingMethodName = jsonObject.getString("name");
                lastSelectedPosition = position;
                if (shippingMethodName.equalsIgnoreCase("Store Pickup")) {
                    addressBinding.rlAddress.setVisibility(View.GONE);
                } else {
                    addressBinding.rlAddress.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        addressBinding.setShippingMethodAdapter(paymentShippingMethodAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
        addressBinding.shippingEdtName.setText("");
        addressBinding.shippingEdtCompanyName.setText("");
        addressBinding.shippingEdtAddressLine.setText("");
        addressBinding.shippingEdtHouseNo.setText("");
        addressBinding.shippingEdtZipCode.setText("");
        addressBinding.shippingEdtCity.setText("");
        addressBinding.shippingEdtState.setText("");
        addressBinding.shippingEdtCountry.setText("");
        getLocation();
    }

    public void getLocation() {
        try {
            String strCountry = SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.COUNTRY);
            Log.e(TAG, "gotLocation: " + strCountry);
            if (strCountry.equalsIgnoreCase("India")) {
                addressBinding.shippingEdtCountry.setText("India");
                strCountry = "IN";
            } else {
                strCountry = "US";
                addressBinding.shippingEdtCountry.setText("United States of America");
            }
            Log.e(TAG, "gotLocation: " + strCountry);
            strCounntryShortCode = strCountry;
            if (!addressBinding.shippingEdtCountry.getText().toString().equalsIgnoreCase("")) {
                getStateList();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Shipping Address");
        findViewById(R.id.title_bar_back_arrow).setOnClickListener(v -> onBackPressed());
        if (VU.isConnectingToInternet(context)) {
            countryNameList = new ArrayList<>();
            counntryShortCodeList = new ArrayList<>();
            getShippingAddress();
            getBillingAddress();
            //   getCountryList();
            getCustomer();
        }
        //   addressBinding.billingEdtCountry.setOnClickListener(this::onClick);
        addressBinding.shippingEdtCountry.setOnClickListener(this::onClick);
        addressBinding.shippingEdtState.setOnClickListener(this::onClick);
        addressBinding.checkboxCurrentAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e(TAG, "onClick: checkbox" + isChecked);
                if (!isChecked)
                    setEnableField(true);
                else setEnableField(false);
            }
        });
        // addressBinding.billingEdtState.setOnClickListener(this::onClick);
        addressBinding.refresh.setOnRefreshListener(this);

        addressBinding.btnNext.setOnClickListener(v -> {    // ===================next btn click============================
            bundle = new JSONObject();
            if (viewModel.validateShipping(context, lastSelectedPosition)) {
                try {
                    setAddressData();
                    bundle.put("customerObj", customerObj.toString());
                    JSONObject shippingAddressObj = new JSONObject();
                    shippingAddressObj.put("name", shippingName);
                    shippingAddressObj.put("company_name", shippingCompanyName);
                    shippingAddressObj.put("address", shippingAddress);
                    shippingAddressObj.put("house_number", shippingHouseNo);
                    shippingAddressObj.put("zip", shippingZip);
                    shippingAddressObj.put("city", shippingCity);
                    shippingAddressObj.put("state", shippingState);
                    shippingAddressObj.put("country", shippingCountry);

                    if (addressBinding.checkbox.isChecked()) {
                        bundle.put("addressObj", shippingAddressObj.toString());
                    } /*else {
                    bundle.put("addressObj", billingAddressObj.toString());
                }*/

                    //   bundle.put("shippingAdddressObj", shippingAddressObj.toString());
                    bundle.put("counntryShortCode", strCounntryShortCode);
                    if (VU.isConnectingToInternet(context)) {
                        Log.e(TAG, "init: " + shippingId + " " + shippingMethodName);
                        if (shippingMethodName.equalsIgnoreCase("Store Pickup")) {
                            startActivity(new Intent(context, BillingAddressActivity.class)
                                    .putExtra("bundle", bundle.toString())
                                    .putExtra("cartDataArray", cartDataArray)
                                    .putExtra("subTotal", subTotal)
                                    .putExtra("noTaxTotal", noTaxTotal)
                                    .putExtra("shippingId", shippingId)
                                    .putExtra("shippingMethdName", shippingMethodName));
                        } else if (viewModel.shippingValidate(context, addressBinding)) {
                            if (addressBinding.checkbox.isChecked()) {
                                if (isBillAddrAvail) {
                                    updateBillingAddress();
                                } else {
                                    setBillingAddress();
                                }
                            }

                            if (isShipAddrAvail) {
                                updateShippingAddress();
                            } else {
                                setShippingAddress();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


    }

    private void setEnableField(boolean isenable) {
        addressBinding.shippingEdtName.setEnabled(isenable);
        addressBinding.shippingEdtCompanyName.setEnabled(isenable);
        addressBinding.shippingEdtAddressLine.setEnabled(isenable);
        addressBinding.shippingEdtHouseNo.setEnabled(isenable);
        addressBinding.shippingEdtZipCode.setEnabled(isenable);
        addressBinding.shippingEdtCity.setEnabled(isenable);
        addressBinding.shippingEdtState.setEnabled(isenable);
        addressBinding.shippingEdtCountry.setEnabled(isenable);
    }

    private void getShippingMethods() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_shipping_methods) + "status=ACTIVE&store_id=" + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.STORE_ID_WRT_CART));
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_shipping_methods));
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogGetItem = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getShippingFees(helper).observe(this, (response) -> {
            try {
                dialogGetItem.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "getShippingMethods: " + responseObj);
                    if (responseObj.getInt("response_code") == 200) {
                        JSONArray jsonArray = responseObj.getJSONObject("data").getJSONArray("results");
                        if (jsonArray.length() == 0) {
                            Utils.getToast(context, "No shipping method available");
                        }
                        paymentShippingMethodAdapter.setData(jsonArray);
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }


    private void verifyAddress() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
       /* String address1 =addressBinding.shippingEdtHouseNo.getText().toString() + addressBinding.shippingEdtAddressLine.getText().toString();
        String city =addressBinding.shippingEdtCity.getText().toString();
        String state =addressBinding.shippingEdtState.getText().toString();
        String zip =addressBinding.shippingEdtZipCode.getText().toString() ;*/
        String address1 =shippingHouseNo+" "+shippingAddress;
        String city = shippingCity;
        String state = shippingState;
        String zip = shippingZip;
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl("https://secure.shippingapis.com/ShippingAPI.dll?API=Verify&XML=<AddressValidateRequest USERID=“605TISSA5241”>\n" +
                    "   <Revision>1</Revision>\n" +
                    "   <Address ID=“0\">\n" +
                    "      <Address1>"+address1+"</Address1>\n" +
                    "      <Address2/>\n" +
                    "      <City>"+city+"</City>\n" +
                    "      <State>"+state+"</State>\n" +
                    "      <Zip5>"+zip+"</Zip5>\n" +
                    "      <Zip4 />\n" +
                    "   </Address>\n" +
                    "</AddressValidateRequest>");
            helper.setUrlParameter("");
            helper.setAccept("xml");

        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogGetItem = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getShippingFees(helper).observe(this, (response) -> {
            try {
                dialogGetItem.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "getShippingMethods: " + responseObj);
                    if (responseObj.getInt("response_code") == 200) {
                       /* JSONArray jsonArray = responseObj.getJSONObject("data").getJSONArray("results");
                        if (jsonArray.length() == 0) {
                            Utils.getToast(context, "No shipping method available");
                        }
                        paymentShippingMethodAdapter.setData(jsonArray);*/
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

   /* //country dialog
    private void setCountry() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Country");
        // add a list
        builder.setItems(Utils.GetStringArray(countryNameList), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                // addressBinding.billingEdtCountry.setText(countryNameList.get(position));
                addressBinding.shippingEdtCountry.setText(countryNameList.get(position));
                // addressBinding.billingEdtState.setText("");
                addressBinding.shippingEdtState.setText("");
                strCounntryShortCode = counntryShortCodeList.get(position);
                dialog.dismiss();
                if (countryNameList.get(position).equalsIgnoreCase(addressBinding.shippingEdtCountry.getText().toString())) {
                    getStateList();
                }
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }*/

    //state dialog
    private void setstateDialog(String type) {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select State");
        // add a list
        builder.setItems(Utils.GetStringArray(stateNameList), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                addressBinding.shippingEdtState.setText(stateNameList.get(position));
                dialog.dismiss();
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void getShippingAddress() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_shippinp_address_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_shippinp_address_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogShipping = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getShippingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogShipping.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getShippingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");

                        if (resultArray.length() > 0) {
                            addressBinding.llCurrentAddress.setVisibility(View.VISIBLE);
                            addressBinding.txtNote.setVisibility(View.VISIBLE);
                            addressBinding.checkboxCurrentAddress.setChecked(true);
                            JSONObject resultObj = resultArray.getJSONObject(0);
                            jsonObjectAddress = resultObj;
                            shippingName = jsonObjectAddress.getString("name");
                            shippingCompanyName = jsonObjectAddress.getString("company_name");
                            shippingAddress = jsonObjectAddress.getString("address");
                            shippingHouseNo = jsonObjectAddress.getString("house_number");
                            shippingZip = jsonObjectAddress.getString("zip");
                            shippingCity = jsonObjectAddress.getString("city");
                            shippingState = jsonObjectAddress.getString("state");
                            shippingCountry = jsonObjectAddress.getString("country");
                            strCounntryShortCode = jsonObjectAddress.getString("country");
                            addressBinding.edtCurrentAddress.setText(shippingName + ",\n" + shippingAddress + ", " + shippingHouseNo + ",\n" +
                                    shippingCity + ", " + shippingState + ", " + shippingCountry + ",\n" + shippingZip);
                            /*Log.e(TAG, "getShippingAddress: resultObj: " + resultObj.toString());
                            addressBinding.shippingEdtName.setText(resultObj.getString("name"));
                            addressBinding.shippingEdtCompanyName.setText(resultObj.getString("company_name"));
                            addressBinding.shippingEdtAddressLine.setText(resultObj.getString("address"));
                            addressBinding.shippingEdtHouseNo.setText(resultObj.getString("house_number"));
                            addressBinding.shippingEdtZipCode.setText(resultObj.getString("zip"));
                            addressBinding.shippingEdtCity.setText(resultObj.getString("city"));
                            addressBinding.shippingEdtState.setText(resultObj.getString("state"));
                            addressBinding.shippingEdtCountry.setText(resultObj.getString("country"));*/
                            strShippingAddressId = resultObj.getString("id");
                            isShipAddrAvail = true;
                         //   verifyAddress();
                        } else {
                            setEnableField(true);
                            addressBinding.checkboxCurrentAddress.setChecked(false);
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void setShippingAddress() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.set_shippinp_address_api);
        try {
            reqObj.put("name", shippingName);
            reqObj.put("company_name", shippingCompanyName);
            reqObj.put("address", shippingAddress);
            reqObj.put("house_number", shippingHouseNo);
            reqObj.put("zip", shippingZip);
            reqObj.put("city", shippingCity);
            reqObj.put("country", shippingCountry);
            reqObj.put("state", shippingState);
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("priority", 1);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.set_shippinp_address_api));
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogShipping = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.setShippingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogShipping.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "setShippingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 201) {
                        JSONObject resultObj = jsonObject.getJSONObject("data");
                        Intent intent = null;
                        if (addressBinding.checkbox.isChecked()) {
                            if (SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CURRENCY_TYPE).equalsIgnoreCase("$")) {
                                intent = new Intent(context, USPaymentActivity.class);
                            } else {
                                intent = new Intent(context, INDPaymentActivity.class);
                            }
                        } else {
                            intent = new Intent(context, BillingAddressActivity.class);
                        }
                        intent.putExtra("bundle", bundle.toString())
                                .putExtra("cartDataArray", cartDataArray)
                                .putExtra("subTotal", subTotal)
                                .putExtra("noTaxTotal", noTaxTotal)
                                .putExtra("shippingId", shippingId)
                                .putExtra("shippingMethdName", shippingMethodName);
                        startActivity(intent);
                        //    navController.navigate(R.id.paymentFragment, bundle);
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void updateShippingAddress() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.update_shippinp_address_api) + strShippingAddressId + "/";
        try {
            reqObj.put("name", shippingName);
            reqObj.put("company_name", shippingCompanyName);
            reqObj.put("address", shippingAddress);
            reqObj.put("house_number", shippingHouseNo);
            reqObj.put("zip", shippingZip);
            reqObj.put("city", shippingCity);
            reqObj.put("country", shippingCountry);
            reqObj.put("state", shippingState);
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("priority", 1);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.PUT));
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.update_shippinp_address_api));
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogShipping = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.setShippingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogShipping.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "updateShippingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONObject resultObj = jsonObject.getJSONObject("data");
                        Log.e(TAG, "updateShippingAddress: " + resultObj.toString());
                        Intent intent = null;
                        if (addressBinding.checkbox.isChecked()) {
                            if (SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CURRENCY_TYPE).equalsIgnoreCase("$")) {
                                intent = new Intent(context, USPaymentActivity.class);
                            } else {
                                intent = new Intent(context, INDPaymentActivity.class);
                            }
                        } else {
                            intent = new Intent(context, BillingAddressActivity.class);
                        }
                        intent.putExtra("bundle", bundle.toString())
                                .putExtra("cartDataArray", cartDataArray)
                                .putExtra("subTotal", subTotal)
                                .putExtra("noTaxTotal", noTaxTotal)
                                .putExtra("shippingId", shippingId)
                                .putExtra("shippingMethdName", shippingMethodName);
                        startActivity(intent);
                        //     navController.navigate(R.id.paymentFragment, bundle);
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void setBillingAddress() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.set_billing_address_api);
        try {
            reqObj.put("name", shippingName);
            reqObj.put("company_name", shippingCompanyName);
            reqObj.put("address", shippingAddress);
            reqObj.put("house_number", shippingHouseNo);
            reqObj.put("zip", shippingZip);
            reqObj.put("city", shippingCity);
            reqObj.put("country", shippingCountry);
            reqObj.put("state", shippingState);
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("priority", 1);
            helper.setAction(getResources().getString(R.string.set_billing_address_api));
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogBilling = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.setBillingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogBilling.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "setBillingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 201) {
                        JSONObject resultObj = jsonObject.getJSONObject("data");
                        Log.e(TAG, "setBillingAddress: " + resultObj.toString());

                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void updateBillingAddress() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.update_billing_address_api) + strBillingAddressId + "/";
        try {
            reqObj.put("name", shippingName);
            reqObj.put("company_name", shippingCompanyName);
            reqObj.put("address", shippingAddress);
            reqObj.put("house_number", shippingHouseNo);
            reqObj.put("zip", shippingZip);
            reqObj.put("city", shippingCity);
            reqObj.put("country", shippingCountry);
            reqObj.put("state", shippingState);
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("priority", 1);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.PUT));
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.update_billing_address_api));
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogBilling = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.setBillingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogBilling.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "updateBillingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONObject resultObj = jsonObject.getJSONObject("data");
                        Log.e(TAG, "updateBillingAddress: " + resultObj.toString());

                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void getBillingAddress() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_billing_address_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_billing_address_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogBilling = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getBillingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogBilling.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getBillingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");

                        if (resultArray.length() > 0) {
                            JSONObject resultObj = resultArray.getJSONObject(0);
                          /*  Log.e(TAG, "getBillingAddress: resultObj: " + resultObj.toString());
                            addressBinding.billingEdtName.setText(resultObj.getString("name"));
                            addressBinding.billingEdtCompanyName.setText(resultObj.getString("company_name"));
                            addressBinding.billingEdtAddressLine.setText(resultObj.getString("address"));
                            addressBinding.billingEdtHouseNo.setText(resultObj.getString("house_number"));
                            addressBinding.billingEdtZipCode.setText(resultObj.getString("zip"));
                            addressBinding.billingEdtCity.setText(resultObj.getString("city"));
                            addressBinding.billingEdtState.setText(resultObj.getString("state"));
                            addressBinding.billingEdtCountry.setText(resultObj.getString("country"));*/
                            strBillingAddressId = resultObj.getString("id");
                            isBillAddrAvail = true;
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    /*private void getCountryList() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_country_api);
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogCountry = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getCountryList(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogCountry.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getCountryList : " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONArray("data");

                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                countryNameList.add(resultArray.getJSONObject(i).getString("country"));
                                counntryShortCodeList.add(resultArray.getJSONObject(i).getString("country_code"));
                            }

                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }*/

    private void getStateList() {
        stateNameList = new ArrayList<>();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_state_api) + addressBinding.shippingEdtCountry.getText().toString();
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_state_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        //   dialogstate = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getStateList(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                //    dialogstate.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getStateList : " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");

                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                stateNameList.add(resultArray.getJSONObject(i).getString("state"));
                            }

                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void getCustomer() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_customer_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_customer_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        viewModel.getCustomer(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject addressObject = new JSONObject(response);
                    Log.e(TAG, "getCustomer: " + addressObject);
                    if (addressObject.getInt("response_code") == 200) {
                        setCustomerData(addressObject);
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void setCustomerData(JSONObject jsonObject) {
        try {
            JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");
            JSONObject resultObj = resultArray.getJSONObject(0);
            //  lastAccess = resultObj.getString("last_access");
            customerObj = resultObj.getJSONObject("customer");
           /* customerBinding.edtSalutation.setText(resultObj.getString("salutation"));
            customerBinding.edtMNo.setText(resultObj.getString("phone_number"));
            customerBinding.edtFName.setText(customerObj.getString("first_name"));
            customerBinding.edtLName.setText(customerObj.getString("last_name"));
            customerBinding.edtEmail.setText(customerObj.getString("email"));*/
            customerObj.put("phone_number", resultObj.getString("phone_number"));

            Log.e(TAG, "onCreate: " + customerObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.shipping_edt_country:
                //   setCountry();
                break;

            case R.id.checkbox_current_address:
                Log.e(TAG, "onClick: checkbox" + addressBinding.checkbox.isChecked());
                if (!addressBinding.checkbox.isChecked()) {
                    Log.e(TAG, "onClick: checkbox if :" + addressBinding.checkbox.isChecked());
                    addressBinding.shippingEdtName.setEnabled(true);
                    addressBinding.shippingEdtCompanyName.setEnabled(true);
                    addressBinding.shippingEdtAddressLine.setEnabled(true);
                    addressBinding.shippingEdtHouseNo.setEnabled(true);
                    addressBinding.shippingEdtZipCode.setEnabled(true);
                    addressBinding.shippingEdtCity.setEnabled(true);
                    addressBinding.shippingEdtState.setEnabled(true);
                    addressBinding.shippingEdtCountry.setEnabled(true);
                }
                break;

            case R.id.shipping_edt_state:
                if (!addressBinding.shippingEdtCountry.getText().toString().equalsIgnoreCase("")) {
                    setstateDialog("shipping");
                } else {
                    Utils.getToast(context, "First select country");
                }
                break;
        }
    }

    private void setAddressData() {
        if (addressBinding.checkboxCurrentAddress.isChecked()) {
            try {
                shippingName = jsonObjectAddress.getString("name");
                shippingCompanyName = jsonObjectAddress.getString("company_name");
                shippingAddress = jsonObjectAddress.getString("address");
                shippingHouseNo = jsonObjectAddress.getString("house_number");
                shippingZip = jsonObjectAddress.getString("zip");
                shippingCity = jsonObjectAddress.getString("city");
                shippingState = jsonObjectAddress.getString("state");
                shippingCountry = jsonObjectAddress.getString("country");
                strCounntryShortCode = jsonObjectAddress.getString("country");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            shippingName = addressBinding.shippingEdtName.getText().toString();
            shippingCompanyName = addressBinding.shippingEdtCompanyName.getText().toString();
            shippingAddress = addressBinding.shippingEdtAddressLine.getText().toString();
            shippingHouseNo = addressBinding.shippingEdtHouseNo.getText().toString();
            shippingZip = addressBinding.shippingEdtZipCode.getText().toString();
            shippingCity = addressBinding.shippingEdtCity.getText().toString();
            shippingState = addressBinding.shippingEdtState.getText().toString();
            shippingCountry = strCounntryShortCode;
        }
    }

    @Override   // swipe refresh
    public void onRefresh() {
        if (VU.isConnectingToInternet(context)) {
            countryNameList = new ArrayList<>();
            counntryShortCodeList = new ArrayList<>();
            getShippingAddress();
            getBillingAddress();
            //   getCountryList();
            getCustomer();
        }
        addressBinding.refresh.setRefreshing(false);
    }
}
