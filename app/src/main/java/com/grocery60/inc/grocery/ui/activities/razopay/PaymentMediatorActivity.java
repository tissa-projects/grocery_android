package com.grocery60.inc.grocery.ui.activities.razopay;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.grocery60.inc.grocery.R;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import java.util.ArrayList;

public class PaymentMediatorActivity extends AppCompatActivity implements PaymentResultWithDataListener {
    public static final String TAG = PaymentMediatorActivity.class.getName();

    Context context = PaymentMediatorActivity.this;
    private Dialog dialog;
    private String strAmt, strPymentId, name, receipt, email, phnNo,strOrderId;
    ArrayList<String> subscriptionIdList = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_mediator);

    //    getIntentData();
     //   startPayment();

    }

   /* private void getIntentData() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            strAmt = bundle.getString("amt");
            strOrderId = bundle.getString("order_id");
            name = bundle.getString("name");
            email = bundle.getString("email");
            phnNo = bundle.getString("phn_no");
            receipt = bundle.getString("receipt");

        }
    }*/

   /* public void startPayment() {

        *//**
         * Instantiate Checkout
         *//*
        Checkout checkout = new Checkout();
        checkout.setKeyID("<YOUR_KEY_ID>");
        *//**
         * Set your logo here
         *//*
        checkout.setImage(R.drawable.cart_app_icon);

        *//**
         * Reference to current activity
         *//*
        final Activity activity = this;

        *//**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         *//*
        try {
            JSONObject options = new JSONObject();

            options.put("name", name);
            options.put("description", receipt);
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("order_id", strOrderId);//from response of step 3.
            options.put("theme.color", getResources().getColor(R.color.colorPrimary));
            options.put("currency", "INR");
            options.put("amount", Double.valueOf(strAmt) * 100);//pass amount in currency subunits
            options.put("prefill.email", email);
            options.put("prefill.contact", phnNo);
            checkout.open(activity, options);
        } catch (Exception e) {
            Log.e(TAG, "Error in starting Razorpay Checkout", e);
        }
    }
*/

    /*public void startPayment() {
     *//*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         *//*
        // final Activity activity = getActivity();
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("currency", "INR");
            options.put("amount", Double.valueOf(strAmt) * 100);
            options.put("name", "Grocery60");

            JSONObject preFill = new JSONObject();
            preFill.put("name", );
            preFill.put("contact", );
            preFill.put("email", );

            options.put("prefill", preFill);
            co.open(activity, options);

        } catch (Exception e) {
            Toast.makeText(activity, "Payment failed", Toast.LENGTH_SHORT)
                    .show();

            Log.e(TAG, "onPaymentFailed2:  " + e.getMessage());

            e.printStackTrace();
            finish();
        }
    }*/

/*
    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String paymentId) {

        try {
            Log.e(TAG, "onPaymentSuccess:  " + paymentId);
       *//*    String fragmentType = Utilities.getSPstringValue(context, Utilities.spPaymentFromActivity);
            Log.e(TAG, "onPaymentSuccess: " + fragmentType);
            if (fragmentType.equalsIgnoreCase("MyServiceServiceFragment")) {
                sendServiceSubscriptionData(paymentId);
            } else if (fragmentType.equalsIgnoreCase("MySpaceSpaceFragment")) {
                sendSpaceSubscriptionData(paymentId);
            } else if (fragmentType.equalsIgnoreCase("RenewActivity")) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("result", paymentId);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
*//*

        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);

        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Log.d(TAG, "onPaymentFailed:  " + response);

            Toast.makeText(context, "Payment failed", Toast.LENGTH_SHORT).show();
            finish();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }*/

    @Override
    public void onPaymentSuccess(String razorpayPaymentId, PaymentData paymentData) {
        Log.e(TAG, "onPaymentSuccess:razorpayPaymentId  "+razorpayPaymentId+"   "+paymentData.getOrderId()+" "+paymentData.toString() );
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {

        Log.e(TAG, "onPaymentError: "+i +" "+s+" "+paymentData.toString());

    }


   /* //send space subscribttion data  to get subscription_id
    public void sendSpaceSubscriptionData(String paymentId) {
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Log.e(TAG, "sendSpaceSubscriptionData: authtoken " + Utilities.getSPstringValue(context, Utilities.spAuthToken));
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().sendSpaceSubscriptionData(new SpaceSubscriptionModel(strId, selectedName,
                Utilities.getSPstringValue(context, Utilities.spUserId), strPlanId, strPlanType, strAmt, Utilities.getSPstringValue(context, Utilities.spAuthToken), paymentId));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            String strMessage = jsonObject.getString("message");
                            JSONArray strSubscriptionIds = jsonObject.getJSONArray("subscription_id");
                            if (strSubscriptionIds != null) {
                                for (int i = 0; i < strSubscriptionIds.length(); i++) {
                                    subscriptionIdList.add(strSubscriptionIds.getString(i));
                                }
                            }
                            //Utilities.setArrayList(context, Utilities.spSubscriptionId, subscriptionIdList);
                            // Utilities.setSPstring(context, Utilities.spSubscriptionId, strSubscriptionId);
                            Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            goToSpaceRegistration();
                        }
                    }

                } catch (Exception e) {
                    Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    //send service subscribttion data  to get subscription_id
    public void sendServiceSubscriptionData(String paymentId) {
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().sendServiceSubscriptionData(new ServiceSubscriptionModel(strId, selectedName,
                Utilities.getSPstringValue(context, Utilities.spUserId), strPlanId, strPlanType, strAmt, Utilities.getSPstringValue(context, Utilities.spAuthToken), paymentId));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    int statusCode = Obj.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            String strMessage = Obj.getString("message");
                            String strSubscriptionId = Obj.getString("subscription_id");
                            Utilities.setSPstring(context, Utilities.spSubscriptionId, strSubscriptionId);
                            Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            goToServiceRegistration();
                        }
                    }

                } catch (Exception e) {
                    Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    private void goToSpaceRegistration() {

        Intent i = new Intent(context, SpaceRegistrationDetailsActivity.class);
        i.putStringArrayListExtra("venueType", selectedNameList);
        i.putStringArrayListExtra("venueId", strId);
        i.putStringArrayListExtra("subscriptionId", subscriptionIdList);
        startActivity(i);
        finish();

    }

    private void goToServiceRegistration() {

        Intent i = new Intent(context, ServiceRegistrationDetailsActivity.class);
        i.putExtra("serviceType", selectedName);
        i.putExtra("serviceId", strId);
        startActivity(i);
        finish();

    }
*/
}
