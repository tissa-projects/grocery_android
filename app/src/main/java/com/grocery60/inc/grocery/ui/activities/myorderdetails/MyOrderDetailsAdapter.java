package com.grocery60.inc.grocery.ui.activities.myorderdetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.databinding.RecyclerMyOrderDetailsBinding;
import com.grocery60.inc.grocery.interfaces.clickinterfaces.SetOnClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class MyOrderDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private static final String TAG = MyOrderDetailsAdapter.class.getSimpleName();

    private SetOnClickListener.setCardClick setViewDetails;
    private JSONArray jsonArray;
    private String currencyType;


    public MyOrderDetailsAdapter(Context context) {
        this.context = context;
        jsonArray = new JSONArray();
        currencyType = SharePreferenceUtil.getSPstringValue(context,SharePreferenceUtil.CURRENCY_TYPE);
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerMyOrderDetailsBinding myOrdersDetailsBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_my_order_details, parent, false);

        return new MyOrderDetailsAdapter.MyViewHolder(myOrdersDetailsBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            if (holder instanceof MyViewHolder) {
                MyViewHolder myViewHolder = (MyViewHolder) holder;
                JSONObject jsonObject = jsonArray.getJSONObject(position);
                JSONObject productObj = jsonObject.getJSONObject("product");
                String imgUrl = productObj.getString("product_url");
                Utils.displayImageOriginalString(context,myViewHolder.myOrderDetailsBinding.productImage,imgUrl);
                myViewHolder.myOrderDetailsBinding.productName.setText(productObj.getString("product_name"));
                myViewHolder.myOrderDetailsBinding.productCategory.setText("Category: "+productObj.getString("product_category"));
                myViewHolder.myOrderDetailsBinding.productQty.setText("Qty: "+jsonObject.getString("quantity"));
                String strprice = (String.valueOf(Double.valueOf(jsonObject.getString("quantity"))*Double.valueOf(productObj.getString("price"))));
                myViewHolder.myOrderDetailsBinding.productTotal.setText("Price: "+currencyType+Utils.convertToUSDFormat(strprice));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public void setData(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        public RecyclerMyOrderDetailsBinding myOrderDetailsBinding;

        public MyViewHolder(@NonNull RecyclerMyOrderDetailsBinding myOrderDetailsBinding) {
            super(myOrderDetailsBinding.getRoot());
            this.myOrderDetailsBinding = myOrderDetailsBinding;
        }

       /* public void bind(Object obj) {
            myOrderDetailsBinding.setMyOrder((MyOrderModel) obj);
            myOrderDetailsBinding.executePendingBindings();
        }*/
    }

}
