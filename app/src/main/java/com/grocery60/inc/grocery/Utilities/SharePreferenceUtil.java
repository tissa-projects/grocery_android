package com.grocery60.inc.grocery.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePreferenceUtil {


    public static String AUTHORIZATION_TOKEN = "authorization_token";
    public static String CART_ID = "cart_id";
    public static String STORE_ID_WRT_CART = "store_id_wrt_Cart";
    public static String ZIPCODE = "zipcode";
    public static String CART_COUNT = "cart_count";
    public static String CURRENCY_TYPE = "currency_type";
    public static String COUNTRY = "country";
    /*
     public static String  COUNTRY = "country";
    public static String SUB_TOTAL = "sub_total";
    public static String NO_TAX_TOTAL = "no_tax_total";
    public static String TAX = "tax";
    public static String TIP = "tip";
    public static String SERVICE_FEE = "service_fee";
    public static String DISCOUNT = "discount";
    public static String TOTAL = "total";
     public static String  COOKIES = "cookies";
    public static String  CSRF_TOKEN = "csrf_token";*/


    public static void clear(Context context) {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();

    }

    //Store string using Shared Preference
    public static void setSPstring(Context mContext, String key, String value) {
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    //Store boolean using Shared Preference
    public static void setSPboolean(Context mContext, String key, boolean value) {
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    //Store integer using Shared Preference
    public static void setSPineger(Context mContext, String key, int value) {
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.commit();
    }


    //get string value from Shared Preference
    public static String getSPstringValue(Context mContext, String key) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        String value = sharedPreferences.getString(key, "");
        return value;
    }

    //get int value from Shared Preference
    public static int getSPintValue(Context mContext, String key) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        int value = sharedPreferences.getInt(key, 0);
        return value;
    }


    //get boolean value from Shared Preference
    public static boolean getSPbooleanValue(Context mContext, String key) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        boolean value = sharedPreferences.getBoolean(key, false);
        return value;
    }

    /*public static  void setArrayList(Context context, String key, ArrayList<String> list){
        SharedPreferences sharedPreferences=context.getSharedPreferences("MyPref",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public static ArrayList<String> getArrayList(Context context,String key){
        SharedPreferences sharedPreferences=context.getSharedPreferences("MyPref",Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }
*/
}
