package com.grocery60.inc.grocery.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.grocery60.inc.grocery.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
