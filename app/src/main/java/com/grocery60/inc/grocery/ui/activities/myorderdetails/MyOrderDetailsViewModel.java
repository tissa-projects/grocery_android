package com.grocery60.inc.grocery.ui.activities.myorderdetails;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;

public class MyOrderDetailsViewModel extends AndroidViewModel {

    private Application application;
    private MyOrderDetailsRepository repository;

    public MyOrderDetailsViewModel(@NonNull Application application) {
        super(application);
        repository = MyOrderDetailsRepository.getInstance(application);
    }

    public MutableLiveData<String> getMyOrderDetails(RestAPIClientHelper restAPIClientHelper) {
        return repository.getOrderDetails(restAPIClientHelper);
    }
    public MutableLiveData<String> getBillingAddress(RestAPIClientHelper restAPIClientHelper) {
        return repository.getBillingAddress(restAPIClientHelper);
    }
}
