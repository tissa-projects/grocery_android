package com.grocery60.inc.grocery.ui.activities.Address;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.ActivityBillingAddressBinding;
import com.grocery60.inc.grocery.ui.activities.payment.INDPaymentActivity;
import com.grocery60.inc.grocery.ui.activities.payment.USPaymentActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class BillingAddressActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private ActivityBillingAddressBinding addressBinding;
    private AddressViewModel viewModel;
    private Context context;
    private Dialog dialogBilling, dialogCountry, dialogState;
    private NavController navController;
    private JSONObject customerObj = null;
    private JSONArray cartDataArray = null;
    private JSONObject bundle = null, jsonObjectAddress;
    private String name, companyName, address, houseNo, zip, city, state, country, shippingId = "0", shippingMethodName;
    ;
    private boolean isBillAddrAvail = false;
    private String strBillingAddressId = null, strCounntryShortCode = null, subTotal = "0", noTaxTotal = "0";
    private ArrayList<String> countryNameList = null, stateNameList = null, counntryShortCodeList = null;
    public static final String TAG = ShippingAddressActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_address);
        addressBinding = DataBindingUtil.setContentView(this, R.layout.activity_billing_address);
        viewModel = ViewModelProviders.of(this).get(AddressViewModel.class);
        context = BillingAddressActivity.this;
        getIntentData();
    }

    private void getIntentData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                Log.e(TAG, "getIntentData: bundle: " + bundle.toString());
                String data = bundle.getString("bundle");
                String data1 = bundle.getString("cartDataArray");
                strCounntryShortCode = bundle.getString("counntryShortCode");
                JSONObject bundleData = new JSONObject(data);
                cartDataArray = new JSONArray(data1);
                subTotal = bundle.getString("subTotal");
                noTaxTotal = bundle.getString("noTaxTotal");
                shippingId = bundle.getString("shippingId");
                shippingMethodName = bundle.getString("shippingMethdName");
                Log.e(TAG, "getIntentData: " + shippingId + " " + shippingMethodName);
                Log.e(TAG, "getIntentData: " + cartDataArray.toString());
                String strCustomer = bundleData.getString("customerObj");
                customerObj = new JSONObject(strCustomer);
                Log.e(TAG, "onCreate: customerObj: " + customerObj.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
        addressBinding.billingEdtName.setText("");
        addressBinding.billingEdtCompanyName.setText("");
        addressBinding.billingEdtAddressLine.setText("");
        addressBinding.billingEdtHouseNo.setText("");
        addressBinding.billingEdtZipCode.setText("");
        addressBinding.billingEdtCity.setText("");
        addressBinding.billingEdtState.setText("");
        addressBinding.billingEdtCountry.setText("");
        getLocation();
    }

    public void getLocation() {

        try {
            String strCountry = SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.COUNTRY);
            if (strCountry.equalsIgnoreCase("India")) {
                strCountry = "IN";
                addressBinding.billingEdtCountry.setText("India");
            } else {
                addressBinding.billingEdtCountry.setText("United States of America");
                strCountry = "US";
            }
            Log.e(TAG, "gotLocation: " + strCountry);
            strCounntryShortCode = strCountry;
            if (!addressBinding.billingEdtCountry.getText().toString().equalsIgnoreCase("")) {
                getStateList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Billing Address");
        findViewById(R.id.title_bar_back_arrow).setOnClickListener(v -> onBackPressed());
        if (VU.isConnectingToInternet(context)) {
            countryNameList = new ArrayList<>();
            counntryShortCodeList = new ArrayList<>();
            getBillingAddress();
            //getCountryList();

        }
        addressBinding.billingEdtCountry.setOnClickListener(this::onClick);
        // addressBinding.shippingEdtCountry.setOnClickListener(this::onClick);
        // addressBinding.shippingEdtState.setOnClickListener(this::onClick);
        addressBinding.billingEdtState.setOnClickListener(this::onClick);
        addressBinding.refresh.setOnRefreshListener(this);

        addressBinding.btnNext.setOnClickListener(v -> {    // ===================next btn click============================
            bundle = new JSONObject();
            try {
                setAddressData();
                bundle.put("customerObj", customerObj.toString());
                JSONObject billingAddressObj = new JSONObject();
                Log.e(TAG, "init: " + addressBinding.checkboxCurrentAddress.isChecked());

                Log.e(TAG, "init: " + name + " " + companyName + " " + address + " " + houseNo + " " + zip + " " + city + " " + state);
                billingAddressObj.put("name", name);
                billingAddressObj.put("company_name", companyName);
                billingAddressObj.put("address", address);
                billingAddressObj.put("house_number", houseNo);
                billingAddressObj.put("zip", zip);
                billingAddressObj.put("city", city);
                billingAddressObj.put("country", country);
                billingAddressObj.put("state", state);


                bundle.put("addressObj", billingAddressObj.toString());
                bundle.put("counntryShortCode", strCounntryShortCode);
                if (VU.isConnectingToInternet(context)) {
                    if (viewModel.biillingValidate(context, addressBinding)) {
                        if (isBillAddrAvail) {
                            updateBillingAddress();
                        } else {
                            setBillingAddress();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });

    }
/*

    //country dialog
    private void setCountry() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Country");
        // add a list
        builder.setItems(Utils.GetStringArray(countryNameList), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                addressBinding.billingEdtCountry.setText(countryNameList.get(position));
                addressBinding.billingEdtState.setText("");
                strCounntryShortCode = counntryShortCodeList.get(position);
                dialog.dismiss();
                if (countryNameList.get(position).equalsIgnoreCase(addressBinding.billingEdtCountry.getText().toString())) {
                    getStateList();
                }
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }
*/

    //state dialog
    private void setstateDialog(String type) {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select State");
        // add a list
        builder.setItems(Utils.GetStringArray(stateNameList), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                addressBinding.billingEdtState.setText(stateNameList.get(position));
                dialog.dismiss();
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void setBillingAddress() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.set_billing_address_api);
        try {

            reqObj.put("name", name);
            reqObj.put("company_name", companyName);
            reqObj.put("address", address);
            reqObj.put("house_number", houseNo);
            reqObj.put("zip", zip);
            reqObj.put("city", city);
            reqObj.put("country", strCounntryShortCode);
            reqObj.put("state", state);
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("priority", 1);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.set_billing_address_api));
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogBilling = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.setBillingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogBilling.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "setBillingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 201) {
                        JSONObject resultObj = jsonObject.getJSONObject("data");
                        Log.e(TAG, "setBillingAddress: resultObj: " + resultObj);
                        Intent intent;
                        if (SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CURRENCY_TYPE).equalsIgnoreCase("$")) {
                            intent = new Intent(context, USPaymentActivity.class);
                        } else {
                            intent = new Intent(context, INDPaymentActivity.class);
                        }
                        intent.putExtra("bundle", bundle.toString())
                                .putExtra("cartDataArray", cartDataArray.toString())
                                .putExtra("subTotal", subTotal)
                                .putExtra("noTaxTotal", noTaxTotal)
                                .putExtra("shippingId", shippingId)
                                .putExtra("shippingMethdName", shippingMethodName);
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void updateBillingAddress() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.update_billing_address_api) + strBillingAddressId + "/";
        try {
            Log.e(TAG, "updateBillingAddress: " + name + " " + companyName + " " + address + " " + houseNo + " " + zip + " " + city + " " + state);
            reqObj.put("name", name);
            reqObj.put("company_name", companyName);
            reqObj.put("address", address);
            reqObj.put("house_number", houseNo);
            reqObj.put("zip", zip);
            reqObj.put("city", city);
            reqObj.put("country", strCounntryShortCode);
            reqObj.put("state", state);
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("priority", 1);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.PUT));
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.set_billing_address_api));
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogBilling = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.setBillingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogBilling.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "updateBillingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONObject resultObj = jsonObject.getJSONObject("data");
                        Log.e(TAG, "updateBillingAddress: resultObj: " + resultObj);
                        Intent intent;
                        if (SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CURRENCY_TYPE).equalsIgnoreCase("$")) {
                            intent = new Intent(context, USPaymentActivity.class);
                        } else {
                            intent = new Intent(context, INDPaymentActivity.class);
                        }
                        intent.putExtra("bundle", bundle.toString())
                                .putExtra("cartDataArray", cartDataArray.toString())
                                .putExtra("subTotal", subTotal)
                                .putExtra("noTaxTotal", noTaxTotal)
                                .putExtra("shippingId", shippingId)
                                .putExtra("shippingMethdName", shippingMethodName);
                        startActivity(intent);
                    } else {
                        getResources().getString(R.string.no_response);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void getBillingAddress() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_billing_address_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogBilling = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getBillingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogBilling.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getBillingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");

                        if (resultArray.length() > 0) {
                            addressBinding.llCurrentAddress.setVisibility(View.VISIBLE);
                            addressBinding.txtNote.setVisibility(View.VISIBLE);
                            addressBinding.checkboxCurrentAddress.setChecked(true);
                            JSONObject resultObj = resultArray.getJSONObject(0);
                            Log.e(TAG, "getBillingAddress: resultObj: " + resultObj.toString());
                            jsonObjectAddress = resultObj;
                            name = resultObj.getString("name");
                            companyName = resultObj.getString("company_name");
                            address = resultObj.getString("address");
                            houseNo = resultObj.getString("house_number");
                            zip = resultObj.getString("zip");
                            city = resultObj.getString("city");
                            state = resultObj.getString("state");
                            country = resultObj.getString("country");
                            strCounntryShortCode = resultObj.getString("country");
                            addressBinding.edtCurrentAddress.setText(name + ",\n" + address + ", " + houseNo + ",\n" + city + ", " + state + ", " + country + ",\n" + zip);
                           /* addressBinding.billingEdtName.setText(resultObj.getString("name"));
                            addressBinding.billingEdtCompanyName.setText(resultObj.getString("company_name"));
                            addressBinding.billingEdtAddressLine.setText(resultObj.getString("address"));
                            addressBinding.billingEdtHouseNo.setText(resultObj.getString("house_number"));
                            addressBinding.billingEdtZipCode.setText(resultObj.getString("zip"));
                            addressBinding.billingEdtCity.setText(resultObj.getString("city"));
                            addressBinding.billingEdtState.setText(resultObj.getString("state"));
                            addressBinding.billingEdtCountry.setText(resultObj.getString("country"));*/
                            strBillingAddressId = resultObj.getString("id");
                            isBillAddrAvail = true;

                        } else {
                            addressBinding.checkboxCurrentAddress.setChecked(false);
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    /*private void getCountryList() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_country_api);
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogCountry = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getCountryList(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogCountry.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getCountryList : " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONArray("data");

                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                countryNameList.add(resultArray.getJSONObject(i).getString("country"));
                                counntryShortCodeList.add(resultArray.getJSONObject(i).getString("country_code"));
                            }

                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }*/

    private void getStateList() {
        stateNameList = new ArrayList<>();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_state_api) + addressBinding.billingEdtCountry.getText().toString();
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.get_state_api));
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //  dialogState = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getStateList(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                //     dialogState.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getStateList : " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");

                        if (resultArray.length() > 0) {
                            for (int i = 0; i < resultArray.length(); i++) {
                                stateNameList.add(resultArray.getJSONObject(i).getString("state"));
                            }

                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.billing_edt_country:
                //       setCountry();
                break;

            case R.id.billing_edt_state:
                if (!addressBinding.billingEdtCountry.getText().toString().equalsIgnoreCase("")) {
                    setstateDialog("billing");
                } else {
                    Utils.getToast(context, "First select country");
                }
                break;

            case R.id.checkbox_current_address:
                if (addressBinding.checkboxCurrentAddress.isChecked()) {
                    try {
                        name = jsonObjectAddress.getString("name");
                        companyName = jsonObjectAddress.getString("company_name");
                        address = jsonObjectAddress.getString("address");
                        houseNo = jsonObjectAddress.getString("house_number");
                        zip = jsonObjectAddress.getString("zip");
                        city = jsonObjectAddress.getString("city");
                        state = jsonObjectAddress.getString("state");
                        country = jsonObjectAddress.getString("country");
                        strCounntryShortCode = jsonObjectAddress.getString("country");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    setAddressData();
                }
                break;
        }
    }

    private void setAddressData() {
        if (addressBinding.checkboxCurrentAddress.isChecked()) {
            try {
                name = jsonObjectAddress.getString("name");
                companyName = jsonObjectAddress.getString("company_name");
                address = jsonObjectAddress.getString("address");
                houseNo = jsonObjectAddress.getString("house_number");
                zip = jsonObjectAddress.getString("zip");
                city = jsonObjectAddress.getString("city");
                state = jsonObjectAddress.getString("state");
                country = jsonObjectAddress.getString("country");
                strCounntryShortCode = jsonObjectAddress.getString("country");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            name = addressBinding.billingEdtName.getText().toString();
            companyName = addressBinding.billingEdtCompanyName.getText().toString();
            address = addressBinding.billingEdtAddressLine.getText().toString();
            houseNo = addressBinding.billingEdtHouseNo.getText().toString();
            zip = addressBinding.billingEdtZipCode.getText().toString();
            city = addressBinding.billingEdtCity.getText().toString();
            state = addressBinding.billingEdtState.getText().toString();
            country = strCounntryShortCode;
        }
    }


    @Override   // swipe refresh
    public void onRefresh() {
        if (VU.isConnectingToInternet(context)) {
            countryNameList = new ArrayList<>();
            counntryShortCodeList = new ArrayList<>();
            getBillingAddress();
            //getCountryList();
        }
        addressBinding.refresh.setRefreshing(false);
    }
}
