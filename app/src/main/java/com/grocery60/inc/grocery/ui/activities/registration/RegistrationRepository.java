package com.grocery60.inc.grocery.ui.activities.registration;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.ServiceCall.RestAsyncTask;

public class RegistrationRepository  {

    private static RegistrationRepository instance;

    private static Application applicationContext;
    private static final String TAG = RegistrationRepository.class.getSimpleName();

    public static RegistrationRepository getInstance(Application application) {
        applicationContext = application;
        if (instance == null) {
            instance = new RegistrationRepository();

        }
        return instance;
    }
    /*public LoginRepository(Application application) {
        this.application = application;
    }*/

    public MutableLiveData<String> setRegistrationData(RestAPIClientHelper helper) {
         MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(applicationContext.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }

    public MutableLiveData<String> login(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(applicationContext.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }


    public MutableLiveData<String> getCustomer(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(applicationContext.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }

    public MutableLiveData<String> createCustomer(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(applicationContext.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }
}
