package com.grocery60.inc.grocery.ui.activities.password;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.ActivityForgotBinding;

import org.json.JSONObject;

public class ForgotActivity extends AppCompatActivity {

    private ActivityForgotBinding forgotBinding;
    private ChangePasswordViewModel forgotViewModel;
    private Context context;
    private Dialog dialog;
    public static final String TAG = ForgotActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        forgotBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot);
        forgotViewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel.class);
        context = ForgotActivity.this;
        init();

    }

    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Change Password");
        forgotBinding.setUpdateBtnClick(() -> {
            if (validation()){
                if (VU.isConnectingToInternet(context))
                    ChangePassword();
            }
        });
    }

    private void ChangePassword() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.forgot_password);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("new_password1", forgotBinding.newPassword.getText().toString());
            stringMap.put("new_password2", forgotBinding.conformPassword.getText().toString());
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        forgotViewModel.changePassoword(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "ForgotPassowrd: " + jsonObject.toString());
                    if (jsonObject.getInt("response_code") == 200) {
                        Utils.getToast(context,getResources().getString(R.string.password_snt));
                    } else
                    Utils.getToast(context,getResources().getString(R.string.no_response));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private boolean validation() {
        if (VU.isEmpty(forgotBinding.currentPassword)) {
            forgotBinding.currentPassword.setError(getResources().getString(R.string.current_password_validate));
            forgotBinding.currentPassword.setFocusable(true);
            return false;
        } else if (VU.isEmpty(forgotBinding.newPassword)) {
            forgotBinding.newPassword.setFocusable(true);
            forgotBinding.newPassword.setError(getResources().getString(R.string.new_password_validate));
            return false;
        } else if (VU.isEmpty(forgotBinding.conformPassword)) {
            forgotBinding.conformPassword.setFocusable(true);
            forgotBinding.conformPassword.setError(getResources().getString(R.string.confrm_password_validate));
            return false;
        } else if (forgotBinding.newPassword.getText().toString().contentEquals(forgotBinding.conformPassword.getText().toString())) {
            Utils.getToast(context,getResources().getString(R.string.password_not_match_validate));
            return false;
        }
        return true;
    }

}
