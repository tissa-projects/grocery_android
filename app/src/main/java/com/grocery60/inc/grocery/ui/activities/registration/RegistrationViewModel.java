package com.grocery60.inc.grocery.ui.activities.registration;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.Validations;
import com.grocery60.inc.grocery.databinding.ActivityRegistrationBinding;

public class RegistrationViewModel extends AndroidViewModel {
    private Application application;
    private RegistrationRepository registrationRepository;

    public RegistrationViewModel(@NonNull Application application) {
        super(application);
        registrationRepository = RegistrationRepository.getInstance(application);
    }

    public MutableLiveData<String> setRegistrationData(RestAPIClientHelper restAPIClientHelper) {
        return registrationRepository.setRegistrationData(restAPIClientHelper);
    }

    public MutableLiveData<String> login(RestAPIClientHelper restAPIClientHelper) {
        return registrationRepository.login(restAPIClientHelper);
    }

    public LiveData<String> getCustomer(RestAPIClientHelper restAPIClientHelper) {
        return registrationRepository.getCustomer(restAPIClientHelper);
    }

    public LiveData<String> createCustomer(RestAPIClientHelper restAPIClientHelper) {
        return registrationRepository.createCustomer(restAPIClientHelper);
    }

    public boolean validate(Context context, ActivityRegistrationBinding registrationBinding){
        if (registrationBinding.edtSalutation.getText().toString().equalsIgnoreCase("")){
            Utils.getToast(context,context.getResources().getString(R.string.salutation_validate));
            return  false;
        }else if (registrationBinding.edtUsername.getText().toString().equalsIgnoreCase("")){
            Utils.getToast(context,context.getResources().getString(R.string.username_validate));
            return  false;
        }else if (registrationBinding.edtFName.getText().toString().equalsIgnoreCase("")){
            Utils.getToast(context,context.getResources().getString(R.string.fname_validate));
            return  false;
        }else if (registrationBinding.edtLName.getText().toString().equalsIgnoreCase("")){
            Utils.getToast(context,context.getResources().getString(R.string.lname_validate));
            return  false;
        }else if (registrationBinding.edtEmail.getText().toString().equalsIgnoreCase("") || Validations.isEnailValid(registrationBinding.edtEmail.getText().toString())){
            Utils.getToast(context,context.getResources().getString(R.string.enter_email_validate));
            return  false;
        }else if (registrationBinding.edtMNo.getText().toString().equalsIgnoreCase("")){
            Utils.getToast(context,context.getResources().getString(R.string.mb_validate));
            return  false;
        }else if (registrationBinding.edtPassword.getText().toString().equalsIgnoreCase("")){
            Utils.getToast(context,context.getResources().getString(R.string.password_validate));
            return  false;
        }else if (registrationBinding.edtPassword.getText().toString().length()<=8){
            Utils.getToast(context,context.getResources().getString(R.string.password_validate_character));
            return  false;
        }else if (registrationBinding.edtCPassword.getText().toString().equalsIgnoreCase("")){
            Utils.getToast(context,context.getResources().getString(R.string.confrm_password_validate));
            return  false;
        }else if (!registrationBinding.edtCPassword.getText().toString().equalsIgnoreCase(registrationBinding.edtPassword.getText().toString())){
            Utils.getToast(context,context.getResources().getString(R.string.password_not_match_validate));
            return  false;
        }
        return true;
    }
}

