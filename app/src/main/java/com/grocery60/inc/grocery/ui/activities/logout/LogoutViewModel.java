package com.grocery60.inc.grocery.ui.activities.logout;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;

public class LogoutViewModel extends AndroidViewModel {

    private LogoutRepository repository;

    public LogoutViewModel(@NonNull Application application) {
        super(application);
        repository = LogoutRepository.getInstance(application);
    }

    public MutableLiveData<String> logout(RestAPIClientHelper restAPIClientHelper) {
        return repository.logout(restAPIClientHelper);
    }
}
