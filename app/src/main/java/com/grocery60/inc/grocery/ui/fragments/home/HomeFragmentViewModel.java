package com.grocery60.inc.grocery.ui.fragments.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;

public class HomeFragmentViewModel extends AndroidViewModel {
    private HomeFragmentRepository repository;

    public HomeFragmentViewModel(@NonNull Application application) {
        super(application);
        repository = new HomeFragmentRepository(application);
    }


    public MutableLiveData<String>  getData(RestAPIClientHelper helper){
        return repository.getData(helper);
    }

    public MutableLiveData<String>  login(RestAPIClientHelper helper){
        return repository.login(helper);
    }

    public MutableLiveData<String> getCart(RestAPIClientHelper helper){
        return repository.getCart(helper);
    }
    public MutableLiveData<String> createCart(RestAPIClientHelper helper){
        return repository.createCart(helper);
    }

    public LiveData<String> getCartCount(RestAPIClientHelper restAPIClientHelper) {
        return repository.getCartCount(restAPIClientHelper);
    }

    public LiveData<String> searchStore(RestAPIClientHelper restAPIClientHelper) {
        return repository.searchStore(restAPIClientHelper);
    }
    public LiveData<String> deleteCart(RestAPIClientHelper restAPIClientHelper) {
        return repository.deleteCart(restAPIClientHelper);
    }
}
