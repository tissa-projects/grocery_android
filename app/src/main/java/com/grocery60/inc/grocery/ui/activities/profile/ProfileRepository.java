package com.grocery60.inc.grocery.ui.activities.profile;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.ServiceCall.RestAsyncTask;

public class ProfileRepository {
    private static Application application;
    private static ProfileRepository instance;
    private static final String TAG = ProfileRepository.class.getSimpleName();


    public static ProfileRepository getInstance(Application applicationContext) {
        application = applicationContext;
        if (instance == null) {
            instance = new ProfileRepository();

        }
        return instance;
    }

    public MutableLiveData<String> getApiResponse(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }

    public MutableLiveData<String> updateProfile(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }
}
