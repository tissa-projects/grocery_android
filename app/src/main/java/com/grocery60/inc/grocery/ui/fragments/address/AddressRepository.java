package com.grocery60.inc.grocery.ui.fragments.address;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.ServiceCall.RestAsyncTask;

public class AddressRepository {

    private static Application application;
    private static AddressRepository instance;
    private static final String TAG = AddressRepository.class.getSimpleName();


    public static AddressRepository getInstance(Application applicationContext) {
        application = applicationContext;
        if (instance == null) {
            instance = new AddressRepository();

        }
        return instance;
    }

    public MutableLiveData<String> getShippingAddress(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }

    public MutableLiveData<String> setShippingAddress(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }

    public MutableLiveData<String> setBillingAddress(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }

    public MutableLiveData<String> getBillingAddress(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }
}
