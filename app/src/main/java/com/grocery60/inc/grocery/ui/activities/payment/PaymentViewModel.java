package com.grocery60.inc.grocery.ui.activities.payment;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.databinding.ActivityUsPaymentBinding;


public class PaymentViewModel extends AndroidViewModel {
    private PaymentRepository repository;

    public PaymentViewModel(@NonNull Application application) {
        super(application);
        repository = PaymentRepository.getInstance(application);
    }

    public MutableLiveData<String> payment(RestAPIClientHelper helper) {
        return repository.payment(helper);
    }

    public MutableLiveData<String> setOrder(RestAPIClientHelper helper) {
        return repository.setOrder(helper);
    }
    public MutableLiveData<String> setOrderItem(RestAPIClientHelper helper) {
        return repository.setOrderItem(helper);
    }

    public MutableLiveData<String> getShippingMethods(RestAPIClientHelper helper) {
        return repository.shippingMethods(helper);
    }

    public MutableLiveData<String> showCartList(RestAPIClientHelper restAPIClientHelper) {
        return repository.showCartList(restAPIClientHelper);
    }

    public MutableLiveData<String> addFee(RestAPIClientHelper restAPIClientHelper) {
        return repository.addFee(restAPIClientHelper);
    }

    public MutableLiveData<String> getShippingFees(RestAPIClientHelper restAPIClientHelper) {
        return repository.getShippingFees(restAPIClientHelper);
    }

    public MutableLiveData<String> getIndiaPaymentDetails(RestAPIClientHelper restAPIClientHelper) {
        return repository.getIndiaPaymentDetails(restAPIClientHelper);
    }



    public boolean validate(Context context, ActivityUsPaymentBinding paymentBinding) {
        if (paymentBinding.edtCardHoldern.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context,  context.getResources().getString(R.string.card_holder_name_validate));
            return false;
        } else if (paymentBinding.edtCreditCardNo.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.card_no_validate));
            return false;
        } else if (paymentBinding.edtSecurityCode.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.security_code_validate));
            return false;
        } else if (paymentBinding.edtMonth.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.month_validate));
            return false;
        } else if (paymentBinding.edtYear.getText().toString().equalsIgnoreCase("")) {
            Utils.getToast(context, context.getResources().getString(R.string.year_validate));
            return false;
        }
        return true;
    }

   /* public boolean validateInd(Context context, int lastSelectedPosition) {
       if (lastSelectedPosition == -1) {
            Utils.getToast(context,  context.getResources().getString(R.string.shipping_method_validate));
            return false;
        }
        return true;
    }*/
}

