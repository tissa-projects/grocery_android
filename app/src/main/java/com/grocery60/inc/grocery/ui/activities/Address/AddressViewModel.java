package com.grocery60.inc.grocery.ui.activities.Address;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.databinding.ActivityBillingAddressBinding;
import com.grocery60.inc.grocery.databinding.ActivityShippingAddressBinding;

public class AddressViewModel extends AndroidViewModel {
    private AddressRepository repository;

    public AddressViewModel(@NonNull Application application) {
        super(application);

        repository = AddressRepository.getInstance(application);
    }

    public MutableLiveData<String> getShippingAddress(RestAPIClientHelper helper) {
        return repository.getShippingAddress(helper);
    }

    public MutableLiveData<String> setShippingAddress(RestAPIClientHelper helper) {
        return repository.setShippingAddress(helper);
    }

    public MutableLiveData<String> getBillingAddress(RestAPIClientHelper helper) {
        return repository.getBillingAddress(helper);
    }

    public MutableLiveData<String> setBillingAddress(RestAPIClientHelper helper) {
        return repository.setBillingAddress(helper);
    }

    public MutableLiveData<String> getCountryList(RestAPIClientHelper helper) {
        return repository.getCountryList(helper);
    }

    public MutableLiveData<String> getStateList(RestAPIClientHelper helper) {
        return repository.getStateList(helper);
    }

    public MutableLiveData<String> getCustomer(RestAPIClientHelper helper) {
        return repository.customer(helper);
    }

    public MutableLiveData<String> getShippingFees(RestAPIClientHelper restAPIClientHelper) {
        return repository.getShippingFees(restAPIClientHelper);
    }


    public boolean shippingValidate(Context context, ActivityShippingAddressBinding addressBinding) {
        if (!addressBinding.checkboxCurrentAddress.isChecked()) {
            if (addressBinding.shippingEdtName.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.name));
                return false;
            } else if (addressBinding.shippingEdtAddressLine.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.address));
                return false;
            } else if (addressBinding.shippingEdtZipCode.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.zip_postal));
                return false;
            } else if (addressBinding.shippingEdtCity.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.city));
                return false;
            } else if (addressBinding.shippingEdtState.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.state));
                return false;
            } else if (addressBinding.shippingEdtCountry.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.country));
                return false;
            }
        }
        return true;
    }

    public boolean biillingValidate(Context context, ActivityBillingAddressBinding addressBinding) {
        if (!addressBinding.checkboxCurrentAddress.isChecked()) {
            if (addressBinding.billingEdtName.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.name));
                return false;
            } else if (addressBinding.billingEdtAddressLine.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.address));
                return false;
            } else if (addressBinding.billingEdtZipCode.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.zip_postal));
                return false;
            } else if (addressBinding.billingEdtCity.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.city));
                return false;
            } else if (addressBinding.billingEdtState.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.state));
                return false;
            } else if (addressBinding.billingEdtCountry.getText().toString().equalsIgnoreCase("")) {
                Utils.getToast(context, "Enter " + context.getResources().getString(R.string.country));
                return false;
            }
        }
        return true;
    }

    public boolean validateShipping(Context context, int lastSelectedPosition) {
        if (lastSelectedPosition == -1) {
            Utils.getToast(context,  context.getResources().getString(R.string.shipping_method_validate));
            return false;
        }
        return true;
    }
}
