package com.grocery60.inc.grocery.ui.activities.payment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.ActivityUsPaymentBinding;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class USPaymentActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityUsPaymentBinding paymentBinding;
    private PaymentViewModel paymentViewModel;
    // private PaymentShippingMethodAdapter paymentShippingMethodAdapter;
    private Context context;
    private Dialog dialog, dialogGetItem;
    private JSONArray cartDataArray = null;
    private JSONObject customerObj = null, addressObj = null, OrderDetails = null;
    private NavController navController;
    private String tip = "0", customTip = "0", shippingId = "0", shippingMethdName, strCounntryShortCode, subTotal = "0", noTaxTotal = "0";
    private String currencyType;
    public static final String TAG = USPaymentActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        paymentBinding = DataBindingUtil.setContentView(this, R.layout.activity_us_payment);
        paymentViewModel = ViewModelProviders.of(this).get(PaymentViewModel.class);
        context = USPaymentActivity.this;
        currencyType = SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CURRENCY_TYPE);
        getIntentData();
        if (VU.isConnectingToInternet(context)) {
            addFees();
        }
        init();
        // setRecyclerForShippingMethod();
    }

    /* private void setRecyclerForShippingMethod() {
         paymentShippingMethodAdapter = new PaymentShippingMethodAdapter(context);
         paymentShippingMethodAdapter.radioBtnClick((position, jsonObject) -> {   // recycler click

             try {
                 shippingId = jsonObject.getString("id");
                 lastSelectedPosition = position;
                 addFees();
             }catch (Exception e){e.printStackTrace();}
         });
         paymentBinding.setShippingMethodAdapter(paymentShippingMethodAdapter);
     }
 */
    private void init() {
        context = USPaymentActivity.this;
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Payment");
        findViewById(R.id.title_bar_back_arrow).setOnClickListener(v -> onBackPressed());
      /*  if (VU.isConnectingToInternet(context)) {
            getShippingMethods();
        }*/
        paymentBinding.serviceFeeNote.setOnClickListener(v -> {
            new SimpleTooltip.Builder(this)
                    .anchorView(paymentBinding.serviceFeeNote)
                    .text(getResources().getString(R.string.service_fee_note))
                    .gravity(Gravity.TOP)
                    .animated(true)
                    .transparentOverlay(false).textColor(Color.WHITE)
                    .build()
                    .show();
            //  CustomDialogs.dialogShowMsg(context, getResources().getString(R.string.service_fee_note));
        });

        paymentBinding.edtMonth.setOnClickListener(this::onClick);
        paymentBinding.edtYear.setOnClickListener(this::onClick);
        paymentBinding.btnNext.setOnClickListener(v -> {
            if (VU.isConnectingToInternet(context)) {
                if (paymentViewModel.validate(context, paymentBinding)) {
                    setOrderDetails();
                }
            }
        });

        paymentBinding.txtChangeTip.setOnClickListener(v -> {
            dialogCustomTip();
        });

    }

    private void getIntentData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                Log.e(TAG, "getIntentData: bundle: " + bundle.toString());
                String data = bundle.getString("bundle");
                String data1 = bundle.getString("cartDataArray");
                strCounntryShortCode = bundle.getString("counntryShortCode");
                JSONObject bundleData = new JSONObject(data);
                Log.e(TAG, "getIntentData: data1: " + data1);
                cartDataArray = new JSONArray(data1);
                Log.e(TAG, "getIntentData: " + cartDataArray.toString());
                String strCustomer = bundleData.getString("customerObj");
                String strAddress = bundleData.getString("addressObj");
                subTotal = bundle.getString("subTotal");
                noTaxTotal = bundle.getString("noTaxTotal");

                shippingMethdName = bundle.getString("shippingMethdName");
                shippingId = bundle.getString("shippingId");
                //      String strShippingAddress = bundleData.getString("shippingAdddressObj");
                customerObj = new JSONObject(strCustomer);
                addressObj = new JSONObject(strAddress);
                //shippingAddressObj = new JSONObject(strShippingAddress);

                Log.e(TAG, "onCreate: customerObj: " + customerObj.toString());
                Log.e(TAG, "onCreate: addressObj: " + addressObj.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void datePicker(String type) {
        final Calendar myCalendar = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(context, new MonthPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {
                Log.d(TAG, "selectedMonth : " + selectedMonth + " selectedYear : " + selectedYear);
                String strSelectedMonth = String.valueOf(selectedMonth).length() > 1 ? (selectedMonth + 1) + "" : "0" + (selectedMonth + 1);
                Log.e(TAG, "onDateSet: +strSelectedMonth: " + strSelectedMonth);
                paymentBinding.edtYear.setText(selectedYear + "");
                paymentBinding.edtMonth.setText(strSelectedMonth);
            }
        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH));

        builder.setActivatedMonth(Calendar.MONTH)
                .setMinYear(1990)
                .setActivatedYear(myCalendar.get(Calendar.YEAR))
                .setMaxYear(2030)
                .setMinMonth(Calendar.JANUARY)
                .setTitle("Select " + type)
                .setMonthRange(Calendar.JANUARY, Calendar.DECEMBER)
                // .setMaxMonth(Calendar.OCTOBER)
                // .setYearRange(1890, 1890)
                // .setMonthAndYearRange(Calendar.FEBRUARY, Calendar.OCTOBER, 1890, 1890)
                //.showMonthOnly()
                // .showYearOnly()
                .setOnMonthChangedListener(new MonthPickerDialog.OnMonthChangedListener() {
                    @Override
                    public void onMonthChanged(int selectedMonth) {
                        Log.d(TAG, "Selected month : " + selectedMonth);
                        String strSelectedMonth = selectedMonth + "".length() > 1 ? (selectedMonth + 1) + "" : "0" + (selectedMonth + 1);
                        paymentBinding.edtMonth.setText(strSelectedMonth);
                    }
                })
                .setOnYearChangedListener(new MonthPickerDialog.OnYearChangedListener() {
                    @Override
                    public void onYearChanged(int selectedYear) {
                        Log.d(TAG, "Selected year : " + selectedYear);
                        paymentBinding.edtYear.setText(selectedYear + "");
                    }
                })
                .build()
                .show();
    }


    private void payment(String orderId) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        JSONObject reqObj = null;
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.payment_api);
        try {
            String currency = "USD";
            reqObj = new JSONObject();
            JSONObject cardObj = new JSONObject();
            JSONObject billingDetails = new JSONObject();
            JSONObject metadata = new JSONObject();
            JSONObject address = new JSONObject();
            Log.e(TAG, "payment: addressObj: " + addressObj.toString());
            metadata.put("store_id", SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.STORE_ID_WRT_CART))
                    .put("shippingmethod_id",shippingId).put("order_id", orderId)
                    .put("phone", customerObj.getString("phone_number"))
                    .put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            address.put("city", addressObj.getString("city")).put("country", strCounntryShortCode)
                    .put("line1", addressObj.getString("address")).put("line2", "")
                    .put("postal_code", addressObj.getString("zip")).put("state", addressObj.getString("state"));
            billingDetails.put("address", address);
            cardObj.put("number", paymentBinding.edtCreditCardNo.getText().toString()).put("exp_month", paymentBinding.edtMonth.getText().toString())
                    .put("exp_year", paymentBinding.edtYear.getText().toString()).put("cvc", paymentBinding.edtSecurityCode.getText().toString());

            reqObj.put("amount", paymentBinding.txtTotal.getText().toString().substring(1))
                    .put("currency", currency).put("receipt_email", customerObj.getString("email"))
                    .put("type", "card").put("card", cardObj)
                    .put("billing_details", billingDetails).put("metadata", metadata);
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
            helper.setAction(getResources().getString(R.string.payment_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        JSONObject finalReqObj = reqObj;
        paymentViewModel.payment(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "payment: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONObject resultObj = jsonObject.getJSONObject("data");
                        Log.e(TAG, "payment: " + resultObj.toString());
                        Utils.getToast(context, getResources().getString(R.string.order_placed));
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.STORE_ID_WRT_CART, "");
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
                        Intent intent = new Intent(context, BackToStoreActivity.class)
                                .putExtra("OrderDetails", OrderDetails.toString())
                                .putExtra("addressDetails", addressObj.toString());
                        startActivity(intent);
                        //showCartList(resultObj);   //for adding cart items to order item after payment success
                    } else if (jsonObject.getInt("response_code") == 400) {
                        Utils.getToast(context, getResources().getString(R.string.incorrect_cart_details));
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

  /*  private void setOrder() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.set_order_api);
        try {
            reqObj.put("status", "active");  //customer always be active
            String currency = ((SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.COUNTRY).contentEquals("India"))) ? "RS" : "USD";
            reqObj.put("currency", currency);
            reqObj.put("subtotal", paymentBinding.txtSubTotal.getText().toString().substring(1));
            reqObj.put("total", paymentBinding.txtTotal.getText().toString().substring(1));
            reqObj.put("extra", "test");
            reqObj.put("customer", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("tip", paymentBinding.txtTip.getText().toString().substring(1));
            reqObj.put("service_fee", paymentBinding.txtServiceFee.getText().toString().substring(1));
            reqObj.put("tax", paymentBinding.txtVat.getText().toString().substring(1));
            reqObj.put("discount", paymentBinding.txtDiscount.getText().toString().substring(1));
            reqObj.put("shipping_fee", paymentBinding.txtShippingFee.getText().toString().substring(1));
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        paymentViewModel.setOrder(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "setOrder: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 201) {
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        Log.e(TAG, "setOrder: " + dataObj.toString());
                        payment(dataObj.getString("id"));   //order id

                    } else {
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }*/

    private void setOrderDetails() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.set_order_details_api);
        try {
            reqObj.put("status", "active");  //customer always be active
            String currency = "USD";
            reqObj.put("currency", currency);
            reqObj.put("subtotal", paymentBinding.txtSubTotal.getText().toString().substring(1));
            reqObj.put("total", paymentBinding.txtTotal.getText().toString().substring(1));
            reqObj.put("extra", paymentBinding.edtExtraAnnotation.getText().toString());
            reqObj.put("customer", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("tip", paymentBinding.txtTip.getText().toString().substring(1));
            reqObj.put("service_fee", paymentBinding.txtServiceFee.getText().toString().substring(1));
            reqObj.put("tax", paymentBinding.txtVat.getText().toString().substring(1));
            reqObj.put("discount", paymentBinding.txtDiscount.getText().toString().substring(1));
            reqObj.put("shipping_fee", paymentBinding.txtShippingFee.getText().toString().substring(1));
            JSONArray reqArray = new JSONArray();
            for (int i = 0; i < cartDataArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("cart_item_id", cartDataArray.getJSONObject(i).getString("cart_item_id"));
                jsonObject.put("cart_id", cartDataArray.getJSONObject(i).getString("cart_id"));
                jsonObject.put("product_id", cartDataArray.getJSONObject(i).getString("product_id"));
                jsonObject.put("product_name", cartDataArray.getJSONObject(i).getString("product_name"));
                jsonObject.put("product_url", cartDataArray.getJSONObject(i).getString("product_url"));
                jsonObject.put("price", cartDataArray.getJSONObject(i).getString("price"));
                jsonObject.put("quantity", cartDataArray.getJSONObject(i).getString("quantity"));
                jsonObject.put("line_total", cartDataArray.getJSONObject(i).getString("line_total"));
                jsonObject.put("canceled", false);
                reqArray.put(jsonObject);
            }
            reqObj.put("items", reqArray);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
            helper.setAction(getResources().getString(R.string.set_order_details_api));
        } catch (Exception e) {
            e.printStackTrace();

        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        paymentViewModel.setOrder(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "setOrderDetails: " + jsonObject);
                    JSONObject dataObj = jsonObject.getJSONObject("data");
                    if (jsonObject.getInt("response_code") == 200) {
                        OrderDetails = dataObj;
                        Log.e(TAG, "setOrderDetails: " + dataObj.toString());
                        payment(dataObj.getString("order_id"));   //order id
                    } else if (jsonObject.getInt("response_code") == 500) {
                        CustomDialogs.dialogShowMsg(context, dataObj.getString("msg"));
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


   /* private void setOrderItems(JSONArray jsonArray, JSONObject obj) {
        RestAPIClientHelper helper = new RestAPIClientHelper();

        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.set_order_item_api);
        try {

            JSONArray reqArray = new JSONArray();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("cart_item_id", jsonArray.getJSONObject(i).getString("cart_item_id"));
                jsonObject.put("cart_id", jsonArray.getJSONObject(i).getString("cart_id"));
                jsonObject.put("product_id", jsonArray.getJSONObject(i).getString("product_id"));
                jsonObject.put("product_name", jsonArray.getJSONObject(i).getString("product_name"));
                jsonObject.put("product_url", jsonArray.getJSONObject(i).getString("product_url"));
                jsonObject.put("price", jsonArray.getJSONObject(i).getString("price"));
                jsonObject.put("quantity", jsonArray.getJSONObject(i).getString("quantity"));
                jsonObject.put("line_total", jsonArray.getJSONObject(i).getString("line_total"));
                jsonObject.put("order_id", obj.getJSONObject("metadata").getString("OrderId"));
                jsonObject.put("canceled", false);
                reqArray.put(jsonObject);
            }
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqArray.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogGetItem = ProgressDialog.show(context, "Please wait", "Loading...");
        paymentViewModel.setOrderItem(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogGetItem.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "setOrderItems: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        //  JSONObject dataObj = jsonObject.getJSONObject("data");

                        JSONObject bundleObject = new JSONObject();
                        Bundle bundle = new Bundle();
                        bundleObject.put("amount", obj.getString("amount")).put("transaction_id", obj.getString("id"))
                                .put("status", "active").put("payment_method", obj.getJSONArray("payment_method_types").getString(0))
                                .put("orderId", obj.getJSONObject("metadata").getString("OrderId"))
                                .put("shippingAddress", shippingAddressObj.getString("address"))
                                .put("customerEmail", customerObj.getString("email"));
                        bundle.putString("bundleObj", bundleObject.toString());
                        navController.navigate(R.id.summaryFragment, bundle);
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.no_response), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void showCartList(JSONObject obj) {  //for adding cart items to order item after payment success

        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_cart_item) + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID));
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogGetItem = ProgressDialog.show(context, "Please wait", "Loading...");
        paymentViewModel.showCartList(helper).observe(this, (response) -> {
            try {
                dialogGetItem.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "showCartList: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONArray("data");
                        if (resultArray.length() > 0) {
                            //setOrderItems(resultArray, obj);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }
*/

    private void addFees() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        //   String tip = (cartBinding.txtTip.getText().toString().equalsIgnoreCase("")) ? "0" : cartBinding.txtTip.getText().toString();
        try {
            JSONObject reqObj = new JSONObject();
            reqObj.put("sub_total", subTotal);
            reqObj.put("no_tax_total", noTaxTotal);
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("shipping_id", shippingId);

            if (customTip.equalsIgnoreCase("0")) {
                reqObj.put("tip", tip);  // %
            } else {
                reqObj.put("custom_tip", customTip); // normal value
            }
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.add_fees));
            helper.setUrlParameter(reqObj.toString());
            helper.setAction(getResources().getString(R.string.add_fees));
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        paymentViewModel.addFee(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "addFees: " + responseObj);
                    if (responseObj.getInt("response_code") == 200) {
                        responseObj = responseObj.getJSONObject("data");
                        paymentBinding.txtSubTotal.setText(currencyType + responseObj.getString("sub_total"));
                        paymentBinding.txtVat.setText(currencyType + responseObj.getString("tax"));
                        paymentBinding.txtTip.setText(currencyType + responseObj.getString("tip"));
                        paymentBinding.txtServiceFee.setText(currencyType + responseObj.getString("service_fee"));
                        paymentBinding.txtDiscount.setText(currencyType + responseObj.getString("discount"));
                        paymentBinding.txtTotal.setText(currencyType + responseObj.getString("total"));
                        paymentBinding.txtShippingFee.setText(currencyType + responseObj.getString("shipping_fee"));
                        //  startActivity(new Intent(context, CheckOutActivity.class));
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }


    /*private void getShippingMethods() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_shipping_methods)+"status=ACTIVE&store_id="+SharePreferenceUtil.getSPstringValue(context,SharePreferenceUtil.STORE_ID_WRT_CART));
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogGetItem = ProgressDialog.show(context, "Please wait", "Loading...");
        paymentViewModel.getShippingFees(helper).observe(this, (response) -> {
            try {
                dialogGetItem.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "getShippingMethods: " + responseObj);
                    if (responseObj.getInt("response_code") == 200) {
                        JSONArray jsonArray = responseObj.getJSONObject("data").getJSONArray("results");
                        if (jsonArray.length() ==0){Utils.getToast(context,"No shipping method available");}
                        paymentShippingMethodAdapter.setData(jsonArray);
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }
*/

    public void dialogCustomTip() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_delevery_tip);
        RadioGroup radioGroup = ((RadioGroup) dialog.findViewById(R.id.radio_grup));
        EditText edtTip = ((EditText) dialog.findViewById(R.id.edt_tip));
        ((RadioButton) dialog.findViewById(R.id.amt_4)).setText(currencyType + "4");
        ((RadioButton) dialog.findViewById(R.id.amt_6)).setText(currencyType + "6");
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) dialog.findViewById(checkedId); // find the radiobutton by returned id
                if (radioButton.getText().toString().equals("Other amount")) {
                    edtTip.setEnabled(true);
                } else {
                    edtTip.setEnabled(false);
                }
            }
        });

        ((Button) dialog.findViewById(R.id.btn_submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();
                if (selectedId == -1) {
                    Utils.getToast(context, getResources().getString(R.string.select_tip_option));
                } else {
                    // find the radiobutton by returned id
                    RadioButton radioButton = (RadioButton) dialog.findViewById(selectedId);
                    if (radioButton.getText().equals("Other amount")) {
                        if (edtTip.getText().toString().equals("")) {
                            Utils.getToast(context, "Please enter Other amount");

                        } else {
                            paymentBinding.txtTip.setText(currencyType + edtTip.getText().toString());
                            customTip = edtTip.getText().toString();
                            tip = "0";
                            if (VU.isConnectingToInternet(context)) {
                                addFees();
                            }
                            dialog.dismiss();
                        }
                    } else {
                        if (radioButton.getText().toString().contains(currencyType)) {
                            paymentBinding.txtTip.setText(radioButton.getText().toString());
                            customTip = radioButton.getText().toString().substring(1);
                            tip = "0";
                        } else {
                            paymentBinding.txtTip.setText(currencyType + radioButton.getText().toString().split("%")[0]);
                            customTip = "0";
                            tip = radioButton.getText().toString().split("%")[0];
                        }
                        dialog.dismiss();
                        if (VU.isConnectingToInternet(context)) {
                            addFees();
                        }
                    }

                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edt_month:
                datePicker("Month");
                break;
            case R.id.edt_year:
                datePicker("Year");
                break;
        }
    }
}
