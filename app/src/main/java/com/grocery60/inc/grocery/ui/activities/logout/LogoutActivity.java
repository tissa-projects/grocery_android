package com.grocery60.inc.grocery.ui.activities.logout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.ActivityLogoutBinding;

import com.grocery60.inc.grocery.ui.activities.login.LoginActivity;

import org.json.JSONObject;

public class LogoutActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityLogoutBinding logoutBinding;
    private LogoutViewModel logoutViewModel;
    private Context context;
    private Dialog loadingDialog;
    public static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logoutBinding = DataBindingUtil.setContentView(this, R.layout.activity_logout);
        logoutViewModel = ViewModelProviders.of(this).get(LogoutViewModel.class);
        context = LogoutActivity.this;

        init();

    }

    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Logout");
        findViewById(R.id.title_bar_back_arrow).setOnClickListener(v -> onBackPressed());
        logoutBinding.btnYes.setOnClickListener(this);
        logoutBinding.btnNo.setOnClickListener(this);
    }



    private void logout() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.logout_api);
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setCookie("");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.logout_api));
        } catch (Exception e) {
            e.printStackTrace();
        }
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        logoutViewModel.logout(helper).observe(this, (response) -> {
            try {
                loadingDialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "logout: " + jsonObject.toString());
                    if (jsonObject.getInt("response_code") == 200) {
                        UserSession.logoutUser(context,getResources().getString(R.string.logout));
                        SharePreferenceUtil.clear(context);
                    } else
                        Utils.getToast(context,getResources().getString(R.string.no_response));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                if (VU.isConnectingToInternet(context))
                    logout();
                break;
            case R.id.btn_no:
                finish();
                break;
        }
    }
}
