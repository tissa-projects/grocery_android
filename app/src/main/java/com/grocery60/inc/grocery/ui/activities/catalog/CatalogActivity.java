package com.grocery60.inc.grocery.ui.activities.catalog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.ActivityCatalogBinding;
import com.grocery60.inc.grocery.ui.DashBoardActivity;
import com.grocery60.inc.grocery.ui.activities.cart.CartActivity;
import com.grocery60.inc.grocery.ui.activities.catalog_details.CatalogDetailsActivity;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

public class CatalogActivity extends AppCompatActivity {

    private ActivityCatalogBinding catalogBinding;
    private CatalogViewModel catalogViewModel;
    private Context context;
    private Dialog loadingDialog, dialogCart, dialogCartCount, dialogSearch;
    private CatalogAdapter catalogAdapter;
    private JSONObject jsonObject;
    private String catalogUrl, storeId, strProdFilter = "";
    private Set<String> catalogFilter;
    private String next = null;
    //variables for pagination
    private ProgressBar progressBar;
    private int page = 1;   // first page for product list
    private boolean isLoading = true, recyclerSearchType = false;
    private int pastVariableItems, visibleItemCount, totalItemCount, privious_total = 0;
    private int view_threshold = 6;
    private Bundle bundle = null;

    public static final String TAG = CatalogActivity.class.getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        catalogBinding = DataBindingUtil.setContentView(this, R.layout.activity_catalog);
        catalogViewModel = ViewModelProviders.of(this).get(CatalogViewModel.class);
        catalogFilter = new HashSet<>();
        catalogFilter.add("All");
        context = CatalogActivity.this;
        getIntentData();
        init();
        initRecycler();
        catalogViewModel.timer(catalogBinding);

        /*if (VU.isConnectingToInternet(context)) {
            getCategoryFilter();
        }*/
    }

    private void searchProduct() {
        String productName = catalogBinding.searchBox.getQuery().toString();

        String query = "query { productSearch(token :\"0o6jcui8mfhmp56we69kcmu5rkejtock\", productName:\"" + productName + "\",storeId:" + Integer.valueOf(storeId) + ") { productId productName productCategory productUrl price taxExempt} }";
        JSONObject jsonObject3 = new JSONObject();
        try {
            jsonObject3.put("query", query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(getResources().getString(R.string.main_url) + "graphql/");
            helper.setUrlParameter(jsonObject3.toString());
            helper.setAction(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (page == 1) {
            dialogSearch = ProgressDialog.show(context, "Please wait", "Loading...");
        }
        catalogViewModel.getData(helper).observe(this, (response) -> {
            try {
                dialogSearch.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("response_code") == 200) {
                        Log.e(TAG, "graphQlEx: " + jsonObject.toString());
                        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONObject("data").getJSONArray("productSearch");
                        if (jsonArray.length() > 0) {
                            catalogAdapter.setData(jsonArray);
                        } else {

                            catalogAdapter.setData(new JSONArray());  // biank array for  no prooduct
                            Utils.getToast(context, getResources().getString(R.string.no_product_for_search));
                        }
                    } else
                        Utils.getToast(context, getResources().getString(R.string.no_response));

                }
            } catch (Exception e) {
                page--;
                e.printStackTrace();
            } finally {
                loadingDialog.dismiss();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (VU.isConnectingToInternet(context)) {
            page = 1;
            recyclerSearchType = false;
            catalogAdapter = new CatalogAdapter(context);
            catalogBinding.setCatalogAdapter(catalogAdapter);
            catalogAdapter.cardViewClick((position, jsonObject) -> {   // recycler card view click
                startActivity(new Intent(context, CatalogDetailsActivity.class).putExtra("data", jsonObject.toString()));
            });
            catalogBinding.recyclerFragHome.setRecycledViewPool(new RecyclerView.RecycledViewPool());
            getCatalogList();
            try {
                if (bundle == null) {
                    if (SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.STORE_ID_WRT_CART).equalsIgnoreCase("")) {
                        createCart();
                    }
                }

                if (!SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID).equalsIgnoreCase("")) {
                    cartCount();  //cart item count
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getIntentData() {
        try {
            bundle = getIntent().getExtras();
            if (bundle != null) {
                String data = bundle.getString("jsonData");
                jsonObject = new JSONObject(data);
                Log.e(TAG, "getIntentData: " + jsonObject);
                catalogBinding.txtStoreName.setText(jsonObject.getString("name"));
                storeId = jsonObject.has("storeId") ? jsonObject.getString("storeId") : jsonObject.getString("store_id");
                Log.e(TAG, "getIntentData: storeId: " + storeId);
                if (jsonObject.has("store_url")) {
                    Utils.displayImageOriginalString(context, catalogBinding.imgHeaderStore, jsonObject.getString("store_url"));
                } else {
                    Utils.displayImageOriginalString(context, catalogBinding.imgHeaderStore, jsonObject.getString("storeUrl"));
                }

                if (VU.isConnectingToInternet(context)) {
                    getCart();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void init() {
        Log.e(TAG, "init: " + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.STORE_ID_WRT_CART));
        progressBar = catalogBinding.progressBar;
        catalogUrl = getResources().getString(R.string.main_url) + "" + getResources().getString(R.string.catalog);
        catalogBinding.setCartClick(this::onCLick);
        final CollapsingToolbarLayout collapsing_toolbar = findViewById(R.id.collapsing_toolbar);
        ((AppBarLayout) findViewById(R.id.app_bar_layout)).addOnOffsetChangedListener((AppBarLayout appBarLayout, int verticalOffset) -> {
            if (collapsing_toolbar.getHeight() + verticalOffset < 2.6 * ViewCompat.getMinimumHeight(collapsing_toolbar)) {
                catalogBinding.llStoreDetails.setVisibility(View.INVISIBLE);
            } else {
                catalogBinding.llStoreDetails.setVisibility(View.VISIBLE);
            }
        });

        catalogBinding.fab.setOnClickListener(view -> {
            catalogFilterDialog();
        });
        catalogBinding.titleBarBackArrow.setOnClickListener(v -> onBackPressed());


        catalogBinding.searchBox.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do your search
                page = 1;
                catalogAdapter.clearProductLiist();
                recyclerSearchType = true;
                searchProduct();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                  /*  recyclerSearchType = false;
                    page = 1;
                    catalogAdapter.clearProductLiist();
                    getCatalogList();*/
                }
                return false;
            }
        });
    }


    private void initRecycler() {

        catalogBinding.refresh.setOnRefreshListener(this::onRefresh);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        catalogBinding.recyclerFragHome.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView

        catalogBinding.recyclerFragHome.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = gridLayoutManager.getChildCount();
                totalItemCount = gridLayoutManager.getItemCount();
                pastVariableItems = (gridLayoutManager).findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > privious_total) {
                            isLoading = false;
                            privious_total = totalItemCount;
                        }
                    }

                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVariableItems + view_threshold)) {
                        Log.e(TAG, "onScrolled: next " + next);
                        if (next == null) {
                            //     Utils.getToast(context, getResources().getString(R.string.no_more_data_available));
                        } else {
                            if (VU.isConnectingToInternet(context))
                                if (!recyclerSearchType) {
                                    page++;
                                    progressBar.setVisibility(View.VISIBLE);
                                    getCatalogList();
                                }
                        }
                        isLoading = true;
                    }
                }


            }
        });
    }

    // swipe refresh
    // @Override
    public void onRefresh() {
        strProdFilter = "";
        recyclerSearchType = false;
        catalogAdapter = new CatalogAdapter(context);
        catalogBinding.setCatalogAdapter(catalogAdapter);
        catalogAdapter.cardViewClick((position, jsonObject) -> {   // recycler card view click
            startActivity(new Intent(context, CatalogDetailsActivity.class).putExtra("data", jsonObject.toString()));
        });

        catalogBinding.searchBox.setQuery("", false);
        catalogBinding.searchBox.clearFocus();
        if (VU.isConnectingToInternet(context)) {
            page = 1;
            getCatalogList();
           /* try {
                if (SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.STORE_ID_WRT_CART).equalsIgnoreCase("")) {
                    createCart();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/
        }
        catalogBinding.refresh.setRefreshing(false);
    }

    private void getCatalogList() {
        Log.e(TAG, "getCatalogList: page: " + page);
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(getResources().getString(R.string.main_url) + "" + getResources().getString(R.string.catalog) + storeId + "&status=ACTIVE&product_category=" + strProdFilter + "&page=" + page);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.catalog));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (page == 1) {
            loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        }
        catalogViewModel.getData(helper).observe(this, (response) -> {
            try {
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("response_code") == 200) {
                        next = jsonObject.getJSONObject("data").getString("next");

                        Log.e(TAG, "getCatalogList: next " + next);
                        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONArray("results");
                        if (jsonArray.length() == 0) {
                            Utils.getToast(context, getResources().getString(R.string.no_catagories));
                        }
                     //   catalogBinding.recyclerFragHome.post(() -> {
                            catalogAdapter.setData(jsonArray);
                            //  catalogAdapter.notifyItemInserted(jsonArray.length() - 1);
                    //    });
                        for (int i = 0; i < jsonArray.length(); i++) {
                            catalogFilter.add(jsonArray.getJSONObject(i).getString("product_category"));
                        }

                    } else if (jsonObject.getInt("response_code") == 404) {
                        next = null;
                        // Utils.getToast(context, getResources().getString(R.string.no_more_data_available));
                    } else {
                        CustomDialogs.dialogShowMsg(context, jsonObject.getJSONObject("data").getString("msg"));
                    }

                }
            } catch (Exception e) {
                page--;
                e.printStackTrace();
            } finally {
                loadingDialog.dismiss();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
            }
        });
    }

    private void getCategoryFilter() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + "" + getResources().getString(R.string.category_filter));
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogSearch = ProgressDialog.show(context, "Please wait", "Loading...");
        catalogViewModel.getcategoryFilter(helper).observe(this, (response) -> {
            dialogSearch.dismiss();
            try {
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getCategoryFilter: " + jsonObject.toString());
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            catalogFilter.add(jsonArray.getJSONObject(i).getString("category"));
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void createCart() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        JSONObject reqObj = new JSONObject();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.create_cart_api);
        try {
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("store", storeId);
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.create_cart_api));
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogCart = ProgressDialog.show(context, "Please wait", "Loading...");
        catalogViewModel.createCart(helper).observe(this, (response) -> {
            try {
                dialogCart.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "createCart : " + jsonObject);
                    if (jsonObject.getInt("response_code") == 201) {
                        JSONObject resultObj = jsonObject.getJSONObject("data");
                        if (resultObj != null) {
                            catalogBinding.cartcount.setText("0");
                            Log.e(TAG, "createCart:cart id " + resultObj.getString("id"));
                            Log.e(TAG, "createCart:cart id " + resultObj);
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, resultObj.getString("id"));
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.STORE_ID_WRT_CART, resultObj.getString("store"));
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    // get cart
    private void getCart() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_cart_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_cart_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (page == 1) {
            dialogCart = ProgressDialog.show(context, "Please wait", "Loading...");
        }
        catalogViewModel.getCart(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogCart.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getCart: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");
                        if (resultArray.length() > 0) {
                            // cart_id
                            Log.e(TAG, "getCart:cart id  " + resultArray.getJSONObject(0));
                            Log.e(TAG, "getCart: cart id " + resultArray.getJSONObject(0).getString("id"));
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, resultArray.getJSONObject(0).getString("id"));
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.STORE_ID_WRT_CART, resultArray.getJSONObject(0).getString("store"));
                            if (!SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.STORE_ID_WRT_CART).equalsIgnoreCase(storeId)
                                    && !SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.STORE_ID_WRT_CART).equalsIgnoreCase("")) {
                                Log.e(TAG, "init: CART_COUNT : " + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_COUNT));
                                deleteCart();
                            }
                        } else {
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, "");
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.STORE_ID_WRT_CART, "");
                            createCart();
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    //cart item count
    private void cartCount() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_cart_item) + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID) + "&status=ACTIVE");
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_cart_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogCartCount = ProgressDialog.show(context, "Please wait", "Loading...");
        catalogViewModel.getCartCount(helper).observe(this, (response) -> {
            try {
                dialogCartCount.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "showCartList: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        if (jsonObject.getJSONObject("data").length() > 0) {
                            JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, String.valueOf(resultArray.length()));
                            catalogBinding.cartcount.setText(SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_COUNT));
                        } else {
                            catalogBinding.cartcount.setText("0");
                        }

                    } else
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

    private void deleteCart() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.delete_cart_api) + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID) + "/";
        try {
            helper.setContentType("application/json");
            helper.setMethodType(context.getResources().getString(R.string.DELETE));
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.delete_cart_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogCart = ProgressDialog.show(context, "Please wait", "Loading...");
        catalogViewModel.deleteCart(helper).observe(this, (response) -> {
            try {
                dialogCart.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getCart: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (jsonObject.getInt("response_code") == 204) {
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.STORE_ID_WRT_CART, "");
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, "");
                        createCart();
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    //  @Override  //cart_app_icon click
    public void onCLick() {
        startActivity(new Intent(context, CartActivity.class));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void catalogFilterDialog() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Product Category");
        // add a list
        builder.setItems(Utils.GetStringArray(catalogFilter), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                page = 1;
                recyclerSearchType = false;
                catalogAdapter.clearProductLiist();
                catalogBinding.searchBox.setQuery("", false);
                if (Utils.GetStringArray(catalogFilter)[position].equals("All")) {
                    strProdFilter = "";
                } else
                    strProdFilter = Utils.GetStringArray(catalogFilter)[position];
                getCatalogList();
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(context, DashBoardActivity.class));
        finish();
    }
}
