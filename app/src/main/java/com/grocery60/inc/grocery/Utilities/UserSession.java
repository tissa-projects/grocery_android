package com.grocery60.inc.grocery.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.grocery60.inc.grocery.ui.DashBoardActivity;
import com.grocery60.inc.grocery.ui.activities.login.LoginActivity;

import java.util.HashMap;

public class UserSession {
    // Shared Preferences reference
    private static SharedPreferences pref;
    // Editor reference for Shared preferences
    private static SharedPreferences.Editor editor;
    // Shared preferences mode
    private static int PRIVATE_MODE = 0;
    // Shared preferences file name
    public static final String PREFER_NAME = "Reg";
    // All Shared Preferences Keys
    public static final String IS_USER_LOGIN = "IsUserLoggedIn";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_F_NAME = "f_name";
    public static final String KEY_L_NAME = "l_name";
    public static final String KEY_SALUTATION = "salutation";
    public static final String KEY_M_NO = "m_no";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_EMAIL = "email";

    // Constructor
    public UserSession(Context context) {

    }

    //Create login session
    public static void createUserLoginSession(Context context, String fName, String lName, String salutation, String mNo, String email, String userName) {
        pref = context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
        // Storing login value as TRUE
        editor.putBoolean(IS_USER_LOGIN, true);
        editor.putString(KEY_F_NAME, fName);
        editor.putString(KEY_L_NAME, lName);
        editor.putString(KEY_SALUTATION, salutation);
        editor.putString(KEY_M_NO, mNo);
        editor.putString(KEY_USERNAME, userName);
        editor.putString(KEY_EMAIL, email);

        // commit changes
        editor.commit();
    }


    //set user id
    public static void setuserId(Context context, String userId) {
        pref = context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
        // Storing user id in preferences
        editor.putString(KEY_USER_ID, userId);

        // commit changes
        editor.commit();
    }

    /**
     * Check login method will check user login status
     * If false it will redirect user to login page
     * Else do anything
     */
    public boolean checkLogin(Context context) {
        pref = context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
        // Check login status
        if (!this.isUserLoggedIn(context)) {

            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(context, LoginActivity.class);

            // Closing all the Activities from stack
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            context.startActivity(i);

            return true;
        }
        return false;
    }


    /**
     * Get stored session data
     */
    public static HashMap<String, String> getUserDetails(Context context) {
        pref = context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();

        //Use hashmap to store user credentials
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_USER_ID, pref.getString(KEY_USER_ID, ""));
        user.put(KEY_F_NAME, pref.getString(KEY_F_NAME, ""));
        user.put(KEY_L_NAME, pref.getString(KEY_L_NAME, ""));
        user.put(KEY_SALUTATION, pref.getString(KEY_SALUTATION, ""));
        user.put(KEY_M_NO, pref.getString(KEY_M_NO, ""));
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, ""));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, ""));

        // return user
        return user;
    }

    public static void clearSession(Context context) {
        pref = context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();

        Intent i = new Intent(context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // Staring Login Activity
        context.startActivity(i);
    }

    /**
     * Clear session details
     */
    public static void logoutUser(Context context, String msg) {
        pref = context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to MainActivity
        Intent i = new Intent(context, DashBoardActivity.class);

        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring DashBoardActivity Activity
        context.startActivity(i);
        ((Activity)context).finish();
        Utils.getToast(context,msg);
    }

    //set user login true
    public static void setIsuserLogin(Context context) {
        pref = context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
        // Storing login value as TRUE
        editor.putBoolean(IS_USER_LOGIN, true);
        // commit changes
        editor.commit();
    }

    // Check for login
    public static boolean isUserLoggedIn(Context context) {
        pref = context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
        return pref.getBoolean(IS_USER_LOGIN, false);
    }
}