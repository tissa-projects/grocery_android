package com.grocery60.inc.grocery.ui.activities.registration;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.MyLocation;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.ActivityRegistrationBinding;
import com.grocery60.inc.grocery.interfaces.clickinterfaces.SetOnClickListener;
import com.grocery60.inc.grocery.ui.activities.login.LoginActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class RegistrationActivity extends AppCompatActivity implements SetOnClickListener, View.OnClickListener {
    private ActivityRegistrationBinding registrationBinding;
    private RegistrationViewModel registrationViewModel;
    private Context context;
    private Dialog dialog;
    private String CountryID;
    private ArrayList<String> salutationArray = null;
    public static final String TAG = RegistrationActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registrationBinding = DataBindingUtil.setContentView(this, R.layout.activity_registration);
        registrationViewModel = ViewModelProviders.of(this).get(RegistrationViewModel.class);
        context = RegistrationActivity.this;
        init();
    }

    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Sign Up");
        findViewById(R.id.title_bar_back_arrow).setOnClickListener(v -> onBackPressed());
        registrationBinding.setSignUpBtnClick(this::onCLick);
        registrationBinding.txtAlreadyRegistered.setOnClickListener(this);
        registrationBinding.edtSalutation.setOnClickListener(this);
        salutationArray = new ArrayList<>();
        salutationArray.add("Mr.");
        salutationArray.add("Mrs.");
        salutationArray.add("Miss.");
        salutationArray.add("Ms.");

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        CountryID= manager.getSimCountryIso().toUpperCase();
        if (CountryID.equalsIgnoreCase("IN")){
            CountryID = "+91";
        }else {CountryID = "+1";}

        registrationBinding.edtCode.setText(CountryID);

    }

    //salutation dialog
    private void setSalutation() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Salutation");
        // add a list
        builder.setItems(Utils.GetStringArray(salutationArray), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                registrationBinding.edtSalutation.setText(salutationArray.get(position));
                dialog.dismiss();
            }
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override //signUp
    public void onCLick() {
        if (registrationViewModel.validate(context, registrationBinding)) {
            if (VU.isConnectingToInternet(context))
                checkForLogin();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_already_registered:
                startActivity(new Intent(context, LoginActivity.class));
                finish();
                break;
            case R.id.edt_salutation:
                setSalutation();
                break;
        }
    }

    private void checkForLogin() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.login);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("username", getResources().getString(R.string.app_username));
            stringMap.put("password", getResources().getString(R.string.app_password));
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setCookie("");
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.login));
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        registrationViewModel.login(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "checkForLogin: " + jsonObject.toString());
                    if (jsonObject.getInt("response_code") == 200) {
                        register(jsonObject.getJSONObject("data").getString("token"));
                    } else {
                      //  registrationViewModel.login(helper).removeObservers(this);
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void register(String authToken) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            JSONObject jsonReqst = new JSONObject();
            jsonReqst.put("username", registrationBinding.edtUsername.getText().toString());
            jsonReqst.put("email", registrationBinding.edtEmail.getText().toString());
            jsonReqst.put("first_name", registrationBinding.edtFName.getText().toString());
            jsonReqst.put("last_name", registrationBinding.edtLName.getText().toString());
            jsonReqst.put("password", registrationBinding.edtPassword.getText().toString());
            jsonReqst.put("verified", "N");
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setAuthorization(authToken);
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.register));
            helper.setAction(getResources().getString(R.string.register));
            helper.setUrlParameter(jsonReqst.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        registrationViewModel.setRegistrationData(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                   // registrationViewModel.setRegistrationData(helper).removeObservers(this);
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "register: " + responseObj.toString());
                    switch (responseObj.getInt("response_code")) {
                        case 201:
                            Utils.getToast(context, getResources().getString(R.string.registration_done));
                            UserSession.setuserId(context, responseObj.getJSONObject("data").getString("id"));  //customer id or user_id
                           // getCustomer(authToken);
                            createCustomer(authToken);
                            break;
                        case 400:
                            Utils.getToast(context, getResources().getString(R.string.user_already_present));
                            break;
                        default:
                            Utils.getToast(context, getResources().getString(R.string.no_response));
                            break;
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void getCustomer(String authToekn) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_customer_api) + UserSession.getUserDetails(context).get("user_id");
        try {

            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setAuthorization(authToekn);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        registrationViewModel.getCustomer(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getCustomer: " + jsonObject.toString());
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");
                        if ((resultArray.length() > 0)) {
                            startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        } else {
                            createCustomer(authToekn);
                        }
                    } else
                        Utils.getToast(context,getResources().getString(R.string.no_response));


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void createCustomer(String authToken) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.create_customer_api);
        try {
            JSONObject reqObj = new JSONObject();
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("last_access", Utils.CurrentTimeStamp());
            reqObj.put("extra", "extra");
            reqObj.put("salutation", registrationBinding.edtSalutation.getText().toString());
            reqObj.put("phone_number", CountryID+registrationBinding.edtMNo.getText().toString());
            reqObj.put("first_name", registrationBinding.edtFName.getText().toString());
            reqObj.put("last_name", registrationBinding.edtLName.getText().toString());
            reqObj.put("email", registrationBinding.edtEmail.getText().toString());
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setAuthorization(authToken);
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        registrationViewModel.createCustomer(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "createCustomer: " + jsonObject.toString());
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONObject resultObj = jsonObject.getJSONObject("data");
                        if (jsonObject != null) {
                            startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        }
                    } else
                        Utils.getToast(context,getResources().getString(R.string.no_response));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
