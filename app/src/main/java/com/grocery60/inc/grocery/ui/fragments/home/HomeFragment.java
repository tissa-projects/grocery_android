package com.grocery60.inc.grocery.ui.fragments.home;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.FragmentHomeBinding;
import com.grocery60.inc.grocery.ui.activities.catalog.CatalogActivity;
import com.grocery60.inc.grocery.ui.activities.login.LoginActivity;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.SliderLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class HomeFragment extends Fragment /*implements SwipeRefreshLayout.OnRefreshListener, LocationListener*/ {

    private FragmentHomeBinding homeBinding;
    private HomeFragmentViewModel homeFragmentViewModel;
    private HomeFragmentAdapter homeFragmentAdapter;
    private View view;
    private SliderLayout sliderLayout;
    private Context context;
    private JSONObject jsonAdapterObj;
    private Dialog dialogGetStores, dialogCart, dialogSearch, dialogLogin;
    private String url, strZipCode = "", strCountry = "";
    private GridLayoutManager gridLayoutManager = null;
    //variables for pagination
    private ProgressBar progressBar;
    private int page = 6;   // first page for product list
    private int skip = 0;
    private boolean storedataAvail = true;
    private boolean isLoading = true, recyclerSearchType = false;
    private int pastVariableItems, visibleItemCount, totalItemCount, privious_total = 0;
    private int view_threshold = 6;
    boolean doubleBackToExitPressedOnce = false;
    public static final String TAG = HomeFragment.class.getSimpleName();


    @RequiresApi(api = Build.VERSION_CODES.N)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        homeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        view = homeBinding.getRoot();
        Log.e(TAG, "onCreateView: ");
        context = getActivity();
        setSliderViews();
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        init();
        getMyLocation();
    }

    public void getMyLocation() {
        try {
            Log.e(TAG, "gotLocation: ");
            strCountry = SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.COUNTRY);
            strZipCode = SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.ZIPCODE);
            Log.e(TAG, "getMyLocation: "+strCountry );
            Log.e(TAG, "getMyLocation: strZipCode: " + strZipCode);
            if (strCountry.equalsIgnoreCase("India")) {
                strCountry = "IN";
            } else {
                strCountry = "US";
            }
            Log.e(TAG, "getMyLocation: "+strCountry );
            if (VU.isConnectingToInternet(context)) {
                if (SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.ZIPCODE) == "") {
                    dialogZipCode();
                } else {
                    checkForAdminLogin();
                    strZipCode = SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.ZIPCODE);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        homeBinding.searchBox.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do your search
                page = 6;
                skip = 0;
                homeFragmentAdapter.clearProductLiist();
                searchStore();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    //     checkForAdminLogin();
                }
                return false;
            }
        });


        homeBinding.recyclerFragHome.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = gridLayoutManager.getChildCount();
                totalItemCount = gridLayoutManager.getItemCount();
                pastVariableItems = (gridLayoutManager).findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > privious_total) {
                            isLoading = false;
                            privious_total = totalItemCount;
                        }
                    }

                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVariableItems + view_threshold)) {
                        if (storedataAvail) {
                            progressBar.setVisibility(View.VISIBLE);
                            if (VU.isConnectingToInternet(context))
                                page = page + 6;
                            skip = skip + 6;
                            checkForAdminLogin();
                            isLoading = true;
                        }
                    }
                }
            }
        });
    }

    private void init() {
        homeFragmentViewModel = ViewModelProviders.of(this).get(HomeFragmentViewModel.class);
        progressBar = homeBinding.progressBar;
        //  homeBinding.refresh.setOnRefreshListener(this);
        gridLayoutManager = new GridLayoutManager(context, 2);
        homeBinding.recyclerFragHome.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        homeFragmentAdapter = new HomeFragmentAdapter(getActivity());

        homeFragmentAdapter.cardViewClick((position, jsonObject) -> {   // recycler card view click
            try {
                jsonAdapterObj = jsonObject;
                String storeId = jsonObject.has("storeId") ? jsonObject.getString("storeId") : jsonObject.getString("store_id");
                String currency = jsonObject.getString("currency").toLowerCase().equals("usd") ? "$" : "₹";
                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CURRENCY_TYPE, currency);
                if (UserSession.isUserLoggedIn(context)) {
                    Log.e(TAG, "init: " + jsonObject.toString());
                    if (!SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.STORE_ID_WRT_CART).equalsIgnoreCase(storeId)
                            && !SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.STORE_ID_WRT_CART).equalsIgnoreCase("")) {
                        Log.e(TAG, "init: CART_COUNT : " + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_COUNT));
                        if (SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_COUNT).equalsIgnoreCase("0")) {
                            deleteCart();
                        } else
                            dialogDeleteCart();
                    } else {
                        getCatalogActivity();
                    }
                } else {
                    Dialog dialog = CustomDialogs.dialogShowMsg(context, getResources().getString(R.string.login_first));
                    dialog.setCancelable(true);
                    dialog.findViewById(R.id.btn_ok).setOnClickListener(v -> {
                        dialog.dismiss();
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.STORE_ID_WRT_CART, storeId);
                        startActivity(new Intent(context, LoginActivity.class).putExtra("jsonData", jsonAdapterObj.toString()));
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        homeBinding.setHomeFragmentAdapter(homeFragmentAdapter);

        homeBinding.fab.setOnClickListener(v -> {
            dialogZipCode();
        });

        homeBinding.fab1.setOnClickListener(v -> {
            setCountry();
        });

    }


    private void getCatalogActivity() {
        Intent intent = new Intent(context, CatalogActivity.class);
        intent.putExtra("jsonData", jsonAdapterObj.toString());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    private void setSliderViews() {
        sliderLayout = homeBinding.imageSlider;
        ArrayList<Integer> imagesAddress = new ArrayList<>();
        imagesAddress.add(R.drawable.slider1);
        imagesAddress.add(R.drawable.slider2);
        imagesAddress.add(R.drawable.slider3);
        for (int i = 0; i < imagesAddress.size(); i++) {

            DefaultSliderView sliderView = new DefaultSliderView(context);

            switch (i) {
                case 0:
                case 2:
                case 1:
                    sliderView.setImageDrawable(imagesAddress.get(i));
                    break;
            }


            sliderView.setImageScaleType(ImageView.ScaleType.FIT_XY);
            sliderLayout.addSliderView(sliderView);
        }
    }

    // swipe refresh
   /* @Override
    public void onRefresh() {
        if (VU.isConnectingToInternet(context)) {
            page = 6;
            skip = 0;
          //  strZipCode = "";
          //  SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.ZIPCODE, "");
            homeFragmentAdapter.clearProductLiist();
            homeBinding.searchBox.setQuery("", false);
            homeBinding.searchBox.clearFocus();
            checkForAdminLogin();
        }
        homeBinding.refresh.setRefreshing(false);

    }*/

    private void checkForAdminLogin() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.login);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("username", getResources().getString(R.string.app_username));
            stringMap.put("password", getResources().getString(R.string.app_password));
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setCookie("");
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.login));
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (page == 6) {
            dialogLogin = ProgressDialog.show(context, "Please wait", "Loading...");
        }

        homeFragmentViewModel.login(helper).observe(this, (response) -> {
            try {
                dialogLogin.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "checkForLogin: " + jsonObject.toString());
                    if (jsonObject.getInt("response_code") == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (jsonObject.getInt("response_code") == 200) {
                        getStoreList(jsonObject.getJSONObject("data").getString("token"));
                        String userId = UserSession.getUserDetails(context).get("user_id");
                        Log.e(TAG, "checkForLogin: userId: " + userId);
                        if (((userId.equalsIgnoreCase("")))) {
                           //============================
                        } else {
                            getCart(jsonObject.getJSONObject("data").getString("token"));
                        }
                    } else
                        Utils.getToast(context, getResources().getString(R.string.no_response));


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void getStoreList(String authToken) {
        Log.e(TAG, "getStoreList: " + authToken);
        Log.e(TAG, "getStoreList: " + page);
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String query = "query { storeZipSearch(token :\"0o6jcui8mfhmp56we69kcmu5rkejtock\", first:"
                + page + ", nearbyZip:\"" + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.ZIPCODE) +
                "\", skip:" + skip + ", country:\"" + strCountry + "\") " +
                "{storeId city name zip state storeUrl address currency country} }";
        JSONObject jsonObject3 = new JSONObject();
        try {
            jsonObject3.put("query", query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(getResources().getString(R.string.main_url) + "graphql/");
            helper.setUrlParameter(jsonObject3.toString());
            helper.setAction(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (page == 6) {
            dialogGetStores = ProgressDialog.show(context, "Please wait", "Loading...");
        }
        homeFragmentViewModel.getData(helper).observe(getActivity(), (response) -> {
            try {
                dialogGetStores.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getStoreList: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (jsonObject.getInt("response_code") == 200) {
                        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONObject("data").getJSONArray("storeZipSearch");
                        if (jsonArray.length() > 0) {
                            homeFragmentAdapter.setData(jsonArray);
                            storedataAvail = true;
                        } else {
                            homeFragmentAdapter.setData(new JSONArray());  //blank array
                            storedataAvail = false;
                            if (page == 6) {
                                Utils.getToast(context, getResources().getString(R.string.no_store_available));
                            }
                        }
                    } else if (jsonObject.getInt("response_code") == 404) {
                        //  Utils.getToast(context, getResources().getString(R.string.no_more_data_available));
                    } else {
                        homeFragmentAdapter.setData(new JSONArray());   //blank array
                        CustomDialogs.dialogShowMsg(context, jsonObject.getJSONObject("data").getString("msg"));
                    }
                }
            } catch (Exception e) {
                page = 6;
                skip = 0;
                e.printStackTrace();
            } finally {
                dialogGetStores.dismiss();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
            }
        });
    }


    private void getCart(String authToken) {
        Log.e(TAG, "getCart: " + authToken);
        RestAPIClientHelper helper = new RestAPIClientHelper();
        url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_cart_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setAuthorization(authToken);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_cart_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (page == 6) {
            dialogCart = ProgressDialog.show(context, "Please wait", "Loading...");
        }
        homeFragmentViewModel.getCart(helper).observe(getActivity(), (response) -> {
            try {
                dialogCart.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getCart: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");
                        if (resultArray.length() > 0) {
                            // cart_id
                            Log.e(TAG, "getCart:cart id  " + resultArray.getJSONObject(0));
                            Log.e(TAG, "getCart: cart id " + resultArray.getJSONObject(0).getString("id"));
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, resultArray.getJSONObject(0).getString("id"));
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.STORE_ID_WRT_CART, resultArray.getJSONObject(0).getString("store"));

                        } else {
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, "");
                            SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.STORE_ID_WRT_CART, "");
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void deleteCart() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.delete_cart_api) + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID) + "/";
        try {
            helper.setContentType("application/json");
            helper.setMethodType(context.getResources().getString(R.string.DELETE));
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.delete_cart_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogCart = ProgressDialog.show(context, "Please wait", "Loading...");
        homeFragmentViewModel.deleteCart(helper).observe(this, (response) -> {
            try {
                dialogCart.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getCart: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 401) {
                        CustomDialogs.dialogSessionExpire(context);
                    } else if (jsonObject.getInt("response_code") == 204) {
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.STORE_ID_WRT_CART, "");
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_ID, "");
                        getCatalogActivity();
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    public void dialogZipCode() {
        try {
            Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.dialog_zip_code);
            //SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.ZIPCODE, strZipCode);
            final EditText edtZip = (EditText) dialog.findViewById(R.id.edt_zip_code);
            final TextView txtCountry = dialog.findViewById(R.id.txt_country);
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                txtCountry.setVisibility(View.VISIBLE);
                txtCountry.setOnClickListener(v -> {
                    setCountry();
                    txtCountry.setText(SharePreferenceUtil.getSPstringValue(context,SharePreferenceUtil.COUNTRY));
                        }
                );
            } else {
                txtCountry.setVisibility(View.GONE);
            }
            edtZip.setText(strZipCode);
            Log.e(TAG, "dialogZipCode: " + edtZip.getText().toString());
            dialog.setCancelable(false);
            if (SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.ZIPCODE) == "") {
                ((Button) dialog.findViewById(R.id.btn_cancel)).setVisibility(View.GONE);
                ((View) dialog.findViewById(R.id.view)).setVisibility(View.GONE);
            } else {
                ((Button) dialog.findViewById(R.id.btn_cancel)).setVisibility(View.VISIBLE);
                ((View) dialog.findViewById(R.id.view)).setVisibility(View.VISIBLE);
            }
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            ((Button) dialog.findViewById(R.id.bt_continue)).setOnClickListener((view) -> {
                if (VU.isConnectingToInternet(context)) {
                    Log.e(TAG, "dialogZipCode: " + edtZip.getText().toString());
                    if (edtZip.getText().toString().equalsIgnoreCase("")) {
                        Utils.getToast(context, getResources().getString(R.string.pass_zip_code));
                    } else {
                        strZipCode = edtZip.getText().toString();
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.ZIPCODE, strZipCode);
                        Log.e(TAG, "dialogZipCode: strZipCode" + strZipCode);
                        if (VU.isConnectingToInternet(context)) {
                            page = 6;
                            skip = 0;
                            homeFragmentAdapter.clearProductLiist();
                            checkForAdminLogin();
                        }
                        dialog.dismiss();
                    }
                }
            });

            ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(v -> dialog.dismiss());
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            dialog.getWindow().setAttributes(lp);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void dialogDeleteCart() {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_delete);

        final TextView txtView = (TextView) dialog.findViewById(R.id.txt);
        txtView.setText(getResources().getString(R.string.dialog_delete_cart));
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener((view) -> {
            if (VU.isConnectingToInternet(context))
                deleteCart();
            dialog.dismiss();
        });

        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener((view) -> {
            dialog.dismiss();
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    private void searchStore() {
        //  String token = "0o6jcui8mfhmp56we69kcmu5rkejtock";

        String storeName = homeBinding.searchBox.getQuery().toString();

        String query = "query { storeSearch(token :\"0o6jcui8mfhmp56we69kcmu5rkejtock\", country:\"" + strCountry + "\", storeName:\"" + storeName + "\") {storeId city name zip state storeUrl address currency} }";
        // String query = "query { storeSearch(token :\"0o6jcui8mfhmp56we69kcmu5rkejtock\", storeName:\"" + storeName + "\") {storeId city name zip state storeUrl address currency} }";
        JSONObject jsonObject3 = new JSONObject();
        try {
            jsonObject3.put("query", query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(getResources().getString(R.string.main_url) + "graphql/");
            helper.setUrlParameter(jsonObject3.toString());
            helper.setAction(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (page == 6) {
            dialogSearch = ProgressDialog.show(context, "Please wait", "Loading...");
        }
        homeFragmentViewModel.searchStore(helper).observe(this, (response) -> {
            try {
                dialogSearch.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("response_code") == 200) {
                        Log.e(TAG, "searchStore: " + jsonObject.toString());
                        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONObject("data").getJSONArray("storeSearch");
                        if (jsonArray.length() > 0) {
                            homeFragmentAdapter.setData(jsonArray);
                        } else {

                            homeFragmentAdapter.setData(new JSONArray());  // biank array for  no prooduct
                            Utils.getToast(context, getResources().getString(R.string.no_store_for_search));
                        }
                    } else
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

   /* public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (doubleBackToExitPressedOnce) {
                        return;
                    }

                    this.doubleBackToExitPressedOnce = true;
                    Utils.getToast(context, "Please click BACK again to exit");

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);
            }
                    return true;
            }

        return super.onKeyDown(keyCode, event);
    }*/


    //Country dialog
    public void setCountry() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Country");
        // add a list
        builder.setItems(context.getResources().getStringArray(R.array.country_array), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                strCountry = getResources().getStringArray(R.array.country_array)[position];
                if (strCountry.equalsIgnoreCase("IN")) {
                    SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.COUNTRY, "India");
                    strCountry = "IN";
                } else {
                    strCountry = "US";
                    SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.COUNTRY, "United States of America");
                }
                dialog.dismiss();
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

}