package com.grocery60.inc.grocery.ui.activities.Customer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.ActivityCustomerBinding;
import com.grocery60.inc.grocery.ui.activities.Address.ShippingAddressActivity;

import org.json.JSONArray;
import org.json.JSONObject;

public class CustomerActivity extends AppCompatActivity {

    private ActivityCustomerBinding customerBinding;
    private CustomerViewModel viewModel;
    private Context context;
    private Dialog dialog;
    private JSONObject customerObj;
    private String lastAccess = null,cartDataArray = null;
    public static final String TAG = CustomerActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customerBinding = DataBindingUtil.setContentView(this, R.layout.activity_customer);
        viewModel = ViewModelProviders.of(this).get(CustomerViewModel.class);
        context = CustomerActivity.this;
        getIntentData();
        init();
    }

    private void getIntentData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                cartDataArray = bundle.getString("cartDataArray");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Customer Details");
        findViewById(R.id.title_bar_back_arrow).setOnClickListener(v -> onBackPressed());
        customerBinding.btnNext.setOnClickListener(v -> {
            startActivity(new Intent(context, ShippingAddressActivity.class).putExtra("customerObj",customerObj.toString()).putExtra("cartDataArray",cartDataArray));
        });

        if (VU.isConnectingToInternet(context))
            getCustomer();
    }

    private void getCustomer() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_customer_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_customer_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getCustomer(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject addressObject = new JSONObject(response);
                    Log.e(TAG, "getCustomer: " + addressObject);
                    if (addressObject.getInt("response_code") == 200) {
                        setCustomerData(addressObject);
                    } else {
                        Utils.getToast(context,getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void setCustomerData(JSONObject jsonObject) {
        try {
            JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");
            Log.e(TAG, "getCustomer: " + resultArray.toString());
            JSONObject resultObj = resultArray.getJSONObject(0);
            lastAccess = resultObj.getString("last_access");
            customerObj = resultObj.getJSONObject("customer");
            customerBinding.edtSalutation.setText(resultObj.getString("salutation"));
            customerBinding.edtMNo.setText(resultObj.getString("phone_number"));
            customerBinding.edtFName.setText(customerObj.getString("first_name"));
            customerBinding.edtLName.setText(customerObj.getString("last_name"));
            customerBinding.edtEmail.setText(customerObj.getString("email"));
            customerObj.put("phone_number",resultObj.getString("phone_number"));
        }catch (Exception e){e.printStackTrace();}
    }


   /* private void UpdateCustomer() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.update_customer_api) + UserSession.getUserDetails(context).get("user_id");
        try {

            JSONObject reqObj = new JSONObject();
            reqObj.put("email", SharePreferenceUtil.getSPstringValue(context,SharePreferenceUtil.CART_ID));
            reqObj.put("first_name",customerBinding.edtFName.getText().toString());
            reqObj.put("last_name",customerBinding.edtLName.getText().toString());
            reqObj.put("phone_number",customerBinding.edtMNo.getText().toString());
            reqObj.put("salutation",customerBinding.edtSalutation.getText().toString());
            reqObj.put("is_active",true);
            reqObj.put("username",customerObj.getString("username"));
            reqObj.put("last_access",lastAccess);

            helper.setContentType("application/json");
            helper.setMethodType("PUT");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getCustomer(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getCustomer: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        setCustomerData(jsonObject);
                    } else {
                        Utils.getToast(context,getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }*/
}
