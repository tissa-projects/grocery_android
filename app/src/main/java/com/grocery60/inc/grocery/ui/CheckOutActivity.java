package com.grocery60.inc.grocery.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.databinding.ActivityCheckOutBinding;

public class CheckOutActivity extends AppCompatActivity {

    ActivityCheckOutBinding checkOutBinding;
    Context context;
    public static FragmentManager fm;
    private static  final String TAG  =CheckOutActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkOutBinding = DataBindingUtil.setContentView(this,R.layout.activity_check_out);
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Checkout");
    }
}
