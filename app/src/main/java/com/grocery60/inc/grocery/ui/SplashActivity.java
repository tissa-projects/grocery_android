package com.grocery60.inc.grocery.ui;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.Utilities.MyLocation;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class SplashActivity extends AppCompatActivity {

    private Context context;
    private static int SPLASH_TIME_OUT = 2000;
    MyLocation myLocation = new MyLocation();
    private ProgressBar progressBar;
    String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};


    public static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        context = SplashActivity.this;
        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        Utils.printHashKey(context);

        checkLocationPermission();
    }


    public void splashTime() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(context, DashBoardActivity.class));
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    public MyLocation.LocationResult locationResult = new MyLocation.LocationResult() {

        @Override
        public void gotLocation(Location location) {
            try {
// TODO Auto-generated method stub
                Log.e(TAG, "gotLocation: ");
                double Longitude = location.getLongitude();
                double Latitude = location.getLatitude();
                Log.e(TAG, "gotLocation: " + Latitude + "  " + Longitude);
                String strCountry = Utils.geLocationData(context, location.getLatitude(), location.getLongitude(), "country");
                String strsName = Utils.geLocationData(context, location.getLatitude(), location.getLongitude(), "state");
                String strcName = Utils.geLocationData(context, location.getLatitude(), location.getLongitude(), "city");
                String strZipCode = Utils.geLocationData(context, location.getLatitude(), location.getLongitude(), "zipcode");
                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.COUNTRY, strCountry);
                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.ZIPCODE, strZipCode);
                Log.e(TAG, "gotLocation: zip" + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.ZIPCODE));
                Log.e(TAG, "gotLocation: county" + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.COUNTRY));
                startActivity(new Intent(context, DashBoardActivity.class));
                finish();
                progressBar.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    public void checkLocationPermission() {

        Permissions.Options options = new Permissions.Options()
                .setRationaleDialogTitle("Info")
                .setSettingsDialogTitle("Warning");

        Permissions.check(this, permissions, R.string.text_location_permission, options, new PermissionHandler() {
            @Override
            public void onGranted() {
                // do your task.
                myLocation.getLocation(context, locationResult);
            }

            @Override
            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                // permission denied, block the feature.
                checkLocationPermission();
            }
        });
    }
}
