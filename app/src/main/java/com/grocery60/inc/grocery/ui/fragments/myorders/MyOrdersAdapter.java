package com.grocery60.inc.grocery.ui.fragments.myorders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.databinding.RecyclerMyOrdersFragmentBinding;
import com.grocery60.inc.grocery.interfaces.clickinterfaces.SetOnClickListener;
import com.grocery60.inc.grocery.models.MyOrderModel;

import org.json.JSONArray;
import org.json.JSONObject;

public class MyOrdersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private static final String TAG = MyOrdersAdapter.class.getSimpleName();

    private SetOnClickListener.setCardClick setViewDetails, setOrderCancel;
    private JSONArray jsonArray;


    public MyOrdersAdapter(Context context) {
        this.context = context;
        jsonArray = new JSONArray();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerMyOrdersFragmentBinding myOrdersFragmentBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_my_orders_fragment, parent, false);

        return new MyViewHolder(myOrdersFragmentBinding);
    }

     @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            String paymentStatus = null;
            MyViewHolder myViewHolder = null;
            MyOrderModel myOrderModel = new MyOrderModel();
            if (holder instanceof MyViewHolder) {
                myViewHolder = (MyViewHolder) holder;
                JSONObject jsonObject = jsonArray.getJSONObject(position);
                paymentStatus =jsonObject.getString("status");
                String currencyType = jsonObject.getJSONObject("order").getString("currency").toLowerCase().equals("usd") ? "$" : "₹";
                SharePreferenceUtil.setSPstring(context,SharePreferenceUtil.CURRENCY_TYPE,currencyType);
                myOrderModel.setOrderNo(context.getResources().getString(R.string.order_no) + jsonObject.getJSONObject("order").getString("order_id"));
                myOrderModel.setStoreName(context.getResources().getString(R.string.store_name) + jsonObject.getJSONObject("store").getString("name"));
                myOrderModel.setOrderDate(context.getResources().getString(R.string.order_date) + Utils.ConvertIntoDateFormat("yyyy-MM-dd", "dd MMMM yyyy", jsonObject.getString("created_at")));
                myOrderModel.setNoOfItems(context.getResources().getString(R.string.no_of_items) + " 3"); //no use
                myOrderModel.setTotal(context.getResources().getString(R.string.total) + currencyType + Utils.convertToUSDFormat(jsonObject.getString("amount")));
                myOrderModel.setTxnId(context.getResources().getString(R.string.txn_id) + jsonObject.getString("transaction_id"));
                myOrderModel.setPaymentType(context.getResources().getString(R.string.pymt_type) + jsonObject.getString("payment_method"));
                myViewHolder.myOrdersFragmentBinding.setViewDetails(() -> {
                    try {
                        setViewDetails.onClick(position, jsonObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

                myViewHolder.myOrdersFragmentBinding.setOrderCancel(() -> {
                    setOrderCancel.onClick(position, jsonObject);
                });
            }

            switch (paymentStatus){
                case "CONFIRMED": paymentStatus ="Confirmed"; myViewHolder.myOrdersFragmentBinding.btnCancelOrder.setVisibility(View.VISIBLE); break;
                case "FAIL": paymentStatus ="Failed";myViewHolder.myOrdersFragmentBinding.btnCancelOrder.setVisibility(View.VISIBLE); break;
                case "READY_TO_FULFILL": paymentStatus ="Ready to fulfill"; myViewHolder.myOrdersFragmentBinding.btnCancelOrder.setVisibility(View.GONE);break;
                case "READY_TO_PICK": paymentStatus ="Ready to pick";myViewHolder.myOrdersFragmentBinding.btnCancelOrder.setVisibility(View.GONE); break;
                case "PICKED": paymentStatus ="Picked";myViewHolder.myOrdersFragmentBinding.btnCancelOrder.setVisibility(View.GONE); break;
                case "COMPLETE": paymentStatus ="Completed";myViewHolder.myOrdersFragmentBinding.btnCancelOrder.setVisibility(View.GONE); break;
                case "CANCELED": paymentStatus ="Cancelled";myViewHolder.myOrdersFragmentBinding.btnCancelOrder.setVisibility(View.GONE); break;
                case "SUCCESS": paymentStatus ="Success";myViewHolder.myOrdersFragmentBinding.btnCancelOrder.setVisibility(View.GONE); break;

                default:paymentStatus= paymentStatus;break;

            }
            myOrderModel.setStatus(context.getResources().getString(R.string.status)+paymentStatus);
            myViewHolder.bind(myOrderModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        //  return 10;
        return jsonArray.length();

    }

    public void setData(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        public RecyclerMyOrdersFragmentBinding myOrdersFragmentBinding;

        public MyViewHolder(@NonNull RecyclerMyOrdersFragmentBinding myOrdersFragmentBinding) {
            super(myOrdersFragmentBinding.getRoot());
            this.myOrdersFragmentBinding = myOrdersFragmentBinding;
        }

        public void bind(Object obj) {
            myOrdersFragmentBinding.setMyOrder((MyOrderModel) obj);
            myOrdersFragmentBinding.executePendingBindings();
        }
    }

    public void ViewDetailsClick(SetOnClickListener.setCardClick setOnClickListener) {
        this.setViewDetails = setOnClickListener;
    }

    public void cancelDeleteClick(SetOnClickListener.setCardClick setOnClickListener) {
        this.setOrderCancel = setOnClickListener;
    }


}
