package com.grocery60.inc.grocery.ui.activities.myorderdetails;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.ActivityMyOrderDetailsBinding;

import org.json.JSONArray;
import org.json.JSONObject;

public class MyOrderDetailsActivity extends AppCompatActivity {

    private ActivityMyOrderDetailsBinding myOrderDetailsBinding;
    private MyOrderDetailsAdapter adapter;
    private MyOrderDetailsViewModel myOrderDetailsViewModel;
    private Context context;
    private Dialog dialog, dialogAddress;
    private String strOrderId, strOrderDate;
    private static final String TAG = MyOrderDetailsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myOrderDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_order_details);
        myOrderDetailsViewModel = ViewModelProviders.of(this).get(MyOrderDetailsViewModel.class);
        context = MyOrderDetailsActivity.this;
        getIntentData();
        init();
        if (VU.isConnectingToInternet(context)) {
            getOrderDetails(strOrderId);
            getShippingAddress();
        }

    }

    private void getIntentData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                String data = bundle.getString("jsonData");
                JSONObject jsonData = new JSONObject(data);
                JSONObject bundleData = jsonData.getJSONObject("order");
                JSONObject shippingObj = jsonData.getJSONObject("shippingmethod");
                JSONObject storeObj = jsonData.getJSONObject("store");
                Log.e(TAG, "getIntentData: jsonObject: " + jsonData);
                //    strPaymentId = bundleData.getString("payment_id");
                //  strPaymentType = bundleData.getString("payment_method");
                strOrderId = bundleData.getString("order_id");
                // strOrderTotal= bundleData.getJSONObject("order").getString("total");
                strOrderDate = Utils.ConvertIntoDateFormat("yyyy-MM-dd", "dd MMMM yyyy", bundleData.getString("created_at"));
                String currencyType = SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CURRENCY_TYPE);
                myOrderDetailsBinding.txtOrderDate.setText(strOrderDate);
                myOrderDetailsBinding.txtSubTotal.setText(currencyType + bundleData.getString("subtotal"));
                myOrderDetailsBinding.txtOrderNo.setText(bundleData.getString("order_id"));
                myOrderDetailsBinding.txtStore.setText(storeObj.getString("name"));
                myOrderDetailsBinding.txtTip.setText(currencyType + bundleData.getString("tip"));
                myOrderDetailsBinding.txtServiceFee.setText(currencyType + bundleData.getString("service_fee"));
                myOrderDetailsBinding.txtTax.setText(currencyType + bundleData.getString("tax"));
                myOrderDetailsBinding.txtShippingFee.setText(currencyType + bundleData.getString("shipping_fee"));
                myOrderDetailsBinding.txtDiscount.setText(currencyType + bundleData.getString("discount"));
                myOrderDetailsBinding.txtOrderTotal.setText(currencyType + bundleData.getString("total"));
                myOrderDetailsBinding.txtPaymtMethod.setText(jsonData.getString("payment_method"));
                myOrderDetailsBinding.txtShippingMethod.setText(shippingObj.getString("name"));
                if (shippingObj.getString("name").equalsIgnoreCase("Store Pickup")){
                    myOrderDetailsBinding.rlAddress.setVisibility(View.GONE);
                    myOrderDetailsBinding.shippingAddress.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Order Details");
        findViewById(R.id.title_bar_back_arrow).setOnClickListener(v -> onBackPressed());
        adapter = new MyOrderDetailsAdapter(context);
        myOrderDetailsBinding.setMyOrderDetailsAdapter(adapter);

    }


    private void getOrderDetails(String strOrderId) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_order_details_api) + strOrderId;
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_order_details_api));
            helper.setOrderId(strOrderId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        myOrderDetailsViewModel.getMyOrderDetails(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getStoreList: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONArray("results");
                        if (jsonArray.length() > 0) {
                            adapter.setData(jsonArray);
                        } else {
                            Utils.getToast(context, getResources().getString(R.string.no_product_available_for_order));
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void getShippingAddress() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_shippinp_address_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_shippinp_address_api));
            helper.setOrderId(strOrderId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogAddress = ProgressDialog.show(context, "Please wait", "Loading...");
        myOrderDetailsViewModel.getBillingAddress(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialogAddress.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getShippingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");

                        if (resultArray.length() > 0) {
                            JSONObject resultObj = resultArray.getJSONObject(0);
                            myOrderDetailsBinding.txtShippingAddress.setText(resultObj.getString("house_number") + ",\n" +
                                    resultObj.getString("address") + ",\n" +
                                    resultObj.getString("city") + ",\n" +
                                    resultObj.getString("state") + ",\n" +
                                    resultObj.getString("country") + ",\n" +
                                    resultObj.getString("zip") + "\n");
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}
