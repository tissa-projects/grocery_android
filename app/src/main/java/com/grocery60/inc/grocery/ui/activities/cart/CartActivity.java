package com.grocery60.inc.grocery.ui.activities.cart;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.ui.activities.Address.ShippingAddressActivity;
import com.grocery60.inc.grocery.ui.activities.catalog.CatalogViewModel;
import com.grocery60.inc.grocery.databinding.ActivityCartBinding;
import com.grocery60.inc.grocery.interfaces.clickinterfaces.SetOnClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CartActivity extends AppCompatActivity implements SetOnClickListener {

    private ActivityCartBinding cartBinding;
    private CatalogViewModel catalogViewModel;
    private Context context;
    private Dialog loadingDialog;
    private CartAdapter cartAdapter;
    private JSONArray cartDataArray;
    private String currencyType;
    private double subtotal = 0, noTaxTotal = 0;
    public static final String TAG = CartActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cartBinding = DataBindingUtil.setContentView(this, R.layout.activity_cart);
        catalogViewModel = ViewModelProviders.of(this).get(CatalogViewModel.class);
        //    getIntentData();
        init();
        initRecycler();
    }

    /* private void getIntentData() {
             try {
                 Bundle bundle = getIntent().getExtras();
                 if (bundle != null){
                     String data = bundle.getString("jsonData");
                     jsonObject = new JSONObject(data);
                     cartBinding.txtStoreName.setText(jsonObject.getString("name"));
                 }
             }catch (Exception e){e.printStackTrace();}
         }
     */
    private void init() {
        context = CartActivity.this;
        currencyType = SharePreferenceUtil.getSPstringValue(context,SharePreferenceUtil.CURRENCY_TYPE);
        cartBinding.setPlaceOrderBtn(this::onCLick);
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Cart");
        findViewById(R.id.title_bar_back_arrow).setOnClickListener(v -> onBackPressed());
        if (VU.isConnectingToInternet(context))
            showCartList();
    }


    private void initRecycler() {

        //   cartBinding.refresh.setOnRefreshListener(this);
        //  GridLayoutManager gridLayoutManager = new GridLayoutManager(context,2);
        // cartBinding.recyclerFragHome.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        cartAdapter = new CartAdapter(context);
        cartAdapter.deleteClick((position, jsonObject) -> {   // recycler card view click
            dialogDeleteCartItem(jsonObject);
        });
        cartBinding.setCartAdapter(cartAdapter);
    }

  /*  // swipe refresh
    @Override
    public void onRefresh() {
       *//* if (VU.isConnectingToInternet(context)){
            getCatalogList();
        }*//*
        cartBinding.r.setRefreshing(false);
    }*/

    @Override //place order btn
    public void onCLick() {
        if (cartBinding.txtCartTotalAmt.getText().toString().substring(1).equalsIgnoreCase("0")) {
            Utils.getToast(context,getResources().getString(R.string.no_items_to_place_order));
        } else {
            if (VU.isConnectingToInternet(context))
                Log.e(TAG, "onCLick: noTaxTotal: "+subtotal +" "+noTaxTotal );
                startActivity(new Intent(context, ShippingAddressActivity.class)
                        .putExtra("cartDataArray",cartDataArray.toString())
                .putExtra("subTotal",String.valueOf(subtotal))
                .putExtra("noTaxTotal",String.valueOf(noTaxTotal)));
        }
    }

    private void showCartList() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_cart_item) + SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CART_ID)+"&status=ACTIVE");
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_cart_item));
        } catch (Exception e) {
            e.printStackTrace();
        }

        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        catalogViewModel.getAddToCartData(helper).observe(this, (response) -> {
            try {
                loadingDialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "showCartList: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        if (jsonObject.getJSONObject("data").length()==0){
                            subtotal = 0;
                            noTaxTotal = 0;
                            cartBinding.recyclerViewCart.setAdapter(null);
                            cartBinding.txtCartItemCount.setText("(0 Items)");
                            cartBinding.txtCartTotalAmt.setText(currencyType+"0");
                            Utils.getToast(context,getResources().getString(R.string.no_items_in_cart));
                        }else {
                            JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");
                            Log.e(TAG, "showCartList: resultArray: "+resultArray );
                            if (resultArray.length() > 0) {
                                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, jsonObject.getJSONObject("data").getString("count"));
                                cartBinding.txtCartItemCount.setText("(" + resultArray.length() + " Items)");
                                for (int i = 0; i < resultArray.length(); i++) {
                                    if (Boolean.valueOf(resultArray.getJSONObject(i).getString("tax_exempt"))) { // tax exempt : false  -> noTaxTotal
                                        noTaxTotal += (Double.valueOf(resultArray.getJSONObject(i).getString("line_total")));
                                    } else {                                                                             // tax exempt : true  -> subtotal
                                        subtotal += (Double.valueOf(resultArray.getJSONObject(i).getString("line_total")));
                                    }
                                }
                                cartBinding.txtCartTotalAmt.setText(currencyType + Utils.convertToUSDFormat((subtotal + noTaxTotal + "")));
                                //  cartBinding.txtVat.setText(Utils.convertToUSDFormat(subtotal * 0.08 + ""));
                                // cartBinding.txtFinalPayAmt.setText(Utils.convertToUSDFormat(String.valueOf(subtotal + (subtotal * 0.08))));
                                Log.e(TAG, "showCartList: subtotal: " + subtotal);
                                Log.e(TAG, "showCartList: noTaxTotal: " + noTaxTotal);
                                cartDataArray = resultArray;
                                cartAdapter.setData(resultArray);
                            } else {
                                subtotal = 0;
                                noTaxTotal = 0;
                                cartBinding.recyclerViewCart.setAdapter(null);
                                cartBinding.txtCartItemCount.setText("(0 Items)");
                                cartBinding.txtCartTotalAmt.setText(currencyType+"0");
                                Utils.getToast(context,getResources().getString(R.string.no_items_in_cart));
                            }
                        }
                    } else
                    Utils.getToast(context,getResources().getString(R.string.no_response));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

    private void deleteCart(JSONObject jsonObject) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.DELETE));  //delete method
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.delete_cart_item) +jsonObject.getString("cart_id") +"/?product_id="+jsonObject.getString("product_id"));
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.delete_cart_item));
        } catch (Exception e) {
            e.printStackTrace();
        }

        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        catalogViewModel.deleteProduct(helper).observe(this, (response) -> {
            try {
                loadingDialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "deleteCart: " + responseObj);
                    if (responseObj.getInt("response_code") == 200) {
                        subtotal = 0;
                        noTaxTotal = 0;
                        Utils.getToast(context,getResources().getString(R.string.item_deleted));
                        int count = Integer.valueOf(SharePreferenceUtil.getSPstringValue(context,SharePreferenceUtil.CART_COUNT)) - 1;
                        SharePreferenceUtil.setSPstring(context,SharePreferenceUtil.CART_COUNT,String.valueOf(count));
                        showCartList();  // refresh cart
                    } else {
                        Utils.getToast(context,getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

    public void dialogDeleteCartItem(JSONObject jsonObject) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_delete);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        ((TextView) dialog.findViewById(R.id.txt)).setText(getResources().getString(R.string.dialog_delete_item_txt));
        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (VU.isConnectingToInternet(context))
                    deleteCart(jsonObject);
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

}
