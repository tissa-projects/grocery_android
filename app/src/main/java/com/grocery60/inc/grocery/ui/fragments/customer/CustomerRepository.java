package com.grocery60.inc.grocery.ui.fragments.customer;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.ServiceCall.RestAsyncTask;

public class CustomerRepository {

    private static Application application;
    private static CustomerRepository instance;
    private static final String TAG = CustomerRepository.class.getSimpleName();


    public static CustomerRepository getInstance(Application applicationContext) {
        application = applicationContext;
        if (instance == null) {
            instance = new CustomerRepository();

        }
        return instance;
    }

    public MutableLiveData<String> customer(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }
}
