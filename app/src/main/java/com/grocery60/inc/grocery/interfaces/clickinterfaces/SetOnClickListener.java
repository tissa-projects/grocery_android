package com.grocery60.inc.grocery.interfaces.clickinterfaces;

import org.json.JSONObject;

public interface SetOnClickListener {
    void onCLick() ;
    interface setCardClick { void onClick(int position, JSONObject jsonObject);}
    interface SetPasswordChange { void onPasswordChangeClick();}
    interface SetprofileImage { void onProfileImageClick();}
}
