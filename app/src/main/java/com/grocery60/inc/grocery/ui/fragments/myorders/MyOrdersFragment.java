package com.grocery60.inc.grocery.ui.fragments.myorders;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.FragmentMyOrdersBinding;
import com.grocery60.inc.grocery.ui.activities.login.LoginActivity;
import com.grocery60.inc.grocery.ui.activities.myorderdetails.MyOrderDetailsActivity;

import org.json.JSONArray;
import org.json.JSONObject;

public class MyOrdersFragment extends Fragment {

    private MyOrdersViewModel viewModel;
    private FragmentMyOrdersBinding myOrdersBinding;
    private Context context;
    private Dialog dialog;
    private MyOrdersAdapter myOrdersAdapter;
    private View view;
    private static final String TAG = MyOrdersFragment.class.getName();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        myOrdersBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_orders, container, false);
        view = myOrdersBinding.getRoot();
        context = getActivity();
        boolean isUserLogin = UserSession.isUserLoggedIn(context);
        if (isUserLogin) {
            init();
            if (VU.isConnectingToInternet(context)) {
                getOrderList();
            }
        } else {
            Dialog dialog = CustomDialogs.dialogShowMsg(context, getResources().getString(R.string.login_first));
            dialog.setCancelable(false);
            dialog.findViewById(R.id.btn_ok).setOnClickListener(v -> {
                startActivity(new Intent(context, LoginActivity.class));
            });
        }
        return view;

    }

    private void init() {

        viewModel = ViewModelProviders.of(this).get(MyOrdersViewModel.class);
        myOrdersBinding.refresh.setOnRefreshListener(this::onRefresh);
        myOrdersAdapter = new MyOrdersAdapter(getActivity());
        myOrdersAdapter.ViewDetailsClick((position, jsonObject) -> {   // recycler View Details btn  click
            try {
                Log.e(TAG, "init: position: " + position);
                String currencyType = jsonObject.getJSONObject("order").getString("currency").toLowerCase().equals("usd") ? "$" : "₹";
                SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CURRENCY_TYPE, currencyType);
                Intent intent = new Intent(context, MyOrderDetailsActivity.class);
                intent.putExtra("jsonData", jsonObject.toString());
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        myOrdersAdapter.cancelDeleteClick((position, jsonObject) -> {   // recycler View Details btn  click
            if (VU.isConnectingToInternet(context))
                dialogCancelOrder(jsonObject);  //just setting active order as cancel
        });

        myOrdersBinding.setMyOrderAdapter(myOrdersAdapter);
    }

    //swife refersh for recycler
    private void onRefresh() {
        if (VU.isConnectingToInternet(context)) {
            getOrderList();
        }
        myOrdersBinding.refresh.setRefreshing(false);

    }

    private void getOrderList() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_order_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_order_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getOrderList(helper).observe(getActivity(), (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "getOrderList: " + responseObj);
                    if (responseObj.getInt("response_code") == 200) {
                        JSONArray resultArray = responseObj.getJSONArray("data");
                        if (resultArray.length() > 0) {
                            myOrdersAdapter.setData(resultArray);
                        } else {
                            Utils.getToast(context, getResources().getString(R.string.no_order_available));
                        }
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void dialogCancelOrder(JSONObject jsonObject) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_delete);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((TextView) dialog.findViewById(R.id.txt)).setText(getResources().getString(R.string.dialog_cancel_order_txt));

        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (VU.isConnectingToInternet(context))
                    orderCancel(jsonObject);
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void orderCancel(JSONObject jsonObject) {
        RestAPIClientHelper helper = new RestAPIClientHelper();

        try {
            String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.cancel_order) + jsonObject.getJSONObject("order").getString("order_id");
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.DELETE));
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_order_api));
            helper.setOrderId(jsonObject.getJSONObject("order").getString("order_id"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getOrderList(helper).observe(getActivity(), (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "orderCancel: " + responseObj);
                    if (responseObj.getInt("response_code") == 200) {
                        Utils.getToast(context, getResources().getString(R.string.order_cancl));
                        getOrderList();
                    } else {
                        CustomDialogs.dialogShowMsg(context, responseObj.getJSONObject("data").getString("msg"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}