package com.grocery60.inc.grocery.ui.activities.password;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.ActivityChangePasswordBinding;

import org.json.JSONObject;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityChangePasswordBinding changePasswordBinding;
    private ChangePasswordViewModel changePasswordViewModel;
    private Context context;
    private Dialog dialog;
    public static final String TAG = ChangePasswordActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changePasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        changePasswordViewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel.class);
        context = ChangePasswordActivity.this;
        init();;
    }


    private void init() {
        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Change Password");
        findViewById(R.id.title_bar_back_arrow).setOnClickListener(v -> onBackPressed());
        changePasswordBinding.btnUpdatePassword.setOnClickListener(this);
    }
    @Override  //change passwd click
    public void onClick(View v) {
        if (VU.isConnectingToInternet(context)){
            if (changePasswordViewModel.validate(context,changePasswordBinding)){
                updatePassword();
            }
        }
    }


    private void updatePassword() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.update_password);

        try {
            JSONObject req = new JSONObject();
            req.put("username",UserSession.getUserDetails(context).get("username"));
            req.put("old_password",changePasswordBinding.currentPassword.getText().toString());
            req.put("new_password1",changePasswordBinding.newPassword.getText().toString());
            req.put("new_password2",changePasswordBinding.confirmPassword.getText().toString());
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.update_password));
            helper.setUrlParameter(req.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        changePasswordViewModel.changePassoword(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObject = new JSONObject(response);
                    Log.e(TAG, "getProfile: " + responseObject);
                    if (responseObject.getInt("response_code") == 200) {
                        Utils.getToast(context,"Password changed  successfully");
                        finish();
                    } else {
                        CustomDialogs.dialogShowMsg(context, responseObject.getJSONObject("data").getString("msg"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
