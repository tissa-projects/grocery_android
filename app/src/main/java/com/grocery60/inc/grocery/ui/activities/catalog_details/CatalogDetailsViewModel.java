package com.grocery60.inc.grocery.ui.activities.catalog_details;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.databinding.ActivityCatalogDetailsBinding;
import com.grocery60.inc.grocery.ui.activities.catalog.CatalogRepository;

public class CatalogDetailsViewModel extends AndroidViewModel {

        private CatalogRepository catalogRepository;

    public CatalogDetailsViewModel(@NonNull Application application) {
            super(application);
            catalogRepository = CatalogRepository.getInstance(application);
        }


        public MutableLiveData<String> getAddToCartData(RestAPIClientHelper restAPIClientHelper) {
            return catalogRepository.setAddToCart(restAPIClientHelper);
        }


    //quantity dialog
    public void setQuantity(Context  context, ActivityCatalogDetailsBinding  catalogDetailsBinding) {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Quantity");
        // add a list
        builder.setItems(context.getResources().getStringArray(R.array.qty_array), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
               catalogDetailsBinding.txtQty.setText(context.getResources().getStringArray(R.array.qty_array)[position]);
                dialog.dismiss();
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }
}
