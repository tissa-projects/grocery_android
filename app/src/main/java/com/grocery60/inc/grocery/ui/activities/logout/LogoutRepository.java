package com.grocery60.inc.grocery.ui.activities.logout;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.ServiceCall.RestAsyncTask;

public class LogoutRepository {

    private static Application application;
    private static LogoutRepository instance;
    private static final String TAG = LogoutRepository.class.getSimpleName();


    public static LogoutRepository getInstance(Application applicationContext) {
        application = applicationContext;
        if (instance == null) {
            instance = new LogoutRepository();

        }
        return instance;
    }

    public MutableLiveData<String> logout(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }
}
