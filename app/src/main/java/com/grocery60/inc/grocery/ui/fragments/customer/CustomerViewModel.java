package com.grocery60.inc.grocery.ui.fragments.customer;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;

public class CustomerViewModel extends AndroidViewModel {


    private CustomerRepository repository;
    public CustomerViewModel(@NonNull Application application) {
        super(application);
        repository = CustomerRepository.getInstance(application);
    }

    public MutableLiveData<String> getCustomer(RestAPIClientHelper helper) {
        return repository.customer(helper);
    }
}
