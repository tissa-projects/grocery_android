package com.grocery60.inc.grocery.ui.fragments.support;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.fragment.app.Fragment;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.ui.activities.login.LoginActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class SupportFragment extends Fragment {

    private Dialog dialog;
    private Context context;
    private View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_support, container, false);
        context = getActivity();
        boolean isUserLogin = UserSession.isUserLoggedIn(context);
        if (isUserLogin) {
            if (VU.isConnectingToInternet(context)) {
                init();
            }
        } else {
            Dialog dialog = CustomDialogs.dialogShowMsg(context, getResources().getString(R.string.login_first));
            dialog.setCancelable(false);
            dialog.findViewById(R.id.btn_ok).setOnClickListener(v -> {
                startActivity(new Intent(context, LoginActivity.class));
            });
        }
        return root;
    }

    private void init() {
        dialog = ProgressDialog.show(getContext(), "Please wait", "Loading...");
        splashTime();
        WebView myWebView = root.findViewById(R.id.webview_support);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.loadUrl(getContext().getResources().getString(R.string.support));
    }

    public void splashTime() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 1500);
    }
}
