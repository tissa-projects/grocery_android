package com.grocery60.inc.grocery.interfaces;

public interface AsyncResponse {
    void processResponse(String response);
}
