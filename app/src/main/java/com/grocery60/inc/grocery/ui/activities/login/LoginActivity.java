package com.grocery60.inc.grocery.ui.activities.login;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.ActivityLoginBinding;
import com.grocery60.inc.grocery.interfaces.clickinterfaces.SetOnClickListener;
import com.grocery60.inc.grocery.ui.DashBoardActivity;
import com.grocery60.inc.grocery.ui.SplashActivity;
import com.grocery60.inc.grocery.ui.activities.catalog.CatalogActivity;
import com.grocery60.inc.grocery.ui.activities.registration.RegistrationActivity;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class LoginActivity extends AppCompatActivity implements SetOnClickListener, View.OnClickListener {

    private ActivityLoginBinding loginBinding;
    private LoginViewModel loginViewModel;
    private Context context;
    private Dialog loadingDialog;
    private JSONObject jsonObjectDataFromStore = null;
    public static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        context = LoginActivity.this;
        init();
        getIntentData();
        forceUpdate();

    }

    private void init() {
        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Login");
        findViewById(R.id.title_bar_back_arrow).setOnClickListener(v -> onBackPressed());
        loginBinding.setLoginBtnClick(this::onCLick);
        loginBinding.newUserSignUp.setOnClickListener(this);
        loginBinding.forgotPassword.setOnClickListener(this);
        loginBinding.forgotUser.setOnClickListener(this);
    }

    private void getIntentData() {  //when came from store page ie. home fragment
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                String data = bundle.getString("jsonData");
                jsonObjectDataFromStore = new JSONObject(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void checkForLogin() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.login);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("username", loginBinding.email.getText().toString());
            stringMap.put("password", loginBinding.Password.getText().toString());
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setCookie("");
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.login));
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        loginViewModel.login(helper).observe(this, (response) -> {
            try {
                loadingDialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "checkForLogin: " + jsonObject.toString());
                    if (jsonObject.getInt("response_code") == 200) {
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.AUTHORIZATION_TOKEN, jsonObject.getJSONObject("data").getString("token"));
                        UserSession.setIsuserLogin(context);
                        UserSession.setuserId(context, jsonObject.getJSONObject("data").getString("id"));
                        Intent intent = null;
                        if (jsonObjectDataFromStore == null) {
                            intent = new Intent(context, DashBoardActivity.class);
                        } else {
                            intent = new Intent(context, CatalogActivity.class).putExtra("jsonData", jsonObjectDataFromStore.toString());
                        }
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    } else if (jsonObject.getInt("response_code") == 401)
                        Utils.getToast(context, getResources().getString(R.string.credential_wrong));
                    else if (jsonObject.getInt("response_code") == 403)
                        Utils.getToast(context, getResources().getString(R.string.email_not_verified));
                    else
                        Utils.getToast(context, getResources().getString(R.string.no_response));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void ForgotPassowrd(String username) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.forgot_password_api);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("username", username);
            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.forgot_password_api));
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        loginViewModel.forgotPasword(helper).observe(this, (response) -> {
            try {
                loadingDialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "ForgotPassowrd: " + jsonObject.toString());
                    if (jsonObject.getInt("response_code") == 200) {
                        Utils.getToast(context, getResources().getString(R.string.password_snt));
                    } else {
                        CustomDialogs.dialogShowMsg(context, jsonObject.getJSONObject("data").getString("msg"));
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void ForgotUser(String email) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.forgot_user_api);
        try {
            JSONObject stringMap = new JSONObject();
            stringMap.put("email", email);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setAction(getResources().getString(R.string.forgot_user_api));
            helper.setUrlParameter(stringMap.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        loginViewModel.forgotPasword(helper).observe(this, (response) -> {
            try {
                loadingDialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "ForgotUser: " + jsonObject.toString());
                    if (jsonObject.getInt("response_code") == 200) {
                        Utils.getToast(context, getResources().getString(R.string.username_snt));
                    } else {
                        CustomDialogs.dialogShowMsg(context, jsonObject.getJSONObject("data").getString("msg"));
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.new_user_sign_up:
                startActivity(new Intent(context, RegistrationActivity.class));
                break;
            case R.id.forgot_password:
                dialogForgotPasswordUser("password");
                break;
            case R.id.forgot_user:
                dialogForgotPasswordUser("user");
                break;
        }
    }

    // login btn Click
    @Override
    public void onCLick() {
        if (validation()) {
            if (loginViewModel.validate(context, loginBinding)) {
                if (VU.isConnectingToInternet(context)) {
                    checkForLogin();
                }
            }
        }
    }


    private boolean validation() {
        if (VU.isEmpty(loginBinding.email)) {
            Utils.getToast(context, getResources().getString(R.string.username_validate));
            return false;
        } else if (VU.isEmpty(loginBinding.Password)) {
            Utils.getToast(context, getResources().getString(R.string.enter_password_validate));
            return false;
        }
        return true;
    }

    public void dialogForgotPasswordUser(String type) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_password_reset);

        EditText edtEmail = dialog.findViewById(R.id.txt_msg);

        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        if (type.equalsIgnoreCase("password"))
            edtEmail.setHint("Enter your username");
        else
            edtEmail.setHint("Enter your email");
        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener((view) -> {
            if (VU.isConnectingToInternet(context)) {
                if (!edtEmail.getText().toString().equalsIgnoreCase("")) {
                    if (type.equalsIgnoreCase("password"))
                        ForgotPassowrd(edtEmail.getText().toString());
                    else
                        ForgotUser(edtEmail.getText().toString());
                    dialog.dismiss();
                } else if (type.equalsIgnoreCase("password"))
                    Utils.getToast(context, getResources().getString(R.string.pass_username));
                else
                    Utils.getToast(context, getResources().getString(R.string.pass_email));
            }
        });

        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener((view) -> {
            dialog.dismiss();
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    //=================================force update  =====================

    public void forceUpdate() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        new ForceUpdateAsync(currentVersion, this).execute();
    }

    public class ForceUpdateAsync extends AsyncTask<String, String, String> {

        private String latestVersion;
        private String currentVersion;
        private Context context;

        public ForceUpdateAsync(String currentVersion, Context context) {
            this.currentVersion = currentVersion;
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.e("playStoreUrl", " : " + "https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en");
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en")
                        .timeout(10000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                latestVersion = sibElemet.text();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return latestVersion;
        }

        @Override
        protected void onPostExecute(String jsonObject) {
            if (latestVersion != null) {
                if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                    if (!(context instanceof SplashActivity)) {
                        if (!((Activity) context).isFinishing()) {
                            showForceUpdateDialog();
                        }
                    }
                }
            }
            super.onPostExecute(jsonObject);
        }

        public void showForceUpdateDialog() {

            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialog.setContentView(R.layout.dialog_app_update);
            dialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onClick(View v) {
                    if (VU.isConnectingToInternet(context)) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName())));
                        dialog.dismiss();
                    }
                }
            });
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            dialog.getWindow().setAttributes(lp);
        }

    }

}
