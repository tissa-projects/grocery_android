package com.grocery60.inc.grocery.ui.fragments.privacyus;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.grocery60.inc.grocery.R;

public class PrivacyPolicyFragment extends Fragment {

    Dialog dialog;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_privacy_policy, container, false);
        dialog = ProgressDialog.show(getContext(), "Please wait", "Loading...");
        splashTime();
        WebView myWebView = root.findViewById(R.id.webview_privacy_policy);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.loadUrl(getContext().getResources().getString(R.string.privacy_policy));
        return root;
    }

    public void splashTime() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 1300);
    }
}