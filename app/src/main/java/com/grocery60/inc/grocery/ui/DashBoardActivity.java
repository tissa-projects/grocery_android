package com.grocery60.inc.grocery.ui;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.ui.activities.login.LoginActivity;
import com.grocery60.inc.grocery.ui.activities.logout.LogoutViewModel;
import com.grocery60.inc.grocery.ui.activities.profile.ProfileActivity;
import com.grocery60.inc.grocery.ui.activities.registration.RegistrationActivity;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class DashBoardActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private Context context;
    private NavController navController;
    private LogoutViewModel logoutViewModel;
    private Dialog dialog;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private Toolbar toolbar;
    boolean doubleBackToExitPressedOnce = false;
    public static final String TAG = DashBoardActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        logoutViewModel = ViewModelProviders.of(this).get(LogoutViewModel.class);
        context = DashBoardActivity.this;
        //checkLocationPermission();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setNavigation();
        /*if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {*/
            navController.setGraph(R.navigation.mobile_navigation, null);
        forceUpdate();
        //}
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();

    }

    private void setNavigation() {
        // side drawer
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_my_orders/*, R.id.cartActivity*/,
                R.id.nav_about_us, R.id.nav_contact_us, R.id.nav_policy_privacy,R.id.nav_support,R.id.nav_faq)
                .setDrawerLayout(drawer)
                .build();
        LayoutInflater inflater = getLayoutInflater();
        View simpleHeaderView = inflater.inflate(R.layout.nav_header_dash_board, null, true);
        View LoginSignUpHeaderView = inflater.inflate(R.layout.nav_header_login_signup, null, true);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 500);
        simpleHeaderView.setLayoutParams(params);
        LoginSignUpHeaderView.setLayoutParams(params);

        boolean isUserLogin = UserSession.isUserLoggedIn(context);
        Log.e(TAG, "onResume: isUserLogin: " + isUserLogin);
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        // navController.popBackStack(R.id.nav_home, false);


        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        if (isUserLogin) {
            navigationView.removeHeaderView(LoginSignUpHeaderView);
            navigationView.addHeaderView(simpleHeaderView);
            clickForProfileNLogout(navigationView);
        } else {
            navigationView.addHeaderView(LoginSignUpHeaderView);
            clickForLoginNsignUp(navigationView);
        }
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    private void clickForLoginNsignUp(NavigationView navigationView) {

        View getHeaderView = navigationView.getHeaderView(0);

        Button btnLogin = getHeaderView.findViewById(R.id.btn_login);
        Button btnSignUp = getHeaderView.findViewById(R.id.btn_sign_up);

        btnLogin.setOnClickListener(v -> startActivity(new Intent(context, LoginActivity.class)));
        btnSignUp.setOnClickListener(v -> startActivity(new Intent(context, RegistrationActivity.class)));
    }

    private void clickForProfileNLogout(NavigationView navigationView) {

        View getHeaderView = navigationView.getHeaderView(0);

        Button btnProfile = getHeaderView.findViewById(R.id.btn_profile);
        Button btnLogout = getHeaderView.findViewById(R.id.btn_logout);

        btnProfile.setOnClickListener(v -> startActivity(new Intent(context, ProfileActivity.class)));
        btnLogout.setOnClickListener(v -> {
            drawer.closeDrawer(Gravity.LEFT);
            Dialog dialog = CustomDialogs.dialogCancelOkBtn(context, getResources().getString(R.string.are_you_sure_want_to_logout));
            Button okbtn = dialog.findViewById(R.id.btn_ok);
            Button cancelbtn = dialog.findViewById(R.id.btn_cancel);
            okbtn.setText("Yes");
            cancelbtn.setText("No");
            okbtn.setOnClickListener(v1 -> {
                if (VU.isConnectingToInternet(context)) {
                    logout();
                    dialog.dismiss();
                }
            });
        });
    }


    /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dash_board, menu);
        return true;
    }*/

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "checkLocationPermission: if 1");
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                Log.e(TAG, "checkLocationPermission: if");
            } else {
                Log.e(TAG, "checkLocationPermission: else");
                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        } else {
            Log.e(TAG, "checkLocationPermission: else 1");
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(context,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        Log.e(TAG, "onRequestPermissionsResult: if");
                        navController.setGraph(R.navigation.mobile_navigation, null);
                    }
                } else {
                    Log.e(TAG, "onRequestPermissionsResult: if");
                    checkLocationPermission();
                }
                return;
            }
        }
    }

    private void logout() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.logout_api);
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setCookie("");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        logoutViewModel.logout(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "logout: " + jsonObject.toString());
                    if (jsonObject.getInt("response_code") == 200) {
                        UserSession.logoutUser(context, getResources().getString(R.string.logout));
                        SharePreferenceUtil.clear(context);
                     //   navController.setGraph(R.navigation.mobile_navigation, null);
                    } else
                        Utils.getToast(context, getResources().getString(R.string.no_response));

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    //=================================force update  =====================

    public void forceUpdate() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        new ForceUpdateAsync(currentVersion, this).execute();
    }

    public class ForceUpdateAsync extends AsyncTask<String, String, String> {

        private String latestVersion;
        private String currentVersion;
        private Context context;

        public ForceUpdateAsync(String currentVersion, Context context) {
            this.currentVersion = currentVersion;
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.e("playStoreUrl", " : " + "https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en");
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en")
                        .timeout(10000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                latestVersion = sibElemet.text();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return latestVersion;
        }

        @Override
        protected void onPostExecute(String jsonObject) {
            if (latestVersion != null) {
                if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                    if (!(context instanceof SplashActivity)) {
                        if (!((Activity) context).isFinishing()) {
                            showForceUpdateDialog();
                        }
                    }
                }
            }
            super.onPostExecute(jsonObject);
        }

        public void showForceUpdateDialog() {

            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialog.setContentView(R.layout.dialog_app_update);
            dialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onClick(View v) {
                    if (VU.isConnectingToInternet(context)) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName())));
                        dialog.dismiss();
                    }
                }
            });
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            dialog.getWindow().setAttributes(lp);
        }

    }
}
