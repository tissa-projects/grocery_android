package com.grocery60.inc.grocery.ui.fragments.faq;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.grocery60.inc.grocery.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FAQFragment extends Fragment {

    private Dialog dialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_f_a_q, container, false);
        dialog = ProgressDialog.show(getContext(), "Please wait", "Loading...");
        splashTime();
        WebView myWebView = root.findViewById(R.id.webview_contact_us);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.loadUrl(getContext().getResources().getString(R.string.faq));
        return root;
    }
    public void splashTime() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 1300);
    }
}
