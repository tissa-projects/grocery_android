package com.grocery60.inc.grocery.ui.activities.payment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.ActivityIndPaymentBinding;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class INDPaymentActivity extends AppCompatActivity implements PaymentResultWithDataListener {

    private ActivityIndPaymentBinding paymentBinding;
    private PaymentViewModel paymentViewModel;
    //private PaymentShippingMethodAdapter paymentShippingMethodAdapter;
    private Context context;
    private Dialog dialog, dialogGetItem;
    private JSONArray cartDataArray = null;
    private JSONObject customerObj = null, addressObj = null, OrderDetails = null;
    private NavController navController;
    private String tip = "0", customTip = "0", shippingId = "0", shippingMethdName, strCounntryShortCode, order_id, receipt, subTotal = "0", noTaxTotal = "0";
    private String currencyType;
    public static final String TAG = INDPaymentActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        paymentBinding = DataBindingUtil.setContentView(this, R.layout.activity_ind_payment);
        paymentViewModel = ViewModelProviders.of(this).get(PaymentViewModel.class);
        context = INDPaymentActivity.this;
        currencyType = SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.CURRENCY_TYPE);
        getIntentData();
        if (VU.isConnectingToInternet(context)) {
            addFees();
        }
        init();
        //  setRecyclerForShippingMethod();
    }

    /*private void setRecyclerForShippingMethod() {
            paymentShippingMethodAdapter = new PaymentShippingMethodAdapter(context);
            paymentShippingMethodAdapter.radioBtnClick((position, jsonObject) -> {   // recycler click

                try {
                shippingId = jsonObject.getString("id");
                lastSelectedPosition = position;
                addFees();
            }catch (Exception e){e.printStackTrace();}
            });
            paymentBinding.setShippingMethodAdapter(paymentShippingMethodAdapter);
    }*/

    private void init() {
        TextView headerText = findViewById(R.id.toolbar_header_text);
        headerText.setText("Payment");
        findViewById(R.id.title_bar_back_arrow).setOnClickListener(v -> onBackPressed());
        /*if (VU.isConnectingToInternet(context)) {
            getShippingMethods();
        }*/
        paymentBinding.serviceFeeNote.setOnClickListener(v -> {
            new SimpleTooltip.Builder(this)
                    .anchorView(paymentBinding.serviceFeeNote)
                    .text(getResources().getString(R.string.service_fee_note))
                    .gravity(Gravity.TOP)
                    .animated(true)
                    .transparentOverlay(false).textColor(Color.WHITE)
                    .build()
                    .show();
        });

        paymentBinding.btnNext.setOnClickListener(v -> {
            if (VU.isConnectingToInternet(context)) {
                setOrderDetails();
            }
        });

        paymentBinding.txtChangeTip.setOnClickListener(v -> {
            dialogCustomTip();
        });

    }

    private void getIntentData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                Log.e(TAG, "getIntentData: bundle: " + bundle.toString());
                String data = bundle.getString("bundle");
                String data1 = bundle.getString("cartDataArray");
                strCounntryShortCode = bundle.getString("counntryShortCode");
                JSONObject bundleData = new JSONObject(data);
                Log.e(TAG, "getIntentData: data1: " + data1);
                cartDataArray = new JSONArray(data1);
                Log.e(TAG, "getIntentData: " + cartDataArray.toString());
                String strCustomer = bundleData.getString("customerObj");
                String strAddress = bundleData.getString("addressObj");
                subTotal = bundle.getString("subTotal");
                noTaxTotal = bundle.getString("noTaxTotal");
                shippingMethdName = bundle.getString("shippingMethdName");
                shippingId = bundle.getString("shippingId");
                //      String strShippingAddress = bundleData.getString("shippingAdddressObj");
                customerObj = new JSONObject(strCustomer);
                addressObj = new JSONObject(strAddress);
                //shippingAddressObj = new JSONObject(strShippingAddress);

                Log.e(TAG, "onCreate: customerObj: " + customerObj.toString());
                Log.e(TAG, "onCreate: addressObj: " + addressObj.toString());
                Log.e(TAG, "onCreate: noTaxTotal: " + noTaxTotal.toString());
                Log.e(TAG, "onCreate: subTotal: " + subTotal.toString());
                Log.e(TAG, "onCreate: shippingMethdName: " + shippingMethdName.toString());
                Log.e(TAG, "onCreate: shippingId: " + shippingId.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addFees() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        //   String tip = (cartBinding.txtTip.getText().toString().equalsIgnoreCase("")) ? "0" : cartBinding.txtTip.getText().toString();
        try {
            JSONObject reqObj = new JSONObject();
            reqObj.put("sub_total", subTotal);
            reqObj.put("no_tax_total", noTaxTotal);
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("shipping_id", shippingId);
            if (customTip.equalsIgnoreCase("0")) {
                reqObj.put("tip", tip);  // %
            } else {
                reqObj.put("custom_tip", customTip); // normal value
            }
            Log.e(TAG, "addFees: "+reqObj.toString() );
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.add_fees));
            helper.setAction(getResources().getString(R.string.add_fees));
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        paymentViewModel.addFee(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "addFees: " + responseObj);
                    if (responseObj.getInt("response_code") == 200) {
                        responseObj = responseObj.getJSONObject("data");
                        Log.e(TAG, "addFees: " + responseObj.toString());

                        paymentBinding.txtSubTotal.setText(currencyType + responseObj.getString("sub_total"));
                        paymentBinding.txtVat.setText(currencyType + responseObj.getString("tax"));
                        paymentBinding.txtTip.setText(currencyType + responseObj.getString("tip"));
                        paymentBinding.txtServiceFee.setText(currencyType + responseObj.getString("service_fee"));
                        paymentBinding.txtDiscount.setText(currencyType + responseObj.getString("discount"));
                        paymentBinding.txtTotal.setText(currencyType + responseObj.getString("total"));
                        paymentBinding.txtShippingFee.setText(currencyType + responseObj.getString("shipping_fee"));
                        //  startActivity(new Intent(context, CheckOutActivity.class));
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

    /*private void getShippingMethods() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.GET));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_shipping_methods) +"status=ACTIVE&store_id="+SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.STORE_ID_WRT_CART));
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogGetItem = ProgressDialog.show(context, "Please wait", "Loading...");
        paymentViewModel.getShippingFees(helper).observe(this, (response) -> {
            try {
                dialogGetItem.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "getShippingMethods: " + responseObj);
                    if (responseObj.getInt("response_code") == 200) {
                        JSONArray jsonArray = responseObj.getJSONObject("data").getJSONArray("results");
                            if (jsonArray.length() ==0){Utils.getToast(context,"No shipping method available");}
                        paymentShippingMethodAdapter.setData(jsonArray);
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }*/

    private void setOrderDetails() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.set_order_details_api);
        try {
            reqObj.put("status", "active");  //customer always be active
            String currency = "INR";
            reqObj.put("currency", currency);
            reqObj.put("subtotal", paymentBinding.txtSubTotal.getText().toString().substring(1));
            reqObj.put("total", paymentBinding.txtTotal.getText().toString().substring(1));
            reqObj.put("extra", paymentBinding.edtExtraAnnotation.getText().toString());
            reqObj.put("customer", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("tip", paymentBinding.txtTip.getText().toString().substring(1));
            reqObj.put("service_fee", paymentBinding.txtServiceFee.getText().toString().substring(1));
            reqObj.put("tax", paymentBinding.txtVat.getText().toString().substring(1));
            reqObj.put("discount", paymentBinding.txtDiscount.getText().toString().substring(1));
            reqObj.put("shipping_fee", paymentBinding.txtShippingFee.getText().toString().substring(1));
            JSONArray reqArray = new JSONArray();
            for (int i = 0; i < cartDataArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("cart_item_id", cartDataArray.getJSONObject(i).getString("cart_item_id"));
                jsonObject.put("cart_id", cartDataArray.getJSONObject(i).getString("cart_id"));
                Log.e(TAG, "setOrderDetails:cart_id:  " + cartDataArray.getJSONObject(i).getString("cart_id"));
                jsonObject.put("product_id", cartDataArray.getJSONObject(i).getString("product_id"));
                jsonObject.put("product_name", cartDataArray.getJSONObject(i).getString("product_name"));
                jsonObject.put("product_url", cartDataArray.getJSONObject(i).getString("product_url"));
                jsonObject.put("price", cartDataArray.getJSONObject(i).getString("price"));
                jsonObject.put("quantity", cartDataArray.getJSONObject(i).getString("quantity"));
                jsonObject.put("line_total", cartDataArray.getJSONObject(i).getString("line_total"));
                jsonObject.put("canceled", false);
                reqArray.put(jsonObject);
            }
            reqObj.put("items", reqArray);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
            helper.setAction(getResources().getString(R.string.set_order_details_api));
        } catch (Exception e) {
            e.printStackTrace();

        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        paymentViewModel.setOrder(helper).observe((LifecycleOwner) context, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "setOrderDetails: " + jsonObject);
                    JSONObject dataObj = jsonObject.getJSONObject("data");
                    if (jsonObject.getInt("response_code") == 200) {
                        OrderDetails = dataObj;
                        Log.e(TAG, "setOrderDetails: " + dataObj.toString());
                        getIndiaPaymentDetails(dataObj.getString("order_id"));   //order id
                    } else if (jsonObject.getInt("response_code") == 500) {
                        CustomDialogs.dialogShowMsg(context, dataObj.getString("msg"));
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void getIndiaPaymentDetails(String orderId) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_india_payment_api));
            JSONObject jsonObject = new JSONObject();

            JSONObject metaDataObject = new JSONObject();
            metaDataObject.put("order_id", orderId);
            metaDataObject.put("store_id", SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.STORE_ID_WRT_CART));
            metaDataObject.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            metaDataObject.put("shippingmethod_id", shippingId);

            jsonObject.put("currency", "INR");
            jsonObject.put("amount", paymentBinding.txtTotal.getText().toString().substring(1));
            jsonObject.put("metadata", metaDataObject);
            helper.setUrlParameter(jsonObject.toString());
            helper.setAction(getResources().getString(R.string.get_india_payment_api));
            helper.setOrderId(orderId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogGetItem = ProgressDialog.show(context, "Please wait", "Loading...");
        paymentViewModel.getIndiaPaymentDetails(helper).observe(this, (response) -> {
            try {
                dialogGetItem.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "getIndiaPaymentDetails: " + responseObj);
                    if (responseObj.getInt("response_code") == 200) {
                        order_id = responseObj.getJSONObject("data").getString("id");
                        receipt = responseObj.getJSONObject("data").getString("receipt");
                  /*  startActivity(new Intent(context,INDPaymentActivity.class)
                            .putExtra("email",customerObj.getString("email"))
                    .putExtra("name",customerObj.getString("first_name")+" "+customerObj.getString("last_name"))
                    .putExtra("phn_no",customerObj.getString("phone_number"))
                    .putExtra("order_id",order_id)
                    .putExtra("receipt",receipt)
                    .putExtra("amt",paymentBinding.txtTotal.getText().toString().substring(1)));
*/
                        Checkout.preload(getApplicationContext());
                        startPayment();
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void startPayment() {

        /**
         * Instantiate Checkout
         */
        Checkout checkout = new Checkout();
        checkout.setKeyID(getResources().getString(R.string.payment_gatwat_key));
        /**
         * Set your logo here
         */
        checkout.setImage(R.drawable.grocery_logo);

        /**
         * Reference to current activity
         */
        final Activity activity = this;

        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */


        try {
            JSONObject options = new JSONObject();

            options.put("name", customerObj.getString("first_name") + " " + customerObj.getString("last_name"));
            //    options.put("description", receipt);
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("order_id", order_id);//from response of step 3.
            options.put("theme.color", "#199a48");  //color primary
            options.put("currency", "INR");
            //  options.put("amount", 1 * 100);
            options.put("amount", Double.valueOf(paymentBinding.txtTotal.getText().toString().substring(1)) * 100);//pass amount in currency subunits
            options.put("prefill.email", customerObj.getString("email"));
            options.put("prefill.contact", customerObj.getString("phone_number"));
            checkout.open(activity, options);
        } catch (Exception e) {
            Log.e(TAG, "Error in starting Razorpay Checkout", e);
        }
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentId, PaymentData paymentData) {
        Log.e(TAG, "onPaymentSuccess:razorpayPaymentId  " + razorpayPaymentId + "   " + paymentData.getOrderId() + " " + paymentData.getSignature());
        getIndiaPaymentResponse(order_id, razorpayPaymentId, paymentData.getSignature());
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        Utils.getToast(context, getResources().getString(R.string.razor_payment_failed) + " " + order_id);
        Log.e(TAG, "onPaymentError: " + i + " " + s + " " + paymentData.toString());

    }


    private void getIndiaPaymentResponse(String orderId, String paymentId, String signature) {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.PUT));
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.get_india_payment_api) + orderId);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("razorpay_payment_id", paymentId);
            jsonObject.put("razorpay_signature", signature);
         //   jsonObject.put("store", SharePreferenceUtil.getSPstringValue(context, SharePreferenceUtil.STORE_ID_WRT_CART));
            jsonObject.put("customer_id", UserSession.getUserDetails(context).get(UserSession.KEY_USER_ID));
            helper.setUrlParameter(jsonObject.toString());
        } catch (Exception e) {
            helper.setAction(getResources().getString(R.string.get_india_payment_api));
            helper.setOrderId(orderId);
            e.printStackTrace();
        }

        dialogGetItem = ProgressDialog.show(context, "Please wait", "Loading...");
        paymentViewModel.getIndiaPaymentDetails(helper).observe(this, (response) -> {
            try {
                dialogGetItem.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "getIndiaPaymentDetails: " + responseObj);
                    if (responseObj.getInt("response_code") == 200) {
                        Utils.getToast(context, getResources().getString(R.string.order_placed));
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.STORE_ID_WRT_CART, "");
                        SharePreferenceUtil.setSPstring(context, SharePreferenceUtil.CART_COUNT, "0");
                        Intent intent = new Intent(context, BackToStoreActivity.class)
                                .putExtra("OrderDetails", OrderDetails.toString())
                                .putExtra("addressDetails", addressObj.toString());
                        startActivity(intent);
                    } else {
                        Utils.getToast(context, getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    public void dialogCustomTip() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_delevery_tip);
        RadioGroup radioGroup = ((RadioGroup) dialog.findViewById(R.id.radio_grup));
        EditText edtTip = ((EditText) dialog.findViewById(R.id.edt_tip));
        ((RadioButton) dialog.findViewById(R.id.amt_4)).setText(currencyType + "20");
        ((RadioButton) dialog.findViewById(R.id.amt_6)).setText(currencyType + "50");
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) dialog.findViewById(checkedId); // find the radiobutton by returned id
                if (radioButton.getText().toString().equals("Other amount")) {
                    edtTip.setEnabled(true);
                } else {
                    edtTip.setEnabled(false);
                }
            }
        });

        ((Button) dialog.findViewById(R.id.btn_submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = radioGroup.getCheckedRadioButtonId();  // get selected radio button from radioGroup
                RadioButton radioButton = (RadioButton) dialog.findViewById(selectedId); // find the radiobutton by returned id
                if (selectedId == -1) {
                    Utils.getToast(context, getResources().getString(R.string.select_tip_option));
                } else {
                    if (radioButton.getText().equals("Other amount")) {
                        if (edtTip.getText().toString().equals("")) {
                            Utils.getToast(context, "Please enter Other amount");

                        } else {
                            paymentBinding.txtTip.setText(currencyType + edtTip.getText().toString());
                            customTip = edtTip.getText().toString();
                            tip = "0";
                            if (VU.isConnectingToInternet(context)) {
                                addFees();
                            }
                            dialog.dismiss();
                        }
                    } else {
                        if (radioButton.getText().toString().contains(currencyType)) {
                            paymentBinding.txtTip.setText(radioButton.getText().toString());
                            customTip = radioButton.getText().toString().substring(1);
                            tip = "0";
                        } else {
                            paymentBinding.txtTip.setText(currencyType + radioButton.getText().toString().split("%")[0]);
                            customTip = "0";
                            tip = radioButton.getText().toString().split("%")[0];
                        }
                        dialog.dismiss();
                        if (VU.isConnectingToInternet(context)) {
                            addFees();
                        }
                    }

                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

}
