package com.grocery60.inc.grocery.ui.activities.login;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.databinding.ActivityLoginBinding;

public class LoginViewModel extends AndroidViewModel {

    //private Application application;
    private LoginRepository loginRepository;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        //  loginRepository = new LoginRepository(application);
        loginRepository = LoginRepository.getInstance(application);
    }

    public LiveData<String> login(RestAPIClientHelper restAPIClientHelper) {
        return loginRepository.getLoginData(restAPIClientHelper);

    }

    public LiveData<String> forgotPasword(RestAPIClientHelper restAPIClientHelper) {
        return loginRepository.forgotPassword(restAPIClientHelper);
    }

    public LiveData<String> getCustomer(RestAPIClientHelper restAPIClientHelper) {
        return loginRepository.getCustomer(restAPIClientHelper);
    }


    public boolean validate(Context context, ActivityLoginBinding loginBinding){
        if (loginBinding.email.getText().toString().equalsIgnoreCase("")){
            Utils.getToast(context,"Enter "+context.getResources().getString(R.string.Email));
            return false;
        }else if (loginBinding.Password.getText().toString().equalsIgnoreCase("")){
            Utils.getToast(context,"Enter "+context.getResources().getString(R.string.pasword));
            return false;
        }
        return true;
    }



}
