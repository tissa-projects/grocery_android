package com.grocery60.inc.grocery.ui.activities.catalog;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.webkit.CookieManager;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.databinding.RecyclerCatalogActivityBinding;
import com.grocery60.inc.grocery.interfaces.clickinterfaces.SetOnClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class CatalogAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private String currencyType;
    private SetOnClickListener.setCardClick setOnClickListenerForCardView;
    private ArrayList<JSONObject> productList;
    private static final String TAG = CatalogAdapter.class.getSimpleName();


    public CatalogAdapter(Context context) {
        this.context = context;
        productList = new ArrayList<>();
        currencyType = SharePreferenceUtil.getSPstringValue(context,SharePreferenceUtil.CURRENCY_TYPE);


    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerCatalogActivityBinding recyclerCatalogActivityBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_catalog_activity, parent, false);
        return new MyViewHolder(recyclerCatalogActivityBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            if (holder instanceof MyViewHolder) {
                MyViewHolder myViewHolder = (MyViewHolder) holder;
                String imgUrl = "";
                JSONObject jsonObject = productList.get(position);

                if (jsonObject.has("product_name")) {
                    myViewHolder.recyclerCatalogActivityBinding.txtProductName.setText(jsonObject.getString("product_name"));
                } else {
                    myViewHolder.recyclerCatalogActivityBinding.txtProductName.setText(jsonObject.getString("productName"));
                }
                if (jsonObject.has("product_url")) {
                    imgUrl=jsonObject.getString("product_url");
                } else {
                    imgUrl = jsonObject.getString("productUrl");
                }
                if ((!imgUrl.equalsIgnoreCase("")) || (imgUrl != null) ) {
                    Utils.displayImageOriginalString(context, myViewHolder.recyclerCatalogActivityBinding.iconDash2, imgUrl);
                }
                myViewHolder.recyclerCatalogActivityBinding.txtPrice.setText(currencyType +
                        Utils.convertToUSDFormat(jsonObject.getString("price")));
                myViewHolder.recyclerCatalogActivityBinding.setCardClick(() -> {
                    setOnClickListenerForCardView.onClick(position, jsonObject);
                });

           //     myViewHolder.bind(catalogModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void setData(JSONArray jsonArray) {

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                productList.add(jsonArray.getJSONObject(i));
            }
            Log.e(TAG, "setData: "+productList.toString() );
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void clearProductLiist(){
       productList = null;
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeSessionCookie();
        cookieManager.removeAllCookie();
        productList = new ArrayList<>();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        public RecyclerCatalogActivityBinding recyclerCatalogActivityBinding;

        public MyViewHolder(@NonNull RecyclerCatalogActivityBinding recyclerCatalogActivityBinding) {
            super(recyclerCatalogActivityBinding.getRoot());
            this.recyclerCatalogActivityBinding = recyclerCatalogActivityBinding;
        }

        /*public void bind(Object obj) {
            recyclerCatalogActivityBinding.setCatalogModel((CatalogModel) obj);
            recyclerCatalogActivityBinding.executePendingBindings();
        }*/
    }

    public void cardViewClick(SetOnClickListener.setCardClick setOnClickListener) {
        setOnClickListenerForCardView = setOnClickListener;
    }

}
