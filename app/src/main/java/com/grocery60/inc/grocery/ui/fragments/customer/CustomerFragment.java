package com.grocery60.inc.grocery.ui.fragments.customer;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.FragmentCustomerBinding;

import org.json.JSONArray;
import org.json.JSONObject;

public class CustomerFragment extends Fragment {

    FragmentCustomerBinding customerBinding;
    private CustomerViewModel viewModel;
    private NavController navController;
    private Context context;
    private Dialog dialog;
    private JSONObject customerObj;
    private Bundle bundle = null;
    private String lastAccess = null;
    public static final String TAG = CustomerFragment.class.getSimpleName();

    public CustomerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        customerBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_customer, container, false);
        viewModel = ViewModelProviders.of(this).get(CustomerViewModel.class);
        context = getActivity();
        return customerBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        customerObj = new JSONObject();
        customerBinding.btnNext.setOnClickListener(v -> {
            navController.navigate(R.id.addressFragment, bundle);
        });

        if (VU.isConnectingToInternet(context))
            getCustomer();
    }


    private void getCustomer() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_customer_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
            helper.setAction(getResources().getString(R.string.get_customer_api));
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getCustomer(helper).observe(getActivity(), (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject addressObject = new JSONObject(response);
                    Log.e(TAG, "getCustomer: " + addressObject);
                    if (addressObject.getInt("response_code") == 200) {
                        setCustomerData(addressObject);
                    } else {
                        Utils.getToast(context,getResources().getString(R.string.no_response));

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void setCustomerData(JSONObject jsonObject) {
        try {
            JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");
            Log.e(TAG, "getCustomer: " + resultArray.toString());
            JSONObject resultObj = resultArray.getJSONObject(0);
            lastAccess = resultObj.getString("last_access");
            customerObj = resultObj.getJSONObject("customer");
            customerBinding.edtSalutation.setText(resultObj.getString("salutation"));
            customerBinding.edtMNo.setText(resultObj.getString("phone_number"));
            customerBinding.edtFName.setText(customerObj.getString("first_name"));
            customerBinding.edtLName.setText(customerObj.getString("last_name"));
            customerBinding.edtEmail.setText(customerObj.getString("email"));
            customerObj.put("phone_number",resultObj.getString("phone_number"));
            bundle = new Bundle();
            bundle.putString("customerObj", customerObj.toString());
        }catch (Exception e){e.printStackTrace();}
    }


   /* private void UpdateCustomer() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.update_customer_api) + UserSession.getUserDetails(context).get("user_id");
        try {

            JSONObject reqObj = new JSONObject();
            reqObj.put("email",SharePreferenceUtil.getSPstringValue(context,SharePreferenceUtil.CART_ID));
            reqObj.put("first_name",customerBinding.edtFName.getText().toString());
            reqObj.put("last_name",customerBinding.edtLName.getText().toString());
            reqObj.put("phone_number",customerBinding.edtMNo.getText().toString());
            reqObj.put("salutation",customerBinding.edtSalutation.getText().toString());
            reqObj.put("is_active",true);
            reqObj.put("username",customerObj.getString("username"));
            reqObj.put("last_access",lastAccess);

            helper.setContentType("application/json");
            helper.setMethodType("PUT");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getCustomer(helper).observe(getActivity(), (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getCustomer: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        setCustomerData(jsonObject);
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.no_response), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }*/
}
