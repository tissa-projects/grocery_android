package com.grocery60.inc.grocery.ui.fragments.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.databinding.RecyclerHomeFragmentBinding;
import com.grocery60.inc.grocery.interfaces.clickinterfaces.SetOnClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class HomeFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private static final String TAG = HomeFragmentAdapter.class.getSimpleName();

    private SetOnClickListener.setCardClick setOnClickListener;
    private ArrayList<JSONObject> storeList;
    private HomeFragmentModel homeFragmentModel;


    public HomeFragmentAdapter(Context context) {
        this.context = context;
        homeFragmentModel = new HomeFragmentModel();
        storeList = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerHomeFragmentBinding homeFragmentBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.recycler_home_fragment, parent, false);

        return new MyViewHolder(homeFragmentBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            if (holder instanceof MyViewHolder) {
                MyViewHolder myViewHolder = (MyViewHolder) holder;
                String imgUrl = null;
                HomeFragmentModel homeFragmentModel = new HomeFragmentModel();
                JSONObject jsonObject = storeList.get(position);
                if (jsonObject.has("store_url")) {
                    imgUrl = jsonObject.getString("store_url");
                } else {
                    imgUrl = jsonObject.getString("storeUrl");
                }
                homeFragmentModel.setAddress(jsonObject.getString("address"));
                homeFragmentModel.setStoreName(jsonObject.getString("name"));
                homeFragmentModel.setZipcode(jsonObject.getString("zip"));
                homeFragmentModel.setCity(jsonObject.getString("city"));
                if ((!imgUrl.equals("null")) /*||(!imgUrl.equals(null))*/){
                Utils.displayImageOriginalString(context, myViewHolder.recyclerHomeFragmentBinding.iconDash2, imgUrl);}
                myViewHolder.recyclerHomeFragmentBinding.setCardClick(() -> {
                    setOnClickListener.onClick(position, jsonObject);
                });
                myViewHolder.bind(homeFragmentModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        //return 10;
        return storeList.size();

    }

    public void setData(JSONArray jsonArray) {

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                storeList.add(jsonArray.getJSONObject(i));
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearProductLiist() {
        storeList = new ArrayList<>();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        public RecyclerHomeFragmentBinding recyclerHomeFragmentBinding;

        public MyViewHolder(@NonNull RecyclerHomeFragmentBinding recyclerHomeFragmentBinding) {
            super(recyclerHomeFragmentBinding.getRoot());
            this.recyclerHomeFragmentBinding = recyclerHomeFragmentBinding;
        }

        public void bind(Object obj) {
            recyclerHomeFragmentBinding.setHomeFragmentModel((HomeFragmentModel) obj);
            recyclerHomeFragmentBinding.executePendingBindings();
        }
    }

    public void cardViewClick(SetOnClickListener.setCardClick setOnClickListener) {
        this.setOnClickListener = setOnClickListener;
    }

}
