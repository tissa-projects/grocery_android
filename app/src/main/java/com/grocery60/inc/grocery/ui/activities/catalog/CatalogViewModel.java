package com.grocery60.inc.grocery.ui.activities.catalog;

import android.app.Application;
import android.os.CountDownTimer;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.databinding.ActivityCatalogBinding;

import java.util.concurrent.TimeUnit;

public class CatalogViewModel extends AndroidViewModel {

    private Application application;
    private CatalogRepository repository;

    public CatalogViewModel(@NonNull Application application) {
        super(application);
        repository = CatalogRepository.getInstance(application);
    }

    public MutableLiveData<String> getData(RestAPIClientHelper restAPIClientHelper) {
        return repository.getData(restAPIClientHelper);
    }

    public MutableLiveData<String> getAddToCartData(RestAPIClientHelper restAPIClientHelper) {
        return repository.setAddToCart(restAPIClientHelper);
    }

    public MutableLiveData<String> deleteProduct(RestAPIClientHelper restAPIClientHelper) {
        return repository.deleteProduct(restAPIClientHelper);
    }

    public MutableLiveData<String> getCart(RestAPIClientHelper helper){
        return repository.getCart(helper);
    }
    public MutableLiveData<String> createCart(RestAPIClientHelper helper){
        return repository.createCart(helper);
    }

    public LiveData<String> getCartCount(RestAPIClientHelper restAPIClientHelper) {
        return repository.getCartCount(restAPIClientHelper);
    }

    public LiveData<String> getcategoryFilter(RestAPIClientHelper restAPIClientHelper) {
        return repository.getcategoryFilter(restAPIClientHelper);
    }

    public LiveData<String> deleteCart(RestAPIClientHelper restAPIClientHelper) {
        return repository.deleteCart(restAPIClientHelper);
    }

    public void timer(ActivityCatalogBinding catalogBinding){
        int noOfMinutes = 60 * 60 * 1000;//Convert minutes into milliseconds
        new CountDownTimer(noOfMinutes, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                Log.e("TAG", "onTick: hms: "+ hms);
                catalogBinding.txtTimer.setText(hms);//set text
            }

            public void onFinish() {

            }
        }.start();
    }

}
