package com.grocery60.inc.grocery.interfaces;

import android.content.Context;

import com.grocery60.inc.grocery.Utilities.CustomDialogs;

public class MyException extends RuntimeException {

    public MyException(Context ctx) {
        super();
        CustomDialogs.dialogSessionExpire(ctx);
    }

}
