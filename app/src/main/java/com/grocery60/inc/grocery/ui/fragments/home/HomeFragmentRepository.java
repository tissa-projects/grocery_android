package com.grocery60.inc.grocery.ui.fragments.home;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.ServiceCall.RestAsyncTask;


public class HomeFragmentRepository {
    private Application application;
    private static final String TAG = HomeFragmentRepository.class.getSimpleName();
    public HomeFragmentRepository(Application application) {
        this.application = application;
    }

    public MutableLiveData<String> getData(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                if (response == null) {
                    mutableLiveData.setValue(null);
                } else {
                    mutableLiveData.setValue(response);
                }
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }
    public MutableLiveData<String> login(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        Log.e(TAG, "login: "+helper.toString() );
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            Log.e(TAG, "login: "+helper.toString() );
            Log.e(TAG, "login: "+response );
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }

    public MutableLiveData<String> getCart(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }

    public MutableLiveData<String> createCart(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }
    public MutableLiveData<String> getCartCount(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }

    public MutableLiveData<String> searchStore(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }

    public MutableLiveData<String> deleteCart(RestAPIClientHelper helper) {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        RestAsyncTask restAsyncTask = new RestAsyncTask(application.getApplicationContext(), helper, TAG, (response) -> {
            try {
                mutableLiveData.setValue(response);
            } catch (Exception e) {
                mutableLiveData.setValue(null);
                e.printStackTrace();
            }
        });
        restAsyncTask.execute();

        return mutableLiveData;
    }
}
