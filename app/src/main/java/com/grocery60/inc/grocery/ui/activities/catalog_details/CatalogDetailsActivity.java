package com.grocery60.inc.grocery.ui.activities.catalog_details;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.SharePreferenceUtil;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.ActivityCatalogDetailsBinding;
import com.grocery60.inc.grocery.interfaces.clickinterfaces.SetOnClickListener;

import org.json.JSONObject;

public class CatalogDetailsActivity extends AppCompatActivity implements SetOnClickListener {

    private ActivityCatalogDetailsBinding catalogDetailsBinding;
    private CatalogDetailsViewModel catalogDetailsViewModel;
    private Context context;
    private Dialog dialog;
    private JSONObject jsonIntentData;
    private String currencyType;
    public static final String TAG = CatalogDetailsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        catalogDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_catalog_details);
        catalogDetailsViewModel = ViewModelProviders.of(this).get(CatalogDetailsViewModel.class);
        context = CatalogDetailsActivity.this;
        findViewById(R.id.title_bar_left_arrow).setOnClickListener(v -> onBackPressed());
        currencyType = SharePreferenceUtil.getSPstringValue(context,SharePreferenceUtil.CURRENCY_TYPE);
        getIntentData();
        init();
        calculateSubTotalOverQty();

        catalogDetailsBinding.edtQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catalogDetailsViewModel.setQuantity(context,catalogDetailsBinding);
            }
        });

    }

    private void calculateSubTotalOverQty() {
        catalogDetailsBinding.txtQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!catalogDetailsBinding.txtQty.getText().toString().equalsIgnoreCase("")) {
                    Log.e(TAG, "afterTextChanged: currencyType: "+currencyType );

                    float unitCost = currencyType.equals("$")?
                            Float.valueOf(catalogDetailsBinding.txtUnitPrice.getText().toString().replace(",","").split("\\$")[1]):
                            Float.valueOf(catalogDetailsBinding.txtUnitPrice.getText().toString().replace(",","").split("\\₹")[1]);
                    float qty = Float.valueOf(catalogDetailsBinding.txtQty.getText().toString());
                    catalogDetailsBinding.txtSubtotal.setText(currencyType+ Utils.convertToUSDFormat((unitCost * qty)+""));
                }
            }
        });
    }

    private void getIntentData() {
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                String data = bundle.getString("data");
                jsonIntentData = new JSONObject(data);
                Log.e(TAG, "getIntentData: "+jsonIntentData.toString() );
                String prooductName = jsonIntentData.has("product_name") ? jsonIntentData.getString("product_name"):jsonIntentData.getString("productName");
                String prooductUrl = jsonIntentData.has("productUrl") ? jsonIntentData.getString("productUrl"):jsonIntentData.getString("product_url");
                catalogDetailsBinding.productName.setText(prooductName);
//                catalogDetailsBinding.txtAvailable.setText(jsonIntentData.getString(""));
                catalogDetailsBinding.txtUnitPrice.setText(currencyType+Utils.convertToUSDFormat(jsonIntentData.getString("price")));
                catalogDetailsBinding.txtSubtotal.setText(currencyType+Utils.convertToUSDFormat(jsonIntentData.getString("price")));
                final String imgUrl = prooductUrl;
                Utils.displayImageOriginalString(context,catalogDetailsBinding.productImage,imgUrl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        catalogDetailsBinding.setAddToCartBtnClcik(this::onCLick);
    }

    @Override // add to cart_app_icon btn click
    public void onCLick() {
        if (VU.isConnectingToInternet(context))
            addToCart();
    }

    private void addToCart() {

        RestAPIClientHelper helper = new RestAPIClientHelper();
        try {
            String prooductId = jsonIntentData.has("product_id") ? jsonIntentData.getString("product_id"):jsonIntentData.getString("productId");
            JSONObject reqObj = new JSONObject();
            reqObj.put("quantity",catalogDetailsBinding.txtQty.getText().toString());
            reqObj.put("cart_id",SharePreferenceUtil.getSPstringValue(context,SharePreferenceUtil.CART_ID));
            reqObj.put("product_id",prooductId);
            reqObj.put("extra","");

            helper.setContentType("application/json");
            helper.setMethodType("POST");
            helper.setRequestUrl(getResources().getString(R.string.main_url) + getResources().getString(R.string.add_to_cart));
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        catalogDetailsViewModel.getAddToCartData(helper).observe(this, (response) -> {
            try {
                dialog.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject responseObj = new JSONObject(response);
                    Log.e(TAG, "getCatalogList: response: " + responseObj);
                    if (responseObj.getInt("response_code") == 201) {
                        CustomDialogs.dialogCustomShowMsg(CatalogDetailsActivity.class,context, getResources().getString(R.string.product_added_into_cart)).findViewById(R.id.btn_ok).setOnClickListener((View)->{finish();});
                    }else
                    Utils.getToast(context,getResources().getString(R.string.no_response));
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.getToast(context,getResources().getString(R.string.no_response));
            }
        });

    }
}
