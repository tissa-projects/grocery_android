package com.grocery60.inc.grocery.ui.fragments.address;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.grocery60.inc.grocery.R;
import com.grocery60.inc.grocery.ServiceCall.RestAPIClientHelper;
import com.grocery60.inc.grocery.Utilities.CustomDialogs;
import com.grocery60.inc.grocery.Utilities.UserSession;
import com.grocery60.inc.grocery.Utilities.Utils;
import com.grocery60.inc.grocery.Utilities.VU;
import com.grocery60.inc.grocery.databinding.FragmentAddressBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddressFragment extends Fragment implements View.OnClickListener {

    private FragmentAddressBinding addressBinding;
    private AddressViewModel viewModel;
    private Context context;
    private Dialog dialogShipping, dialogBilling;
    private NavController navController;
    private JSONObject customerObj = null;
    private Bundle bundle = null;
    private boolean isShipAddrAvail = false, isBillAddrAvail = false;
    private String strBillingAddressId = null, strShippingAddressId = null;
    private ArrayList<String> countryNameList = null;
    public static final String TAG = AddressFragment.class.getSimpleName();

    public AddressFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Bundle bundleData = getArguments();
            if (bundleData != null) {
                String strCustomer = bundleData.getString("customerObj");
                customerObj = new JSONObject(strCustomer);
                Log.e(TAG, "onCreate: " + customerObj.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        countryNameList = new ArrayList<>();
        countryNameList.add("Ind");
        countryNameList.add("US");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        addressBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_address, container, false);
        viewModel = ViewModelProviders.of(this).get(AddressViewModel.class);
        context = getActivity();
        if (VU.isConnectingToInternet(context)) {
            getShippingAddress();
            getBillingAddress();

        }

        addressBinding.billingEdtCountry.setOnClickListener(this::onClick);
        addressBinding.shippingEdtCountry.setOnClickListener(this::onClick);
        return addressBinding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);

        addressBinding.btnNext.setOnClickListener(v -> {    // ===================next btn click============================
            bundle = new Bundle();
            bundle.putString("customerObj", customerObj.toString());
            try {
                JSONObject shippingAddressObj = new JSONObject();
                JSONObject billingAddressObj = new JSONObject();

                shippingAddressObj.put("name", addressBinding.shippingEdtName.getText().toString());
                shippingAddressObj.put("company_name", addressBinding.shippingEdtCompanyName.getText().toString());
                shippingAddressObj.put("address", addressBinding.shippingEdtAddressLine.getText().toString());
                shippingAddressObj.put("house_number", addressBinding.shippingEdtHouseNo.getText().toString());
                shippingAddressObj.put("zip", addressBinding.shippingEdtZipCode.getText().toString());
                shippingAddressObj.put("city", addressBinding.shippingEdtCity.getText().toString());
                shippingAddressObj.put("country", addressBinding.shippingEdtCountry.getText().toString());

                billingAddressObj.put("name", addressBinding.billingEdtName.getText().toString());
                billingAddressObj.put("company_name", addressBinding.billingEdtCompanyName.getText().toString());
                billingAddressObj.put("address", addressBinding.billingEdtAddressLine.getText().toString());
                billingAddressObj.put("house_number", addressBinding.billingEdtHouseNo.getText().toString());
                billingAddressObj.put("zip", addressBinding.billingEdtZipCode.getText().toString());
                billingAddressObj.put("city", addressBinding.billingEdtCity.getText().toString());
                billingAddressObj.put("country", addressBinding.billingEdtCountry.getText().toString());

                if (addressBinding.checkbox.isChecked()) {
                    bundle.putString("addressObj", shippingAddressObj.toString());
                } else {
                    bundle.putString("addressObj", billingAddressObj.toString());
                }

                bundle.putString("shippingAdddressObj", shippingAddressObj.toString());
                if (VU.isConnectingToInternet(context)) {
                    if (viewModel.validate(context, addressBinding)) {
                        if (addressBinding.checkbox.isChecked()){
                        if (isBillAddrAvail) {
                            updateBillingAddress();
                        } else {
                            setBillingAddress();
                        }}

                        if (isShipAddrAvail) {
                            updateShippingAddress();
                        } else {
                            setShippingAddress();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        });

        addressBinding.checkbox.setOnClickListener((View v) -> {
            if (addressBinding.checkbox.isChecked()) {
                addressBinding.rlBillingAddress.setVisibility(View.GONE);
            } else addressBinding.rlBillingAddress.setVisibility(View.VISIBLE);
        });

    }

    //country dialog
    private void setCountry() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select City");
        // add a list
        builder.setItems(Utils.GetStringArray(countryNameList), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                addressBinding.billingEdtCountry.setText(countryNameList.get(position));
                addressBinding.shippingEdtCountry.setText(countryNameList.get(position));
                dialog.dismiss();
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void getShippingAddress() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_shippinp_address_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogShipping = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getShippingAddress(helper).observe(getActivity(), (response) -> {
            try {
                dialogShipping.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getShippingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");

                        if (resultArray.length() > 0) {
                            JSONObject resultObj = resultArray.getJSONObject(0);
                            Log.e(TAG, "getShippingAddress: resultObj: " + resultObj.toString());
                            addressBinding.shippingEdtName.setText(resultObj.getString("name"));
                            addressBinding.shippingEdtCompanyName.setText(resultObj.getString("company_name"));
                            addressBinding.shippingEdtAddressLine.setText(resultObj.getString("address"));
                            addressBinding.shippingEdtHouseNo.setText(resultObj.getString("house_number"));
                            addressBinding.shippingEdtZipCode.setText(resultObj.getString("zip"));
                            addressBinding.shippingEdtCity.setText(resultObj.getString("city"));
                            addressBinding.shippingEdtState.setText(resultObj.getString("state"));
                            addressBinding.shippingEdtCountry.setText(resultObj.getString("country"));
                            strShippingAddressId = resultObj.getString("id");
                            isShipAddrAvail = true;
                        }
                    } else {
                        Utils.getToast(context,getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void setShippingAddress() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.set_shippinp_address_api);
        try {
            reqObj.put("name", addressBinding.shippingEdtName.getText().toString());
            reqObj.put("company_name", addressBinding.shippingEdtCompanyName.getText().toString());
            reqObj.put("address", addressBinding.shippingEdtAddressLine.getText().toString());
            reqObj.put("house_number", addressBinding.shippingEdtHouseNo.getText().toString());
            reqObj.put("zip", addressBinding.shippingEdtZipCode.getText().toString());
            reqObj.put("city", addressBinding.shippingEdtCity.getText().toString());
            reqObj.put("country", addressBinding.shippingEdtCountry.getText().toString());
            reqObj.put("state", addressBinding.shippingEdtState.getText().toString());
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("priority", 1);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogShipping = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.setShippingAddress(helper).observe(getActivity(), (response) -> {
            try {
                dialogShipping.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "setShippingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 201) {
                        JSONObject resultObj = jsonObject.getJSONObject("data");
                        Log.e(TAG, "setShippingAddress: " + resultObj.toString());
                        navController.navigate(R.id.paymentFragment, bundle);
                    } else {
                        Utils.getToast(context,getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private void updateShippingAddress() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.update_shippinp_address_api) + strShippingAddressId + "/";
        try {
            reqObj.put("name", addressBinding.shippingEdtName.getText().toString());
            reqObj.put("company_name", addressBinding.shippingEdtCompanyName.getText().toString());
            reqObj.put("address", addressBinding.shippingEdtAddressLine.getText().toString());
            reqObj.put("house_number", addressBinding.shippingEdtHouseNo.getText().toString());
            reqObj.put("zip", addressBinding.shippingEdtZipCode.getText().toString());
            reqObj.put("city", addressBinding.shippingEdtCity.getText().toString());
            reqObj.put("country", addressBinding.shippingEdtCountry.getText().toString());
            reqObj.put("state", addressBinding.shippingEdtState.getText().toString());
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("priority", 1);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.PUT));
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogShipping = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.setShippingAddress(helper).observe(getActivity(), (response) -> {
            try {
                dialogShipping.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "updateShippingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONObject resultObj = jsonObject.getJSONObject("data");
                        Log.e(TAG, "updateShippingAddress: " + resultObj.toString());
                        navController.navigate(R.id.paymentFragment, bundle);
                    } else {
                        Utils.getToast(context,getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void setBillingAddress() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.set_billing_address_api);
        try {
            reqObj.put("name", addressBinding.billingEdtName.getText().toString());
            reqObj.put("company_name", addressBinding.billingEdtCompanyName.getText().toString());
            reqObj.put("address", addressBinding.billingEdtAddressLine.getText().toString());
            reqObj.put("house_number", addressBinding.billingEdtHouseNo.getText().toString());
            reqObj.put("zip", addressBinding.billingEdtZipCode.getText().toString());
            reqObj.put("city", addressBinding.billingEdtCity.getText().toString());
            reqObj.put("country", addressBinding.billingEdtCountry.getText().toString());
            reqObj.put("state", addressBinding.billingEdtState.getText().toString());
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("priority", 1);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.POST));
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogBilling = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.setBillingAddress(helper).observe(getActivity(), (response) -> {
            try {
                dialogBilling.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "setBillingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 201) {
                        JSONObject resultObj = jsonObject.getJSONObject("data");
                        Log.e(TAG, "setBillingAddress: " + resultObj.toString());

                    } else {
                        Utils.getToast(context,getResources().getString(R.string.no_response));

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void updateBillingAddress() {
        JSONObject reqObj = new JSONObject();
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.update_billing_address_api) + strBillingAddressId + "/";
        try {
            reqObj.put("name", addressBinding.billingEdtName.getText().toString());
            reqObj.put("company_name", addressBinding.billingEdtCompanyName.getText().toString());
            reqObj.put("address", addressBinding.billingEdtAddressLine.getText().toString());
            reqObj.put("house_number", addressBinding.billingEdtHouseNo.getText().toString());
            reqObj.put("zip", addressBinding.billingEdtZipCode.getText().toString());
            reqObj.put("city", addressBinding.billingEdtCity.getText().toString());
            reqObj.put("country", addressBinding.billingEdtCountry.getText().toString());
            reqObj.put("state", addressBinding.billingEdtState.getText().toString());
            reqObj.put("customer_id", UserSession.getUserDetails(context).get("user_id"));
            reqObj.put("priority", 1);
            helper.setContentType("application/json");
            helper.setMethodType(getResources().getString(R.string.PUT));
            helper.setRequestUrl(url);
            helper.setUrlParameter(reqObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogBilling = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.setBillingAddress(helper).observe(getActivity(), (response) -> {
            try {
                dialogBilling.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "updateBillingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONObject resultObj = jsonObject.getJSONObject("data");
                        Log.e(TAG, "updateBillingAddress: " + resultObj.toString());

                    } else {
                        Utils.getToast(context,getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void getBillingAddress() {
        RestAPIClientHelper helper = new RestAPIClientHelper();
        String url = getResources().getString(R.string.main_url) + getResources().getString(R.string.get_billing_address_api) + UserSession.getUserDetails(context).get("user_id");
        try {
            helper.setContentType("application/json");
            helper.setMethodType("GET");
            helper.setRequestUrl(url);
            helper.setUrlParameter("");
        } catch (Exception e) {
            e.printStackTrace();
        }

        dialogBilling = ProgressDialog.show(context, "Please wait", "Loading...");
        viewModel.getBillingAddress(helper).observe(getActivity(), (response) -> {
            try {
                dialogBilling.dismiss();
                if (response == null) {
                    CustomDialogs.dialogRequestTimeOut(context);
                } else {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e(TAG, "getBillingAddress: " + jsonObject);
                    if (jsonObject.getInt("response_code") == 200) {
                        JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("results");

                        if (resultArray.length() > 0) {
                            JSONObject resultObj = resultArray.getJSONObject(0);
                            Log.e(TAG, "getBillingAddress: resultObj: " + resultObj.toString());
                            addressBinding.billingEdtName.setText(resultObj.getString("name"));
                            addressBinding.billingEdtCompanyName.setText(resultObj.getString("company_name"));
                            addressBinding.billingEdtAddressLine.setText(resultObj.getString("address"));
                            addressBinding.billingEdtHouseNo.setText(resultObj.getString("house_number"));
                            addressBinding.billingEdtZipCode.setText(resultObj.getString("zip"));
                            addressBinding.billingEdtCity.setText(resultObj.getString("city"));
                            addressBinding.billingEdtState.setText(resultObj.getString("state"));
                            addressBinding.billingEdtCountry.setText(resultObj.getString("country"));
                            strBillingAddressId = resultObj.getString("id");
                            isBillAddrAvail = true;
                        }
                    } else {
                        Utils.getToast(context,getResources().getString(R.string.no_response));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.shipping_edt_country:
            case R.id.billing_edt_country:
                setCountry();
                break;
        }
    }
}
